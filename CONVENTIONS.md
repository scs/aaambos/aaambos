# Conventions

Header/Preamble of a python file in this project, following recommendations from  [StackOverflow](https://stackoverflow.com/questions/1523427/what-is-the-common-header-format-of-python-files)
```python 
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""\
short doc

long doc
"""

# built-in modules imports
# followed by third-party imports
# followed by changes to the path and own modules

__author__ = "One solo developer"
__authors__ = ["One developer", "And another one", "etc"]
__contact__ = "mail@example.com"
__copyright__ = "Copyright $YEAR, $COMPANY_NAME"
__credits__ = ["One developer", "And another one", "etc"]
__date__ = "YYYY/MM/DD"
__deprecated__ = False
__email__ =  "mail@example.com"
__license__ = "GPLv3"
__maintainer__ = "developer"
__status__ = "Production"
__version__ = "0.0.1"

# start code
```
Example:
```python 
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""\
short doc

long doc
"""

# built-in modules imports
# followed by third-party imports
# followed by changes to the path and own modules

__author__ = "Florian Schröder"
__contact__ = "fschroeder@techfak.de"
# __credits__ = []
__date__ = "YYYY/MM/DD"
__deprecated__ = False
__license__ = "GPLv3"
__maintainer__ = "florian.schroeder"
__status__ = "Development"
__version__ = "0.0.1"

# start code
```