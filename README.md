# Architectures for Autonomous Agents: A Modular Basis with an Operating Supervisor - AAAMBOS Framework

- [Website](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/)
- [API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos/aaambos.html)

## Run

`aaambos run --run_config="examples/run_config_pg.yml" --arch_config="examples/arch_config_ping_pong.yml"`

TODOs:
- [x] run time manager behavior (check start up of modules, inform etc.), communication, own process
- [ ] ~~pkgs~~, conventions, systems, (cookiecutter)
  - [ ] with entry_points etc.
  - [ ] ruff, black etc.
- [ ] API Docs
  - [x] Create Pdoc docs
  - [ ] write docs for all classes, methods, and attributes
- [x] Web Docs / Infos (docusaurus)
- [ ] diagrams for understanding
  - [x] idea
  - [ ] nice diagrams (diagrams.net)
- [x] auto generated diagrams of arch
- [x] std modules, extensions etc.
- [ ] Convert VIVA/SCS Architecture
- [x] module that executes p27 code (Popen with pipe)
- [x] Pepper / Naoqi Backend (py27)
- [x] extensions add communication promises to module
- [x] GUI via Extension
- [x] set type hint for leaf topic, id, and module_name instead of str
- [x] ipaacar wrapper, and iu

Midterm TODOs for version 1.0.0:
- [ ] tests
  - [ ] unit tests -> methods, classes in a pkg
  - [ ] integration tests -> module simulator (communication service replaced), test in and output
- [ ] implement commands
  - [ ] solo (single module)
  - [ ] check (just setup) (maybe just an attribute for the run command?)
  - [ ] test
  - [ ] discover/list -> show all installed pkgs, modules, extensions, etc.; Link to website
  - [ ] docs ? 
  - [x] history/agents -> link to default aaambos agents dir; list all aaambos agents (from global aaaambos history file) with date and link to folder; "list" coomand
  - [x] configure -> set general aaambos options -> stored in aaambos config
  - [ ] remove; install; etc. ??? pkgs, modules, (remove history, agent data)
  - [ ] logs: show log file content of agents and modules
- [x] flexdiam / dialog
- [ ] zeromq com, ros com?, rabbitmq com, etc.
  - [x] redis
  - [ ] rabbit mq
  - [ ] zero mq
  - [ ] ros, possible? because no asnyc?
  - [ ] websocket
- [ ] -> module expectations / run time manager
- [ ] module status manager can terminate the complete architecture with closing window  ? 
- [x] run a single module (in a solo modus). For tests, solo command, etc. Replace com service. Idea how to simulate timed and reflex messages (communication Models?)
- [ ] with the pkg installation, register entry points for:
  - [x] create command (flexdiam: issues, observers, etc.)
  - [ ] module info (autofind modules that provide features)
  - [ ] (com services that satisfy specific com attributes / categories)
  - [ ] communication unit/wrapper/promise converter 
- [x] global aaambos file that contains all aaambos agents/runs with the link to the folder. Needs to be in a suitable location (.locals, /etc ? ...) Defined in aaambos lib (not changeable)
- [x] global aaambos config file, location similar to the prev todo point
  - [x] define content of aaambos config
- [x] aaambos create command "com_promise": alias "promise"
- [x] aaambos create command "com_promise": allow adding the new promise content to a python file
- [ ] allow docker container connection -> decide: start docker container or just connect to a running node / mqtt channel
- [x] execute shell commands or python functions before architecture start
- [x] more functionality in module status extension: trigger method calls and sending messages based on received module statuses.
- [x] implement module start delay (also negative number -> utility modules want to start before all other modules)
- [ ] all attrs define as kwargs only for inheritance compatibility
- [ ] GUI that shows com connections and which can also assign new topics to unused/assigned promises/callbacks. 
  - [ ] Also show content of a topic if possible/if selected.
  - [ ] Calculate and show the delay between input and output messages
- [x] store copies of configs in the agent dirs (maybe also copies of the filled config objects?)

Long-term TODOs:
- [ ] concurrency: threading, synchronous/asynchronous
- [ ] redo feature satisfaction algo (e.g., include feature requirments list in relation to modules (extension module requires com feature etc.))
- [ ] exception catching and reformatting/convert to/as understandable messages for the aaambos user
- [ ] global config not only overwrite run and arch config but also internal behaviour changes / hooks / position in code decisions
- [ ] naming ? -> different names for same concepts (agent -> multi-service architecture; different agent terminology); allow different naming -> set names of terms / naming convention and use it in an architecture consistently -> logs, guis, code, imports etc.
- [ ] hydro
- [ ] communication models -> relationships between promises (timing, which should be send after each other, e.g., image -> face event -> etc.) (dependencies between attributes: ids, etc.)
- [ ] auto gen configs
- [ ] await awaitables based on more complex module statuses with the module status extension. (Check also old statuses, reset (failed/termianted)) etc.
- [ ] terminate as async (get event loop in signal callback and call terminate func with await)
- [ ] docker supervisor/module runner
- [ ] terminate of GUI / Failed processes do not send status (or reset / ignore old statuses)
- [ ] register atexit calls