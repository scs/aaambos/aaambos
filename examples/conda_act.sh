#!/bin/sh

# bash script that activates a conda environment and runs the test1.py
#
# first argument: path to conda.sh, e.g., ~/miniconda3/etc/profile.d/conda.sh
# second argument name of the conda environment, e.g., aaambos


. $1
SCRIPTPATH=$(dirname "$0")
conda activate $2
python3 $SCRIPTPATH/test1.py