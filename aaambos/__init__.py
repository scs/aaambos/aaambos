"""
## API Documentation

For a general introduction into aaambos visit the website: [AAAMBOS Website](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/)

The *aaambos* library is divided in the `aaambos.core` library and the standard (aaambos.std) library.
The core functionality is provided in the **core** part and contains definitions, base classes and minimal implementations of mayor parts of aaambos.

The **std** library contains specific implementations, e.g., general communication attributes and categories, a hook and run config access extension, the shell command module, a ping pong example for msgs and ius, a status manager as a module and extensions for other modules, gui modules and extensions, and a more complex instracution manager.

### ⇒ `aaambos.core`
Defines the core base classes.


### ⇒ `aaambos.std`
Defines standard modules, extensions, and communication attributes.

"""

__author__ = """Florian Schröder"""
__email__ = 'fschroeder@techfak.de'
__version__ = '0.1.0'


APP_NAME = "aaambos"