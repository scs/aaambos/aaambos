"""The main entry point of aaambos via cli.

It builds a parser based on the default commands and then parses the command line arguments.

The extracted command is executed, e.g., running an architecture.
"""

import sys

import jsonargparse
from loguru import logger

from aaambos.core.cli.jsonargparse_args import CommandSetupArgs, ParserArgs
from aaambos.core.commands.command import Command
from aaambos.core.cli.jsonargparse_parser import JsonArgParseParserFactory


def main():
    """

    Returns:

    """
    parser = JsonArgParseParserFactory().build_parser(
        parser_args=ParserArgs(
            description="Execute aaambos commands like 'run' an architecture or 'create' a pkg.",
        ),
        command_setup_args=CommandSetupArgs(),
        commands=JsonArgParseParserFactory.get_default_commands())
    cfg = parser.parse_args()
    logger.remove()
    logger.add(sys.stderr, level=cfg.log_level)
    logger.trace("Args parsed based on JsonArgParseParser with default commands")
    command_name, command_args = JsonArgParseParserFactory.extract_command(cfg)
    logger.info(f"Command {command_name!r} with {command_args=} found")
    command: Command = command_args.command_class(**jsonargparse.namespace_to_dict(command_args))
    logger.info(f"Execute {command_name!r}-Command")
    command.execute()


if __name__ == '__main__':
    main()
