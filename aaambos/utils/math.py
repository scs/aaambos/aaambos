
def clip(n, min_n, max_n):
    """

    Args:
        n:
        min_n:
        max_n:

    Returns:

    """
    return max(min(max_n, n), min_n)