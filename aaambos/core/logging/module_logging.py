from __future__ import annotations

from loguru import logger

from aaambos.core.configuration.logging_config import LoggingConfig


def loguru_module_logger(logger_config: LoggingConfig):
    """Provides the loguru logger for a module.

    Args:
        logger_config: a logger config that is not used at the moment.

    Returns:
        An individual loguru logger instance that can be adapted.
    """
    module_logger = logger.bind()

    return module_logger

