import csv
import os
from datetime import datetime
from pathlib import Path
from typing import Tuple

from platformdirs import user_data_dir

from aaambos import APP_NAME


HISTORY_FILE_NAME = "history.csv"


class ExecutedAgents:
    """
    Class for managing executed agents history.
"""
    @classmethod
    def get_executed_agents_dir(cls) -> Path:
        """Returns the directory path where the executed agents are stored.

        Returns:
            Path: The directory path where the executed agents are stored.
        """
        return Path(user_data_dir(appname=APP_NAME, appauthor=False))

    @classmethod
    def get_executed_agents_file_path(cls) -> Path:
        """
        get_executed_agents_file_path method is a class method that returns the file path of the executed agents history file.

        Returns:
            Path: Returns a Path object representing the file path of the executed agents history file.

        Example Usage:
            file_path = MyClass.get_executed_agents_file_path()
        """
        return cls.get_executed_agents_dir() / HISTORY_FILE_NAME

    @classmethod
    def create_history_file(cls):
        """

        Create a history file for executed agents.

        This method creates a history file to log the execution start time, agent ID, and agent directory for each executed agent.

        Example usage:
            create_history_file()

        """
        if not cls.get_executed_agents_file_path().is_file():
            if not cls.get_executed_agents_dir().is_dir():
                os.makedirs(cls.get_executed_agents_dir(), exist_ok=True)
            with open(cls.get_executed_agents_file_path(), "w") as file:
                file.write("Execution Start Time,Agent Id,Agent Directory\n")

    @classmethod
    def add_agent(cls, date: datetime, agent_id: str, directory: Path):
        """
        Args:
            date (datetime): The date when the agent is added.
            agent_id (str): The ID of the agent being added.
            directory (Path): The directory where the agent is located.

        """
        cls.create_history_file()
        str_to_store = f"{date},{agent_id},{directory}\n"
        with open(cls.get_executed_agents_file_path(), "a") as file:
            file.write(str_to_store)

    @classmethod
    def get_all_executed_agents(cls, after: datetime = ..., before: datetime = ...) -> list[Tuple[datetime, str, Path]]:
        """
        Args:
            after: The minimum date and time. Only agents executed after this date and time will be included in the result. Defaults to None.
            before: The maximum date and time. Only agents executed before this date and time will be included in the result. Defaults to None.

        Returns:
            A list of tuples containing the date and time of execution, the agent name, and the agent file path.
        """
        if not cls.get_executed_agents_file_path().is_file():
            return []
        history = []
        with open(cls.get_executed_agents_file_path(), "r") as file:
            reader = csv.reader(file, delimiter=",")
            for i, row in enumerate(reader):
                if i == 0:
                    continue
                assert len(row) >= 3, f"Error in {cls.get_executed_agents_file_path()}, less than 3 values in line {i}."
                date = datetime.fromisoformat(row[0])
                if after is ... or date > after:
                    if before is ... or date < before:
                        history.append((date, row[1], Path(row[2])))
        return history

    @classmethod
    def get_last_executed_agent(cls, id_starts_with: str = "") -> tuple[datetime, str, Path] | None:
        """
        Args:
            id_starts_with: A string indicating the prefix that the agent id should start with. Defaults to an empty string.

        Returns:
            A tuple containing the date as a datetime object, the agent id as a string, and the path as a Path object. Returns None if the executed agents file does not exist or if no agent
        * with the specified prefix is found.
        """
        # TODO after, before
        if not cls.get_executed_agents_file_path().is_file():
            return None
        with open(cls.get_executed_agents_file_path(), "r") as file:
            reader = csv.reader(file, delimiter=",")
            for i, row in enumerate(reversed(list(reader))):
                assert len(row) >= 3, f"Error in {cls.get_executed_agents_file_path()}, less than 3 values in line {i}."
                date = datetime.fromisoformat(row[0])
                if row[1].startswith(id_starts_with):
                    return date, row[1], Path(row[2])