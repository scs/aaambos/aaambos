import sys

from aaambos.core.configuration.logging_config import LoggingConfig


class LoggerSetup:
    """Provides class methods to setup a logger."""

    @classmethod
    def setup_logger(cls, log, logging_config: LoggingConfig, name: str):
        """Configure a logger based on a loggingConfig, requires the name of the location where it is used.

        Args:
            log: Loguru logger.
            logging_config: the info about the logging.
            name: name of the module or 'supervisor' if it used in the supervisor.

        Returns:
            The adapted logger (log / first argumemt)
        """
        bash_format = (
            "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | "
            "<level>{level: <8}</level> | "
            + ("{extra[module]} | " if name != "supervisor" else "") +
            "<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - "
            "<level>{message}</level>"
        )
        if name != "supervisor":
            log.configure(extra={"module": name})
        log.remove()
        log.add(sys.stderr, format=bash_format, level=logging_config.log_level_command_line)
        log = cls.add_file_logger(log, logging_config, name)
        return log

    @classmethod
    def add_file_logger(cls, log, logging_config: LoggingConfig, name: str):
        """Add the sub part of the logger that passes the logs also to a file.

        Args:
            log: the logger.
            logging_config: config about specific logging location.
            name: the name of the module.

        Returns:

        """
        logfile_format = (
            "{time:YYYY-MM-DD HH:mm:ss.SSS} | "
            "{level: <8} | "
            "{function}:{line} - "
            "{message}"
        )
        log.add(logging_config.log_dir / f"{name}.log", format=logfile_format, level=logging_config.log_level_logfile)
        return log