"""The core aaambos library is divided in several parts (navigation on the left sidebar).

### Architecture
For defining own aaambos systems and conventions base classes are located here.

### CLI
The handling of cli commands (start and run an architecture) is defined here.

### Commands
The definition of different cli commands is located here.

### Communication
Everything about the communication betweeen modules is defined here.

### Configuration
Currently all configs are located in a seperate folder.

### Execution
Different concurrencies (multiprocessing, multithreading, asynchronous) of modules is defined here.

### Logging
Currently aaambos only uses loguru as a logger. Future abstractions will be located here.

### Module
The definition of the base module, module features and module extensions are located here.

### Supervision
All components of the running and managing of the architecture (with its modules) is located here.
"""