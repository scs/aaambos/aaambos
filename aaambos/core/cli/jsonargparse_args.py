import logging
from collections.abc import Iterable
from typing import Type, List, Literal

from attrs import define, field
from jsonargparse import ArgumentParser, DefaultHelpFormatter

from aaambos.core.commands.command import Command


@define
class ParserArgs:
    """argparse ArgumentParser constructor/init (kw-)arguments"""
    prog: str | None = None
    """The name of the program (default: ``os.path.basename(sys.argv[0])``)"""
    usage: str | None = None
    """A usage message (default: auto-generated from arguments)"""
    description: str | None = None
    """A description of what the program does"""
    epilog: str | None = None
    """Text following the argument descriptions"""
    parents: Iterable = field(factory=list)
    """Parsers whose arguments should be copied into this one"""
    # prefix_chars: str = "-",
    # """Characters that prefix optional arguments"""
    fromfile_prefix_chars: str | None = None
    """Characters that prefix files containing additional arguments"""
    argument_default = None
    """The default value for all arguments"""
    conflict_handler: Literal["error", "resolve"] = "error"
    """String indicating how to handle conflicts"""
    add_help: bool = True
    """Add a -h/-help option"""
    allow_abbrev: bool = True
    """Allow long options to be abbreviated unambiguously"""
    # additional JsonArgParse arguments
    env_prefix: bool | str = True
    """Prefix for environment variables. ``True`` to derive from ``prog``."""
    formatter_class: Type[DefaultHelpFormatter] = DefaultHelpFormatter
    """Class for printing help messages."""
    exit_on_error: bool = True
    """Determines whether or not ArgumentParser exits with error info when an error occurs. (default: True)"""
    logger: bool | str | dict | logging.Logger = False
    """Configures the logger, see :class:`.LoggerProperty`."""
    version: str | None = None
    """Program version which will be printed by the --version argument."""
    print_config: str | None = "--print_config"
    """Add this as argument to print config, set None to disable."""
    parser_mode: str = "yaml"
    """Mode for parsing configuration files: ``'yaml'``, ``'jsonnet'`` or ones added via :func:`.set_loader`."""
    dump_header: List[str] | None = None
    """Header to include as comment when dumping a config object."""
    default_config_files: List[str] | None = None
    """Default config file locations, e.g. :code:`['~/.config/myapp/*.yaml']"""
    default_env: bool = False
    """Set the default value on whether to parse environment variables."""
    default_meta: bool = True
    """Set the default value on whether to include metadata in config objects."""


@define
class CommandSetupArgs:
    """argparse parser (kw-)arguments for the method `add_subcommands`"""
    title: str = "commands"
    """Title for the sub-parser group in help output; \
    by default “subcommands” if description is provided, otherwise uses title for positional arguments."""
    description: str = "For more details of each command, add it as an argument followed by --help."
    """Description for the sub-parser group in help output, by default None."""
    # parser_class: Type = ArgumentParser
    # """Class which will be used to create sub-parser instances, \
    # by default the class of the current parser (e.g. ArgumentParser)."""
    # action: Action | None = None
    # """The basic type of action to be taken when this argument is encountered at the command line."""
    help: str | None = None
    """Help for sub-parser group in help output, by default None."""
    metavar: str | None = None
    """String presenting available sub-commands in help; \
    by default it is None and presents sub-commands in form {cmd1, cmd2, ..}."""
    # additional JsonArgParse arguments
    required: bool = True
    """Whether the subcommand must be provided."""
    dest: str = "command"
    """Name of the attribute under which sub-command name will be stored; by default None and no value is stored."""


@define
class CommandDefinition:
    """Util class that stores info about a command definition."""
    name: str
    parser: ArgumentParser
    command_class: Type[Command]
    aliases: Iterable[str] = field(factory=tuple)

