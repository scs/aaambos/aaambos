"""
The CLI is build with the `jsonargparse` library.

### `aaambos.core.cli.jsonargparse_args`
Contain the arguments as attrs dataclasses for jsonargparse arguments. So that, the arguments can easily be exchanged (instead of just kwargs dicts).
Further, it contains other attrs dataclasses that are used to define exchange classes.

### `aaambos.core.cli.jsonargparse_commands`
Here, the Subparsers for each command are defined.

### `aaambos.core.cli.jsonargparse_parser`
Here, the `aaambos.core.cli.jsonargparse_parser.JsonArgParseParserFactory` is defined. 
It contains the implementation of building the parser to parse the CLI arguments (containing the subcommands).
Further it implements the method that can extract the subcommand that is parsed by the parser.

### `aaambos.core.cli.parser_factory`
The abstract `aaambos.core.cli.parser_factory.ParserFactory` defines the relevant methods that are used during the creation of the Parser and the parsing process.
With this base class, other ParserFactories can be implemented which might do not use the `jsonargparse` library.
"""
import jsonargparse