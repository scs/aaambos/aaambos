from abc import ABC, abstractmethod


class ParserFactory(ABC):
    """Abstract class for the creation of parsers. 

    Necessary for the replacement of the jsonargparse parser creation.
    It requires the build_parser method.
    """

    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def build_parser(self, *args, **kwargs):
        """Create the parser of the aaambos system.

        Args:
            *args: parser specific arguments.
            **kwargs: parser specific kwargs arguments

        Returns:
            the created argument parser.
        """
        ...