from jsonargparse import ArgumentParser, set_docstring_parse_options
from jsonargparse.typing import Path_fr  # ignore

from aaambos.core.cli.jsonargparse_args import CommandDefinition
from aaambos.core.commands.list import what_to_list, List
from aaambos.core.commands.run import Run
from aaambos.core.commands.create import Create, core_create_names, create_names_from_eps
from aaambos.core.commands.set import Set
from aaambos.core.configuration.global_aaambos_config import GlobalAaaambosConfig

set_docstring_parse_options(attribute_docstrings=True)


def add_run_and_arch_config(parser: ArgumentParser):
    """Add the run config and arch config argument to a CLI ArgumentParser

    Args:
        parser: the argument parser to which the arguments are added.

    Returns:
        None
    """
    # TODO add info/link to docs / help
    parser.add_argument("-rc", "--run_config", type=Path_fr, required=True, help="the path to the run_config file")
    parser.add_argument("-ac", "--arch_config", type=Path_fr, required=True, help="the path to the arch_config file")


run_parser = ArgumentParser(description="'Run'-Command, runs an architecture based on the run- and arch-config.")
add_run_and_arch_config(run_parser)
run_parser.add_argument("--instance", type=str, help="Overwrite instance name of the agent in the run config")
run_parser.add_argument("--general_plus", type=str, help="additional parameter for the general run config part")
run_command = CommandDefinition(
    name="run",
    parser=run_parser,
    command_class=Run,
    aliases=("start", ),
)

# debug_parser = ArgumentParser()
# debug_command = CommandDefinition(
#     name="debug",
#     parser=debug_parser,
#     command_class=Run,  # TODO change
# )
#
# help_parser = ArgumentParser()
# help_command = CommandDefinition(
#     name="man",
#     parser=help_parser,
#     command_class=Run,  # TODO change
#     aliases=("describe", )
# )

create_parser = ArgumentParser(description="'Create'-Command, it uses the library cookiecutter (https://cookiecutter.readthedocs.io/en/stable/README.html) to create a pkg based on a template.")
create_parser.add_argument("what_to_create", type=str, choices=core_create_names + create_names_from_eps, help="What to create.")
create_parser.add_argument("-s", "--source", type=str, default="https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg.git", help="Cookiecutter repo to use, can be a git url or a local directory.")
create_parser.add_argument("-l", "--location", type=str, default=".", help="Location where the new pkg/file/etc. should be created/located.")
create_parser.add_argument("-n", "--name", type=str, default="My", help="Name of the module or extension to create in CamelCase. Not used for pkg")
create_parser.add_argument("-f", "--file", type=str, default="__no__", help="Name (and path) of the file to extend instead of printing the content (promise).")
create_parser.add_argument("-v", "--version", "--checkout", type=str, default="main", help="The version or variant to create something (The checkout/branch for cookiecutter template). E.g., for pkg the 'flexdiam_pkg'")
create_command = CommandDefinition(
    name="create",
    parser=create_parser,
    command_class=Create,
    aliases=("template", ),
)

set_parser = ArgumentParser(description="Set global aaambos options which are stored in a global aaambos config.")
set_parser.add_argument("option", type=str, choices=list(GlobalAaaambosConfig.__annotations__.keys()) + ["reset", "default"], help="What option / attribute to set in the global aaambos config.")
set_parser.add_argument("value", help="The value to set the option in the global aaambos config. Type depends on the option.")
set_command = CommandDefinition(
    name="set",
    parser=set_parser,
    command_class=Set,
    aliases=("global", "configure")
)

list_parser = ArgumentParser(description="List different things: agents/architectures that ran on the machine, installed pkgs, modules, extensions, ...")
list_parser.add_argument("what_to_list", type=str, choices=what_to_list)
list_command = CommandDefinition(
    name="list",
    parser=list_parser,
    command_class=List,
    aliases=("discover",)
)


# install_parser = ArgumentParser()
# install_command = CommandDefinition(
#     name="install",
#     parser=install_parser,
#     command_class=Run,  # TODO change
# )
#
# test_parser = ArgumentParser()
# test_command = CommandDefinition(
#     name="test",
#     parser=test_parser,
#     command_class=Run,  # TODO change
#     aliases=("tests", "run_tests"),
# )
#
# eval_parser = ArgumentParser()
# eval_command = CommandDefinition(
#     name="evaluate",
#     parser=eval_parser,
#     command_class=Run,  # TODO change
#     aliases=("eval", "run_eval", "run_evaluation")
# )
#
# check_parser = ArgumentParser()
# check_command = CommandDefinition(
#     name="check",
#     parser=check_parser,
#     command_class=Run,  # TODO change
#
# )
#
# find_parser = ArgumentParser()
# find_command = CommandDefinition(
#     name="find",
#     parser=find_parser,
#     command_class=Run,  # TODO change
#     aliases=("search", ),
# )
