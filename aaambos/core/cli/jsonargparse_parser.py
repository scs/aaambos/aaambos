from collections.abc import Iterable
from typing import Tuple, Type

from attrs import asdict
from jsonargparse import ArgumentParser, set_docstring_parse_options, Namespace

from aaambos.core.cli.jsonargparse_args import ParserArgs, CommandSetupArgs, CommandDefinition
from aaambos.core.cli.jsonargparse_commands import run_command, create_command, set_command, list_command
from aaambos.core.cli.parser_factory import ParserFactory
from aaambos.core.commands.command import Command

set_docstring_parse_options(attribute_docstrings=True)


class JsonArgParseParserFactory(ParserFactory):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ...

    @staticmethod
    def get_default_commands() -> Iterable[CommandDefinition]:
        """Provide the default commands for the aaambos system.

        Returns:
            list of command definitions.
        """
        return [
            run_command,
            create_command,
            set_command,
            list_command,
            # debug_command,
            # help_command,
            # install_command,
            # test_command,
            # eval_command,
            # check_command,
            # find_command,
        ]

    def build_parser(
            self,
            parser_args: ParserArgs = ParserArgs(),
            command_setup_args: CommandSetupArgs | None = None,
            commands: Iterable[CommandDefinition] | None = None
    ):
        """Create the CLI ArgumentParser of JsonArgParse with sucommands for aaambos

        Args:
            parser_args: the init args of the ArgumentParser from JsonArgParse via a attrs object.
            command_setup_args: required if commands should be added.
             The args of the add_subcommands method from JsonArgParse.
             https://jsonargparse.readthedocs.io/en/stable/#jsonargparse.ArgumentParser.add_subcommands
            commands: list of command definitions, e.g., from the get_default_commands method.

        Returns:
            the created argument parser with sucommands.
        """
        parser = ArgumentParser(**asdict(parser_args))

        # subcommands
        if command_setup_args and commands:
            subcommands = parser.add_subcommands(**asdict(command_setup_args))
            for parser_definition in commands:
                parser_definition.parser.add_argument(
                    "--command_class",
                    type=Type[Command],
                    required=False,
                    default=parser_definition.command_class,
                    help="command class to execute the subcommand"
                )
                subcommands.add_subcommand(**asdict(parser_definition))

        parser.add_argument("-ll", "--log_level", default="TRACE", help="the log level to use in the command execution, e.g., supervisor", choices=["TRACE", "DEBUG", "INFO", "SUCCESS", "WARNING", "ERROR", "CRITICAL"])

        return parser

    @staticmethod
    def extract_command(cfg: Namespace) -> Tuple[str, Namespace]:
        """Extracts the command and its arguments from an ArgumentParser Namespcae

        Args:
            cfg: the Namespace that is created by gthe parser by parsing CLI arguments.

        Returns:
            the name of the command and the "sub"namespace of the command.
        """
        return cfg.command, cfg[cfg.command]
