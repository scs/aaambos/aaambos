from __future__ import annotations

from typing import Type, TYPE_CHECKING

from attrs import define
from semantic_version import Version

if TYPE_CHECKING:
    from aaambos.core.configuration.run_config import RunConfig
    from aaambos.core.configuration.arch_config import ArchConfig

"""
name

version

arch_conf and run conf child / defaults (communication concurrency prefs, etc.)
with -c --convention <-convention_name-> search via input_points 


future / optional: add expected conventions (/check them) like features, type of modules etc. ??
"""


@define
class ArchitectureConvention:  # Scheme?
    name: str
    version: Version
    arch_config: Type[ArchConfig]
    run_config: Type[RunConfig]

    ...


