


"""
requirements / dependencies to other aaambos packages via conda/pip

entry points: info about provided features, modules, extensions, configs

"""
from attrs import define


@define
class AaambosPackageInfo:
    ...