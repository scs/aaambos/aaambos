from __future__ import annotations

from enum import Enum, auto
from typing import Type, TYPE_CHECKING


if TYPE_CHECKING:
    from aaambos.core.supervision.module_runner import ModuleRunner


class ConcurrencyLevel(Enum):
    @staticmethod
    def _generate_next_value_(name, start, count, last_values):
        return name

    MultiProcessing = auto()
    MultiThreading = auto()
    Asynchronous = auto()  # just asyncio
    Synchronous = auto()

    @classmethod
    def to_process_runner(cls, concurrency_level) -> Type[ModuleRunner]:  # TODO naming
        """Map an enum instance to the ModuleRunner variant based on the concurrency.

        Import the specific module runner only if it is matched.

        Args:
            concurrency_level: the concurrency level to map.

        Returns:
            The module runner class.
        """
        match concurrency_level:
            case cls.MultiProcessing:
                from aaambos.core.execution.process_based import ProcessRunner
                return ProcessRunner
            case cls.MultiThreading:
                from aaambos.core.execution.thread_based import ThreadRunner
                return ThreadRunner
            case cls.Asynchronous:
                from aaambos.core.execution.asynchronous import AsyncRunner
                return AsyncRunner
            case cls.Synchronous:
                raise NotImplementedError("Synchronous Runner is not implemented in this version of aaambos.")
        ...


