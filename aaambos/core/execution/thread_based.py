from multiprocessing import Queue
from threading import Thread

from loguru import logger

from aaambos.core.supervision.module_runner import IndependentRunner


class ThreadRunner(IndependentRunner):

    def __init__(self, config):
        logger.error("ThreadRunner does not work as intended")
        super().__init__(
            config,
            execution_class=Thread,
            queue_class=Queue,
            terminate_with_module=False
        )
