import sys
from collections import defaultdict
from multiprocessing import Queue
from pathlib import Path
from typing import Callable, Any, Awaitable, Tuple, Dict
from typing import Type

from hyperpyyaml import load_hyperpyyaml
from semantic_version import Version, SimpleSpec

from aaambos.core.communication.promise import PromiseSettings, CommunicationPromise
from aaambos.core.communication.service import CommunicationServiceInfo, CommunicationService
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.communication.topic import HierarchicalTopic, Topic, TopicId
from aaambos.core.configuration.logging_config import LoggingConfig
from aaambos.core.configuration.module_config import ModuleConfig, ModuleCommunicationInfo
from aaambos.core.configuration.run_config import RunConfig, GeneralRunPart
from aaambos.core.logging.module_logging import loguru_module_logger
from aaambos.core.module.base import ModuleInfo, Module
from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import CommunicationFeature, FeatureNecessity, SimpleFeatureNecessity
from aaambos.core.module.feature import ExtensionFeature
from aaambos.std.communications.attributes import IncrementalUnitAccessAttribute, MsgTopicSendingAttribute, \
    MsgPayloadWrapperCallbackReceivingAttribute, PayloadWrapperAttribute, RunTimeReceivingNewTopicsAttribute, \
    RunTimeSendingNewTopicsAttribute, IncrementalUnitTopicWrapperAttribute
from aaambos.std.modules.ping_pong_example import CountingMsg

STANDALONE_MODULE_TEST_EXTENSION = "StandaloneModuleTestExtension"

class EndTestException(Exception):
    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg


class StandaloneModuleTestExtension(Extension):
    """Base class for standalone module tests.

    It provides additional functionality for sending messages to a module and registering notifications for sending.

    Overwrite the class attribute `name` for your child classes. Further, you can implement the async `step` and `after_module_initialize` (equivialant to `initialize` in a module) methods.
    You can register notifications (like callbacks) from the module that you want to test. You can also send messages to the module. For both, see the methods `send_to_module` and `register_notification_for_send`.
    """
    name = STANDALONE_MODULE_TEST_EXTENSION

    async def before_module_initialize(self, module: Module):
        self.module = module

    async def after_module_initialize(self, module):
        pass

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return StandaloneModuleTestExtensionSetup

    async def send_to_module(self, topic_name: str, payload: Any):
        """Send msgs to the module.

        Args:
            topic_name: A string representing the name of the topic to which the message will be sent.
            payload: Any data that you want to send as the message payload.
        """
        assert topic_name in self.module.tpc
        await self.module.com.send_msg_to_module(self.module.tpc[topic_name], payload)

    def register_notification_for_send(self, function, topic_name="__all__"):
        """Registers a notification for sending.

        Args:
            function: The function to be registered for sending notifications.
            topic_name: The name of the topic to which the function should be subscribed. By default, the function will be subscribed to all topics with the name "__all__".
        """
        self.module.com.register_notification_for_send(function, topic_name)


class StandaloneModuleTestExtensionSetup(ExtensionSetup):
    """Setup class for standalone module test extension"""
    extension_class: Extension = StandaloneModuleTestExtension

    def before_module_start(self, module_config) -> Extension:
        return self.extension_class()


def create_test_extension_setup(extension_class: Type[StandaloneModuleTestExtension]) -> Type[
    StandaloneModuleTestExtensionSetup]:
    """Utility function to create the setup class for a child class of the module test extension"""
    # copy class, so that we can change the class attribute without changing it on the original class
    setup_class = type(f"{extension_class.name}Setup", StandaloneModuleTestExtensionSetup.__bases__,
                       dict(StandaloneModuleTestExtensionSetup.__dict__))
    setup_class.extension_class = extension_class
    return setup_class


StandaloneModuleTestExtensionFeature = ExtensionFeature(
    name=STANDALONE_MODULE_TEST_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=StandaloneModuleTestExtensionSetup,
    as_kwarg=False,
    kwarg_alias="standalone_module_test",
)


def define_test_standalone_feature(
        extension_class: Type[StandaloneModuleTestExtension] | list[Type[StandaloneModuleTestExtension]]) -> Dict[
    str, Tuple[ExtensionFeature, FeatureNecessity]]:
    """Utility method to create the feature declaration of a child class of the module test extension for the setup call (data structure similar to required/provided feature output)."""
    if isinstance(extension_class, list):
        feature_dict = {}
        for single_extension_class in extension_class:
            feature_dict.update(define_test_standalone_feature(single_extension_class))
        return feature_dict
    return {
        extension_class.name: (ExtensionFeature(
            name=extension_class.name,
            version=Version("0.1.0"),
            requirements=[],
            version_compatibility=SimpleSpec(">=0.1.0"),
            extension_setup_class=create_test_extension_setup(extension_class),
            as_kwarg=False,
            kwarg_alias="--",
        ), SimpleFeatureNecessity.Required)
    }


class TestComService(CommunicationService,
                     MsgTopicSendingAttribute,
                     MsgPayloadWrapperCallbackReceivingAttribute,
                     PayloadWrapperAttribute,
                     RunTimeReceivingNewTopicsAttribute,
                     RunTimeSendingNewTopicsAttribute,
                     IncrementalUnitTopicWrapperAttribute,
                     IncrementalUnitAccessAttribute
                     ):
    """Communication service for the standalone module test.

    Most methods are called by the standalone module. Only the `register_notification_for_send` should be called by the extension.
    """

    def __init__(self, config: ModuleCommunicationInfo, in_promises: list[CommunicationPromise],
                 out_promises: list[CommunicationPromise]):

        super().__init__(config)
        self.topic_to_callback_and_wrapper: dict[
            TopicId, list[tuple[Callable[[Topic, Any], Awaitable[None]], Type[Any]]]] = defaultdict(list)
        self.topics_listening: dict[TopicId, Topic] = {}
        self.topics_sending: dict[TopicId, Topic] = {}
        self.in_promises = in_promises
        self.out_promises = out_promises
        self.outgoing_msgs = defaultdict(list)
        self.registered_callbacks = defaultdict(list)
        self.send_notifications = defaultdict(list)

    def register_notification_for_send(self, function, topic_name="__all__"):
        """Called by the test extension to register a notification (callback) for messages from the module."""
        self.send_notifications[topic_name].append(function)

    async def send_msg_to_module(self, topic: Topic, payload: Any):
        """The extension can insert messages here that should be passed to the module via the `send_to_module` method."""
        if self.topic_to_callback_and_wrapper[topic.id()]:
            for callback, wrapper in self.topic_to_callback_and_wrapper[topic.id()]:
                print(wrapper, payload)
                if wrapper is not ...:
                    assert isinstance(payload, wrapper), "payload does not confirm to registered wrapper class from the module"
                await callback(topic, payload)

    async def initialize(self):
        for in_promise in self.in_promises:
            self.topics_listening[in_promise.settings.topic.id()] = in_promise.settings.topic
        for out_promise in self.out_promises:
            self.topics_sending[out_promise.settings.topic.id()] = out_promise.settings.topic

    async def send(self, topic: Topic, msg):
        print("----> Send Msg:", topic.name, msg)
        self.outgoing_msgs[topic.name].append(msg)
        for func in self.send_notifications[topic.name]:
            await func(topic, msg)
        for func in self.send_notifications["__all__"]:
            await func(topic, msg)

    async def register_callback(self, topic: Topic, callback: Callable, wrapper: Type[Any] = ..., *args, **kwargs):
        print("---> Register Callback:", topic.name, callback.__name__, wrapper.__name__)
        self.topic_to_callback_and_wrapper[topic.id()].append((callback, wrapper))
        self.registered_callbacks[topic.name].append((callback, wrapper))

    async def unregister_callback(self, topic: Topic, callback: Callable):
        pass

    def check_wrapping(self, payload, wrapper: Type[Any]) -> bool:
        pass

    async def add_input_topic(self, topic: Topic, callback: Callable[[Topic, Any], Awaitable[None]] = ...,
                              wrapper: Any = ...):
        pass

    async def add_output_topic(self, topic: Topic):
        pass

    async def create_and_send_iu(self, topic: Topic, payload) -> 'IU':
        pass

    async def register_callback_iu(self, topic: Topic, callback: Callable[[Topic, Any, 'IUEvent', 'IU', bool], None],
                                   wrapper: Type[Any] = ..., *args, **kwargs):
        pass

    async def set_payload_iu(self, iu, payload):
        pass

    async def get_all_ius(self) -> list['IU']:
        pass

    async def get_iu_by_id(self, uid=str) -> 'IU':
        pass


class TestComServiceSetup(CommunicationSetup):
    """Setup of the communication service for standalone (module) tests."""

    def __init__(self, communication_graph, run_config: RunConfig, in_promises, out_promises):
        super().__init__(communication_graph, run_config)
        self.in_promises = in_promises
        self.out_promises = out_promises

    def before_arch_start(self):
        pass

    def before_module_start(self, module_config: ModuleConfig) -> CommunicationService:
        return TestComService(module_config.com_info, in_promises=self.in_promises, out_promises=self.out_promises)


TestComServiceInfo = CommunicationServiceInfo(
    name="TestComServiceInfo",
    import_path="aaambos.core.execution.standalone.TestComService",
    attributes={},
    extra_categories=[],
    setup=TestComServiceSetup
)
"""Info of the standalone test communication service."""


class StandaloneTest:
    """Run a single _aaambos_ module, e.g., for testing purposes."""

    def __init__(self,
                 module_config: str | dict | ModuleConfig,
                 run_config_plus: str | dict,
                 module_name: str,
                 agent_name: str = "standalone_test",
                 instance: str = "test",
                 local_agent_directories: str = "~/aaambos_agents",
                 always_unique_instance: bool = True,
                 test_com_info: CommunicationServiceInfo = TestComServiceInfo
                 ):
        self.module_config_data = module_config
        self.run_config_plus = run_config_plus
        self.agent_name = agent_name
        self.instance = instance
        self.local_agent_directories = local_agent_directories
        self.always_unique_instance = always_unique_instance
        self.module_name = module_name

        self.module_config = None
        self.module_info = None
        self.com_setup = None
        self.test_com_info = test_com_info

    def setup(self, extra_extensions: dict[str, Tuple[ExtensionFeature, FeatureNecessity]]):
        """Assign additional features like extension features to the module and do the setup for a standalone run. """
        # define shallow run config
        logger_config = {"provide_logger_func": loguru_module_logger}
        if isinstance(self.run_config_plus, str):
            self.run_config_plus = load_hyperpyyaml(self.run_config_plus)
        run_config = RunConfig(general=GeneralRunPart(agent_name=self.agent_name, instance=self.instance,
                                                      always_unique_instance=self.always_unique_instance,
                                                      local_agent_directories=Path(self.local_agent_directories),
                                                      plus=self.run_config_plus), logging=logger_config)
        # setup module config and get module info
        if isinstance(self.module_config_data, ModuleConfig):
            self.module_config = self.module_config_data
            assert isinstance(self.module_config.module_info, ModuleInfo)
            self.module_info = self.module_config.module_info
            if self.module_name:
                self.module_config.name = self.module_name
        else:
            if isinstance(self.module_config_data, str):
                self.module_config_data = load_hyperpyyaml(self.module_config_data)

                assert "module_info" in self.module_config_data
                assert issubclass(self.module_config_data["module_info"], ModuleInfo)
            self.module_info = self.module_config_data["module_info"]

            self.module_config = self.module_info.get_module_config_class()(**self.module_config_data)
            self.module_config.name = self.module_name
        self.module_config.general_run_config = run_config.general
        self.module_config.logging = LoggingConfig(provide_logger_func=loguru_module_logger)
        self.module_config.logging.log_dir = run_config.general.get_instance_dir() / run_config.logging.log_dir_name
        self.module_config.setup()

        # feature setup
        r_features = self.module_info.requires_features(self.module_config).copy()
        r_features.update(extra_extensions)

        # turn all features on
        turn_on = self.module_info.provides_features(self.module_config).copy()
        turn_on.update(r_features)
        self.module_config.update_ft_info(turn_on, self.module_info.provides_features(self.module_config), r_features)
        # extract communication promises and define communication info for the module
        in_promises = [p for f in self.module_info.provides_features(self.module_config).values() if
                       isinstance(f[0], CommunicationFeature) for p in f[0].required_input]
        out_promises = [p for f in self.module_info.provides_features(self.module_config).values() if
                        isinstance(f[0], CommunicationFeature) for p in f[0].provided_output]
        for p in in_promises + out_promises:
            p.set_settings(
                PromiseSettings(topic=HierarchicalTopic(self.agent_name, self.instance, p.suggested_leaf_topic),
                                communication_service=TestComServiceInfo))
        in_wrapper = defaultdict(None,
                                 {p.settings.topic.name: p.communication_unit.payload_wrapper for p in in_promises})
        out_wrapper = defaultdict(None, {p.settings.topic.name: p.communication_unit.payload_wrapper for p in
                                         out_promises})
        module_com_info = ModuleCommunicationInfo(
            services=[TestComServiceInfo],
            in_wrapper=in_wrapper,
            out_wrapper=out_wrapper,
            leaf_to_topic=defaultdict(None, {p.settings.topic.name: p.settings.topic for p in
                                             in_promises + out_promises}),
            leaf_to_promise=defaultdict(None, {p.settings.topic.name: p for p in in_promises + out_promises})
        )
        self.module_config.com_info = module_com_info
        self.run_config = run_config
        # setup extension features
        if self.module_config.ft_info.ext_features:
            self.module_config.module_extension_setups = {}
            for name, e_feature in self.module_config.ft_info.ext_features.items():
                e_feature: ExtensionFeature
                self.module_config.module_extension_setups[name] = (
                    e_feature.extension_setup_class(run_config), e_feature)
        # communication setup
        self.com_setup = self.test_com_info.setup(None, self.run_config, in_promises, out_promises)
        self.run_config_plus["standalone_com_setup"] = self.com_setup

    def run(self):
        """Run a module after calling the `setup` method."""
        # atexit.register(sys.exit)
        try:
            self.module_info.get_start_function(self.module_config)(module_path=self.module_config.module_path,
                                                                    config=self.module_config, control_queue=Queue(),
                                                                    mean_step_time=(
                                                                            1 / self.module_config.mean_frequency_step),
                                                                    terminate_with_module=True,
                                                                    com_setups={self.test_com_info.name: self.com_setup})
        except EndTestException as e:
            print(f"Stop run based on EndTestException: {e}")

def run_standalone_ping_module():
    """Example how to run a module in standalone mode, e.g, for testing purposes.

    First of all you need to define a config for the module. Similar to the config of in an arch_config for a module.
    It needs at least contain the module_info key with the "!name:<import_path_module_info" of the module.
    You can also directly add a ModuleConfig object, if you do not want to test the import and parsing part.

    Next you need to define a StandaloneModuleTestExtension if you want to test the module, e.g., check if msgs from
    the module are correct and/or you need or want to send msgs to the module to trigger msgs or processing.

    It is quite simple:
     - you can send msgs to the module via the `send_to_module` method, which arguments need the topic name (leaf topic, e.g., the reference that should use the module internally) and the msg itself (the msgspec struct instance).
     - you can register own methods that should be called when the module sends msgs. Via `register_notification_for_send` with the awaitable function reference (and an optional filter for specific topic names).
     - "after_module_initialize" method is called after the module is initialized.
     - you can define a "step" function that is called with the same frequency as the step function from the module.

    To run a module in standalone you need to create an instance of the `StandaloneTest` class, call the `setup` method on it with the additional extension feature dict (created with the `define_test_standalone_feature` function), and call run.

    See the source code of this function for a detailed example for the PingPongModule.

    Due to a recursive call only around 380 messages can be exchanged per standalone run when using the notification method, due to the recursion depth limit.
    """

    module_config_yaml = """\
module_info: !name:aaambos.std.modules.ping_pong_example.PingPongModule
fst_send: 0"""

    class PingPongModuleTestExtension(StandaloneModuleTestExtension):
        name = "PingPongModuleTestExtension"

        async def after_module_initialize(self, module):
            print("Send Counting")
            from aaambos.std.modules.ping_pong_example import Counting
            self.register_notification_for_send(self.receive_ping, topic_name=Counting)
            payload = CountingMsg(value=1, last_module="pong")
            # self.own_counter = 1
            await self.send_to_module(Counting, payload)

        async def receive_ping(self, topic: Topic, msg: Any):
            if msg.value > 380:
                # maximum recursion depth exceeded after around 389 exchanged messages
                raise EndTestException("Finished Test")
            from aaambos.std.modules.ping_pong_example import Counting
            print(msg.value)
            await self.send_to_module(Counting, CountingMsg(value=msg.value + 1, last_module="pong"))

        # async def step(self):
        #     # check messages from the module via accessing the self.module.com.outgoing_msgs dict
        #     from aaambos.std.modules.ping_pong_example import Counting
        #     if self.module.com.outgoing_msgs[Counting] and self.module.com.outgoing_msgs[Counting][-1].value > self.own_counter:
        #         c = self.module.com.outgoing_msgs[Counting][-1].value + 1
        #         payload = CountingMsg(value=c, last_module="pong")
        #         self.own_counter = c
        #         await self.send_to_module(Counting, payload)

    standalone_ping = StandaloneTest(
        module_config=module_config_yaml,
        run_config_plus={},
        module_name="ping"
    )
    standalone_ping.setup(define_test_standalone_feature(PingPongModuleTestExtension))
    standalone_ping.run()

if __name__ == "__main__":
    run_standalone_ping_module()
