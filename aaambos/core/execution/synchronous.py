from aaambos.core.supervision.module_runner import ModuleRunner
from aaambos.core.supervision.run_time_manager import RunTimeManager, ControlMsg


class SynchronousRunner(ModuleRunner):

    def __init__(self, config, run_time_manager: RunTimeManager):
        super().__init__(config, run_time_manager)

    def check_modules(self, modules):
        pass

    async def start_modules(self, modules):
        pass

    def get_step_duration(self) -> float:
        pass

    async def step(self):
        pass

    async def start_module(self, control_msg: ControlMsg):
        pass

    async def terminate_module(self, control_msg):
        pass

    async def restart_module(self, control_msg: ControlMsg):
        pass

    def terminate(self):
        pass
