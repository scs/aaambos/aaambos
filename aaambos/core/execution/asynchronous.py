import importlib
from asyncio import Queue

from aaambos.core.module.base import Module
from aaambos.core.module.run import loop
from aaambos.core.module.setup import ModuleSetup
from aaambos.core.supervision.module_runner import AsyncTask, IndependentRunner


async def start_module_async(module_class_path: str, config, control_queue: Queue, mean_step_time: float,
                             terminate_with_module: bool):
    py_module = importlib.import_module(module_class_path)

    if not hasattr(py_module, "provide_module"):
        raise Exception()  # TODO

    arch_module = py_module.provide_module()

    arch_module: Module = ModuleSetup.setup_module(arch_module, ..., ...)

    return await loop(arch_module, config, control_queue, mean_step_time, terminate_with_module)


class AsyncRunner(IndependentRunner):

    def __init__(self, config):
        super().__init__(
            config,
            execution_class=AsyncTask,
            queue_class=Queue,
            terminate_with_module=False
        )
