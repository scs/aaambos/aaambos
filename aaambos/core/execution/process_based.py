from __future__ import annotations

from multiprocessing import Process, Queue

from aaambos.core.module.run import start_module
from aaambos.core.supervision.module_runner import IndependentRunner


class ProcessRunner(IndependentRunner):
    """The process variant of the independent runner.

    Uses the multiprocessing Queue and the Process. The process should terminate when the module terminates.
    """

    def __init__(self, config):
        super().__init__(
            config,
            execution_class=Process,
            queue_class=Queue,
            terminate_with_module=True
        )
