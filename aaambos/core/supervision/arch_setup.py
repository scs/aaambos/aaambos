from __future__ import annotations

import os
from collections import defaultdict
from pprint import pprint
from typing import TYPE_CHECKING

import attrs
from loguru import logger

from aaambos.core.communication.graph import CommunicationGraph
from aaambos.core.communication.promise import PromiseSettings
from aaambos.core.configuration.config_base import CONFIG_FILE
from aaambos.core.configuration.nested_config import NestedConfigFormat, NestedConfigParser
if TYPE_CHECKING:
    from aaambos.core.communication.service import CommunicationServiceInfo, ServiceName, ComServiceAttrCategory, \
        CommunicationServiceAttribute, ComServiceAttrName
    from aaambos.core.module.extension import ExtensionSetup

"""
Module Initiator (rename?)  / ArchPreparation
- creates arch graph
- checks dependencies
- chooses communications
- creates dependency config for modules
"""
from abc import abstractmethod
from typing import Type, Iterable, Any, TYPE_CHECKING

from aaambos.core.execution.concurrency import ConcurrencyLevel
from aaambos.core.configuration.module_config import ModuleConfig, ModuleCommunicationInfo
if TYPE_CHECKING:
    from aaambos.core.configuration.arch_config import ArchConfig
    from aaambos.core.module.base import ModuleName, ModuleInfo
    from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.configuration.setup_config import SetupConfig
from aaambos.core.module.feature import Feature, FeatureNecessity, CommunicationFeature, \
    ExtensionFeature, FeatureGraph

MODULE_INFO = "module_info"
RUN_TIME_MANAGER = "RunTimeManager"


class ArchSetup:
    """Base class for the setup of the architecture.

    Merging configs, decide for features, extensions and communication services.
    """
    # solves feature dependency / communication dep.-> which are required which are provided -> configure turned on features per module

    def __init__(self, setup_config: SetupConfig, run_config: RunConfig):
        """Init with some configs. Maybe add just all configs in the future (arch config).

        Args:
            setup_config: topic naming policy.
            run_config: run dependent config
        """
        # TODO add arch config
        self.setup_config = setup_config
        self.run_config = run_config
        self.modules_with_configs = ...

    @abstractmethod
    def merge_module_configs(self, arch_config: ArchConfig) -> dict[ModuleName, ModuleConfig]:
        """Prepare module configs by merging info from run config etc.

        Args:
            arch_config: contains the defined module configs

        Returns:
            merged module configs keyed by their name.
        """
        ...

    @abstractmethod
    def setup_features(self, modules_with_configs: dict[ModuleName, ModuleConfig], ignore=...) -> tuple[FeatureGraph, dict[ModuleName, ModuleConfig]]:
        """Decide which features to turn on and setup extension configs.

        Args:
            modules_with_configs: merged module configs.
            ignore: not used.

        Returns:
            the feature graph and the updated module configs (which should be updated indirectly in the run config anyway)
        """
        ...

    @abstractmethod
    def setup_communication_and_parallelism(self, module_configs: dict[ModuleName, ModuleConfig], arch_config: ArchConfig) -> tuple[CommunicationGraph, ConcurrencyLevel]:
        """Decide for concurrency and communication services.

        Args:
            module_configs: the module configs with info about turned on features.
            arch_config: the arch config.

        Returns:
            communication graph and the concurrency level
        """
        ...

    @classmethod
    def create_config(cls, config_dict: dict) -> SetupConfig:
        """

        Args:
            config_dict:

        Returns:

        """
        return SetupConfig(**config_dict)

    @abstractmethod
    def create_directories(self):
        """Create the directories that the agent can use (logs, data, etc.)

        Returns:
            None
        """
        ...


class SimpleArchSetup(ArchSetup):  # TODO adapt name to approach
    """A straight forward version of the arch setup."""

    def build_base_module_config(self, module_name: ModuleName, module_dict: dict[str: Any]) -> ModuleConfig:
        """Merge and init config for one module.

        Args:
            module_name: the module to merge.
            module_dict: the provided config as a dict from the arch config.

        Returns:
            the config for the module.
        """
        if CONFIG_FILE in module_dict:
            config_file = module_dict[CONFIG_FILE]
            del module_dict[CONFIG_FILE]
            module_dict = NestedConfigParser(NestedConfigFormat).parse_with_overrides(config_file, overrides=module_dict)
        assert MODULE_INFO in module_dict, f'"{MODULE_INFO}" is a required attribute of the modules section in the arch config.'
        module_class: Type[ModuleInfo] = module_dict[MODULE_INFO]
        module_config_class = module_class.get_module_config_class()
        module_config = module_config_class(**module_dict)

        module_config.logging = self.run_config.logging.__class__(**attrs.asdict(self.run_config.logging))

        if self.run_config.module_options and module_name in self.run_config.module_options:
            for option_name, option_value in self.run_config.module_options[module_name].items():
                match option_name:
                    case "logging":
                        if isinstance(option_value, dict):
                            for name, opt in option_value:
                                setattr(module_config.logging, opt, name)
                    case _:
                        setattr(module_config, option_name, option_value)

        if not module_config.logging.provide_logger_func:
            from aaambos.core.logging.module_logging import loguru_module_logger
            module_config.logging.provide_logger_func = loguru_module_logger

        module_config.name = module_name
        module_config.general_run_config = self.run_config.general
        module_config.setup()
        return module_config

    def merge_module_configs(self, arch_config: ArchConfig) -> dict[ModuleName, ModuleConfig]:
        module_configs = {}
        smallest_start_delay = 0  # only negative ones are relevant
        for module_name, module_conf in arch_config.modules.items():
            module_configs[module_name] = self.build_base_module_config(module_name, module_conf)
            smallest_start_delay = min(smallest_start_delay, module_configs[module_name].module_start_delay)
        if smallest_start_delay < 0:
            for module_name, module_conf in module_configs.items():
                module_conf.extra_delay = smallest_start_delay

        return module_configs

    def create_directories(self):
        self.run_config.logging.log_dir = self.run_config.general.get_instance_dir() / self.run_config.logging.log_dir_name
        if not os.path.exists(self.run_config.general.get_instance_dir()):
            logger.info(f"Create directory {self.run_config.general.get_instance_dir()!r}")
            os.makedirs(self.run_config.general.get_instance_dir())
        if not os.path.exists(self.run_config.logging.log_dir):
            logger.info(f"Create directory {self.run_config.logging.log_dir!r}")
            os.makedirs(self.run_config.logging.log_dir)

    def setup_features(self, module_configs: dict[ModuleName, ModuleConfig], ignore=...) -> tuple[FeatureGraph, dict[ModuleName, ModuleConfig]]:
        #self.modules_with_configs = modules_with_configs
        #return None,  modules_with_configs
        # TODO redo with module instance names instead of module classes (current: does not work with multiple instances of a model class)
        # just create a graph and turn not requested features on
        requested_features = []
        provided_features = []
        extensions = {}

        # TODO handle extension features

        provided_and_required_features_per_model = {}
        features_to_turn_on = {}  # turn all on that provide feature, TODO depend on config or feature type


        self.extract_and_group_features(extensions, features_to_turn_on, self.run_config.supervisor.run_time_manager_config, self.run_config.supervisor.run_time_manager_class, RUN_TIME_MANAGER,
                                          provided_and_required_features_per_model, provided_features,
                                          requested_features)

        # get required and provided features
        for module_name, module_config in module_configs.items():
            module_info = module_config.module_info
            self.extract_and_group_features(extensions, features_to_turn_on, module_config, module_info, module_name,
                                              provided_and_required_features_per_model, provided_features, requested_features)

        # apply feature configs (assign and remove features)


        # find features to turn on, so that all required features are satisfied
        feature_requests_to_satisfy = requested_features.copy()
        while feature_requests_to_satisfy:
            # TODO link reason why it needs to turned on, feature_nes, module_class etc.
            feature_name, (req_feature, feature_nes) = feature_requests_to_satisfy.pop()
            if not self.find_satisfying_features(req_feature, features_to_turn_on.values()):
                satisfying_features = self.find_satisfying_features(req_feature, provided_features)
                if not satisfying_features:
                    # TODO if config contains setting for find modules for not provided feature then find feature from known module pool
                    raise ValueError(f"Feature {req_feature} cannot be satisfied. Currently module search for features is not implemented.")
                for new_req_feature in satisfying_features:
                    print("----", new_req_feature)
                    features_to_turn_on[new_req_feature.name] = new_req_feature
                    # todo create new feature_nes based on old one and adapt it to new_feature_req
                    additional_required_features = list(filter(lambda req_f: not isinstance(req_f[1][0], ExtensionFeature), map(lambda req_f: (req_f.name, (req_f, feature_nes)), new_req_feature.requirements)))
                    # maybe: add complete feature dependency during satisfaction search, to link required communication feature by extension feature to module (until then add communication feature to requirements with extentsion)
                    # or just add them in the extension for loop below to the config / ft_info
                    feature_requests_to_satisfy.extend(additional_required_features)
                    requested_features.extend(additional_required_features)

        feature_graph = FeatureGraph()
        for module_name, (r_features, p_features) in provided_and_required_features_per_model.items():
            feature_graph.add_module(module_name)
            turn_on_features = {
                feature.name: (feature, self.create_feature_necessity_for_module(feature, requested_features)) for feature in features_to_turn_on.values()
                if feature.name in r_features or feature.name in p_features
            }
            for feature, _ in turn_on_features.values():
                feature_graph.connect_module_and_feature(module_name, feature, is_request=feature.name in r_features)
            if module_name == RUN_TIME_MANAGER:
                self.run_config.supervisor.run_time_manager_config.module_config_hull.update_ft_info(turn_on_features, p_features, r_features)
                continue
            module_configs[module_name].update_ft_info(turn_on_features, p_features, r_features)

        # Extension setup
        shared_extension_setups = {}
        for module_name, extension_features in extensions.items():
            if module_name == RUN_TIME_MANAGER:
                continue
            setups = {}
            for extension_name, (extension_request, extension_necessity) in extension_features.items():
                # TODO necessity
                feature_graph.connect_module_and_feature(module_name, extension_request, is_request=True)
                if extension_request.shared_extension_setup and extension_name in shared_extension_setups:
                    setups[extension_name] = (shared_extension_setups[extension_name], extension_request)
                    shared_extension_setups[extension_name].add_assigned_module(module_configs[module_name])
                else:
                    setup_class: Type[ExtensionSetup] = extension_request.extension_setup_class
                    self.update_general_plus_config_with_defaults(setup_class.run_config_general_plus_keys_and_defaults())
                    extension_setup = setup_class(self.run_config)
                    extension_setup.before_ach_start(module_configs)
                    extension_setup.check_run_config(self.run_config)
                    setups[extension_name] = (extension_setup, extension_request)
                    if extension_request.shared_extension_setup:
                        shared_extension_setups[extension_name] = extension_setup
                    extension_setup.add_assigned_module(module_configs[module_name])
                # TODO update enabled extension features?
            module_configs[module_name].module_extension_setups = setups

        # feature_graph.debug_plot_graph(self.run_config.general.agent_name)
        created_plot = feature_graph.plot(location=self.run_config.general.get_instance_dir(), agent_name=self.run_config.general.agent_name)
        if created_plot:
            logger.info("Created feature graph plot in agent directory")
        else:
            logger.info("Did not create feature graph.")
        return feature_graph, module_configs

    def extract_and_group_features(self, extensions, features_to_turn_on, module_config, module_info, module_name,
                                     provided_and_required_features_per_model, provided_features, requested_features):
        """Extract required and provided features of a module and its extensions and filter them by extension features and check required as turn on features.

        Args:
            extensions: store for all found extensions in the arch.
            features_to_turn_on: store for all features to turn on.
            module_config: required for method that gets features.
            module_info: the object that has the info about the features.
            module_name: the name of the module (indirect also stored in the config)
            provided_and_required_features_per_model: all features of all modules.
            provided_features: all provided features of all modules.
            requested_features: all requested features of all modules

        Returns:
            None
        """
        r_features = module_info.requires_features(module_config) or {}
        p_features = module_info.provides_features(module_config) or {}
        extensions[module_name] = dict(filter(lambda f: isinstance(f[1][0], ExtensionFeature), r_features.items()))
        for extension_name, (extension_request, extension_necessity) in extensions[module_name].items():
            p_features.update(extension_request.extension_setup_class.provides_features() or {})
            r_features.update(extension_request.extension_setup_class.requires_features() or {})
        requested_features.extend(list(filter(lambda f: not isinstance(f[1][0], ExtensionFeature), r_features.items())))
        # TODO add requirement features for provided features with necessity required to requested feature
        provided_and_required_features_per_model[module_name] = (r_features, p_features)
        provided_features.extend(map(lambda f: f[0], p_features.values()))
        for p_f_name, (p_f, p_f_n) in p_features.items():
            if p_f_n.is_required():
                features_to_turn_on[p_f_name] = p_f

    def create_feature_necessity_for_module(self, feature: tuple[Feature, FeatureNecessity], requested_features: list[tuple[Feature, FeatureNecessity]]) -> FeatureNecessity:
        """The feature necessity of turned on features should represent in the future the dependency from which necessity and requirement they are turned on.

        Args:
            feature: feature and its necessity
            requested_features: the new requested feature.

        Returns:
            the new created feature necessity.
        """
        # TODO maybe remove because of the removal of feature requests. (now only interesting to add the dependency)
        return FeatureNecessity()

    def find_satisfying_features(self, feature_request: Feature, provided_features: Iterable[Feature]) -> Iterable[Feature]:
        """Filter for features that satisfy / are convertible / are the same for a requested feature.

        Args:
            feature_request: the feature that is requested.
            provided_features: the available features.

        Returns:
            features that satisfy the requested feature.
        """
        # TODO include config parameters
        # TODO check compatibility -> competing -> find best based on promises (in and out)
        return filter(lambda prov_feat: prov_feat.is_convertable_to_feature(feature_request), provided_features)

    def find_module_for_feature(self, feature):
        """Future: find a module in installed and/or online db / entry points that provide requested features that are not satisfied by the defined modules.

        Args:
            feature: the feature to satisfy.

        Returns:
            In the future: info to load / install module.
        """
        # TODO
        ...

    def setup_communication_and_parallelism(self, module_configs: dict[ModuleName, ModuleConfig], arch_config: ArchConfig) -> tuple[CommunicationGraph, ConcurrencyLevel]:
        com_prefs = arch_config.communication.communication_prefs
        concurrency = ConcurrencyLevel(arch_config.concurrency)
        if not all(concurrency in pref.extra_categories for pref in com_prefs):
            concurrency = ConcurrencyLevel.MultiProcessing

        com_graph = CommunicationGraph()
        used_services: dict[ServiceName, CommunicationServiceInfo] = {}

        self.run_config.settings.topic_prefix = self.setup_config.topic_naming_policy(self.run_config, "").id()[:-1]

        for module_name, module_config in module_configs.items():
            turned_on_com_features: Iterable[CommunicationFeature] = filter(lambda f: isinstance(f, CommunicationFeature) and f.name in module_config.ft_info.turn_on_features, module_config.ft_info.p_features.values())
            module_com_info, services = self.update_com_graph(module_name, turned_on_com_features, com_prefs, com_graph)
            module_config.com_info = module_com_info

            used_services.update({s.name: s for s in services})

        # same for possible run time manager features
        rtm_turned_on_com_features = self.run_config.supervisor.run_time_manager_config.module_config_hull.ft_info.p_features.values()
        self.run_config.supervisor.run_time_manager_config.module_config_hull.general_run_config = self.run_config.general
        self.run_config.supervisor.run_time_manager_config.module_config_hull.com_info, services = self.update_com_graph(RUN_TIME_MANAGER, rtm_turned_on_com_features, com_prefs, com_graph)
        used_services.update({s.name: s for s in services})

        # for module_class, module_config in self.modules_with_configs.items():
        #     com_features: list[CommunicationFeature] = list(filter(lambda f: isinstance(CommunicationFeature, f), module_class.enabled_features(module_config).keys()))
        #     module_input_promises[module_class] = list(map(lambda cf: cf.required_input, com_features))
        #     module_output_promises[module_class] = list(map(lambda cf: cf.provided_output, com_features))

        # TODO solve, based on communication unit-> service requirements

        # TODO setup topic / hierarchy topic based on config

        # set topic: Topic and communication_service: str fields of promises

        # for module_class, module_config in self.modules_with_configs.items():
        #     module_config.add_communication_promise_service_mapping(module_input_promises[module_class], module_output_promises[module_class])

        # TODo based on config and check communication service requirements
        for service_info in used_services.values():
            self.update_general_plus_config_with_defaults(service_info.setup.run_config_general_plus_keys_and_defaults())
        self.run_config.settings.communication_setups = {service_name: service_info.setup(com_graph, self.run_config) for service_name, service_info in used_services.items()}

        for _, com_setup in self.run_config.settings.communication_setups.items():
            com_setup.before_arch_start()
        # com_graph.debug_plot_graph(self.run_config.general.agent_name)
        created_plot = com_graph.plot(location=self.run_config.general.get_instance_dir(), agent_name=self.run_config.general.agent_name)
        if created_plot:
            logger.info("Created communication graph plot in agent directory")
        else:
            logger.info("Did not create communication graph.")
        return com_graph, concurrency

    def update_com_graph(self, module_name, turned_on_com_features, com_prefs, com_graph) -> tuple[ModuleCommunicationInfo, list[CommunicationServiceInfo]]:
        """Update the communication graph for a module name and it's turned on com features.

        Args:
            module_name: the module that is added.
            turned_on_com_features: the turned on com features for the module
            com_prefs: the preferences of communication services that should be used.
            com_graph: the communication graph to update.

        Returns:
            the module communication info for the module and the used services.
        """
        com_graph.add_module(module_name)
        for com_feature in turned_on_com_features:
            for input_prom in com_feature.required_input:
                self.setup_com_promise(com_prefs, input_prom, module_name)
                com_graph.add_promise(module_name, input_prom, is_module_output=False)
            for output_prom in com_feature.provided_output:
                self.setup_com_promise(com_prefs, output_prom, module_name)
                com_graph.add_promise(module_name, output_prom, is_module_output=True)
        in_services, out_services, services = com_graph.get_in_out_com_infos_per_module(module_name)
        in_promises, out_promises = com_graph.get_in_out_promises_per_module(module_name)
        if services:
            in_wrapper = defaultdict(None,
                                     {p.settings.topic.name: p.communication_unit.payload_wrapper for p in in_promises})
            out_wrapper = defaultdict(None, {p.settings.topic.name: p.communication_unit.payload_wrapper for p in
                                             out_promises})
            module_com_info = ModuleCommunicationInfo(
                services=services,
                in_wrapper=in_wrapper,
                out_wrapper=out_wrapper,
                leaf_to_topic=defaultdict(None, {p.settings.topic.name: p.settings.topic for p in
                                                 in_promises + out_promises}),
                leaf_to_promise=defaultdict(None, {p.settings.topic.name: p for p in in_promises + out_promises})
            )
        else:
            module_com_info = ModuleCommunicationInfo(
                services=[],
                in_wrapper=defaultdict(),
                out_wrapper=defaultdict(),
                leaf_to_topic=defaultdict(),
                leaf_to_promise=defaultdict()
            )
        return module_com_info, services

    def setup_com_promise(self, com_prefs, promise, module_name):
        """Find a service for a communication promise and set the topic fro the promise, update the promise info.

        Args:
            com_prefs: the communication services available.
            promise: the communication promise to setup.
            module_name: the module the promise is from

        Returns:
            None
        """
        # TODO select com_service based on which meets the most requirements

        # TODO get attribute from promise and check with it required_com_attr_groups
        com_service_info = self.find_com_service_for_requirements(promise.required_com_attr, promise.communication_unit.required_service_categories, com_prefs)
        if com_service_info is None:
            raise Exception(f"Cannot find a CommunicationService that meets the requirements for {promise.communication_unit.required_service_categories} from {promise.communication_unit} com_unit for {module_name} module")
        topic = self.setup_config.topic_naming_policy(self.run_config, promise.suggested_leaf_topic)
        promise.set_settings(PromiseSettings(topic, com_service_info))

    @staticmethod
    def find_com_service_for_requirements(
            required_com_attr: dict[ComServiceAttrName, Type[CommunicationServiceAttribute]], required_categories_unit: list[set[ComServiceAttrCategory]], com_prefs: list[CommunicationServiceInfo], find_additional=False) -> CommunicationServiceInfo | None:
        """Solve the requested communication attributes to a communication service.

        Args:
            required_com_attr: from the promise.
            required_categories_unit: from the promise.
            com_prefs: available services (info).
            find_additional: in the future find services that are not listed in the com pref no one solves the requirements.

        Returns:
            the first service info that satisfies the requirements.
        """
        required_attrs = set(required_com_attr.keys())
        for service_info in com_prefs:
            if required_attrs.issubset(service_info.attributes.keys()):
                for req_category_set in required_categories_unit:
                    for group_name, info_cat_group in service_info.get_category_groups().items():
                        if req_category_set.issubset(info_cat_group):
                            break
                    else:
                        break

                else:
                    return service_info

    def update_general_plus_config_with_defaults(self, defaults: dict[str, Any]):
        """Setup the general part of the run config with defaults.

        Args:
            defaults: the defaults to use when they are not present in the config.

        Returns:
            None
        """
        plus_default = {default_key: defaults[default_key] for default_key in set(defaults).difference(self.run_config.general.plus)}
        self.run_config.general.plus.update(plus_default)




