"""
InitPreparer  / SupervisorPreparation
- converts / parses configs
- prepares, initializes & starts ArchModuleSupervisor
- handles wrong config formats & wrong manager/supervisor configs to output
"""
import time
from pathlib import Path
from typing import MutableMapping, Type

from loguru import logger

from aaambos.core.configuration.arch_config import ArchConfig
from aaambos.core.configuration.global_aaambos_config import GlobalAaaambosConfig
from aaambos.core.configuration.nested_config import NestedConfigParser
from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.supervision.arch_supervisor import ArchSupervisor


class InitPreparer:  # InitService, SupervisorInitiator
    """Parses the run and arch config and starts the Supervisor in the init."""
    def __init__(self, run_config_path: Path, arch_config_path: Path, run_options: MutableMapping):
        """Runs the architecture.

        Args:
            run_config_path: the path to the run config provied via cli argument.
            arch_config_path: the path to the arch config provied via cli argument. 
            run_options: additional config that override the run config from the cli argument.
        """
        # TODO add location of run_config to local path?
        self.global_config = GlobalAaaambosConfig.load()

        self.config_parser = NestedConfigParser()  # configurable via argument?
        logger.trace("Parse run_config")
        run_config_dict = self.load_config(run_config_path, run_options)
        if "run_config_class" in run_config_dict:
            run_config_class = run_config_dict["run_config_class"]
        else:
            run_config_class = RunConfig # TODO change based on system conventions
        run_config = run_config_class(**run_config_dict)

        self.global_config.update_run_config(run_config)

        if run_config.general.always_unique_instance:
            run_config.general.instance = f"{run_config.general.instance}_{time.strftime('%Y%m%d-%H%M%S')}"
        arch_config_class = run_config.architecture.arch_config if run_config.architecture else ArchConfig
        logger.trace("Parse arch_config")
        arch_config = arch_config_class(**self.load_config(arch_config_path))

        self.global_config.update_arch_config(arch_config)

        run_config.integrate_arch_config(arch_config=arch_config)

        supervisor = self.get_supervisor(run_config)
        # setup arch_config based on arch_config class from architecture / architecture
        logger.info(f"Start supervisor {supervisor.__class__.__name__!r}")
        supervisor.run(arch_config)

    def get_supervisor(self, run_config) -> ArchSupervisor:
        """Get the supervisor class from the run config.

        Args:
            run_config: The run_config

        Returns:
            the supervisor class.
        """
        return run_config.supervisor.supervisor_class(run_config)

    def check_supervisor_config(self, supervisor_class: Type[ArchSupervisor], **kwargs):
        """Part where the supervisor config could be checked and raise an Error when it is not valid.

        Args:
            supervisor_class: the extracted suoervisor class
            **kwargs: further arguments with the config ?

        Returns:
            None

        Raises:
            Error if the config is not valid.
        """
        ...

    def load_config(self, config_path: Path, overrides: MutableMapping | None = None) -> MutableMapping:
        """Load a config given the path and some overrides.

        Args:
            config_path: the path to the config.
            overrides: a dict that contains overrides for the config (optional)

        Returns:
            the loaded config as a dict like object.
        """
        return self.config_parser.parse_with_overrides(config_path, overrides)
