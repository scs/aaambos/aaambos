from __future__ import annotations

from loguru import logger


"""
RunTimeManager
(=> Mediator?)
- own individual process, that overwatches / manages the other modules /
- gets feature graph / config from module initiator
- depends on config
- receives start-init msgs
- sends all-online msgs
- coordinates run time error msgs from modules
- stores protocol about module history

- creates communication graph
"""
from abc import abstractmethod
from enum import Enum, auto
from typing import Any, TYPE_CHECKING

from attrs import define

if TYPE_CHECKING:
    from aaambos.core.communication.graph import CommunicationGraph
    from aaambos.core.configuration.module_config import ModuleConfig
    from aaambos.core.module.base import ModuleName
    from aaambos.core.module.feature import FeatureGraph, FeatureNecessity, Feature, FeatureName
    from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.configuration.run_time_manager_config import RunTimeManagerConfig


class ControlType(Enum):
    Terminate = auto()
    Start = auto()


@define
class ControlMsg:
    control: ControlType
    info: Any


@define
class TerminateInfo:
    cause: str
    module_name: ModuleName


@define
class StartInfo:
    module_name: ModuleName
    config: Any


class RunTimeManager:
    """Base class managing modules by instructing the module runner.

    Different implementations of the manager can be more complex than others. E.g. also receive communication and status information from the modules.
    See in the std lib part of aaambos (`aaaambos.std.supervision.instruction_run_time_manager.instruction_run_time_manager.InstructionRunTimeManager`).
    """

    def __init__(self, run_config: RunConfig, module_configs: dict[ModuleName, ModuleConfig]):
        """Set the attributes of the run time manager. The module runner will be set by the module runner itself.

        Args:
            run_config: agent name etc.
            module_configs: the configs of the modules to manage (all modules of the architecture).
        """
        # TODO own process / setup own communication
        self.run_config = run_config
        self.module_configs = module_configs
        self.module_runner = ...

    @abstractmethod
    def get_step_duration(self) -> float:
        """Provide the minimal duration in seconds the step function should be called.

        Returns:
            duration in seconds 
        """
        ...

    @abstractmethod
    def step(self):
        """Iteratively called.

        Returns:
            None
        """
        ...

    @abstractmethod
    def handle_module_termination(self, module_name: ModuleName, termination_cause):
        """Callback from the module runner when a module terminated.

        Args:
            module_name: the name of the module that terminated.
            termination_cause: the termination cause, e.g. int exitcode.

        Returns:
            None
        """
        ...

    @abstractmethod
    def terminate(self):
        """Terminate the run time manager.

        Returns:
            None
        """
        ...

    @abstractmethod
    def terminate_all_modules(self):
        """Terminate all modules indicates that the supervisor wants to terminate. Not used in the current version.

        Returns:
            None
        """
        ...

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        """For a more complex run time manager that also communicates to modules.

        Args:
            config: placeholder

        Returns:
            the provided features, similar to the same function in a Module.
        """
        return {}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        """For a more complex run time manager that also communicates to modules.

        Args:
            config: placeholder.

        Returns:
            the required features, similar to the same function in a Module.
        """
        return {}

    @classmethod
    def create_config(cls, config_dict: dict) -> RunTimeManagerConfig:
        """Create a suitable RunTimeManagerConfig from a config dict parsed by a config parser.

        Args:
            config_dict: the content to put in the config.

        Returns:
            the config object.
        """
        return RunTimeManagerConfig(**config_dict)


class SimpleRunTimeManager(RunTimeManager):
    """A simple RunTimeManager that starts every module if it terminated with the exitcode 1."""
    def __init__(self, run_config: RunConfig, module_configs: dict[ModuleName, ModuleConfig]):
        super().__init__(run_config, module_configs)
        self.restart_modules = []
        self.is_termination_process_running = False
        # module (_name) to status

    def get_step_duration(self) -> float:
        return 0.5

    def step(self):
        pass

    def handle_module_termination(self, module_name: ModuleName, termination_cause):
        """ Restart module with the termination cause 1.

        Args:
            module_name:
            termination_cause:

        Returns:

        """
        logger.info(f"Handle termination of module {module_name!r}")
        if self.is_termination_process_running:
            return
        match termination_cause:
            case 0:
                pass
            case 1:
                if module_name not in self.restart_modules and module_name in self.module_configs and self.module_configs[module_name].restart_after_failure:
                    # adapt config so that the module knows that it is a restarted one / has a terminated sibling?
                    logger.info(f"Restart module {module_name!r}")
                    self.module_runner.start_module(ControlMsg(ControlType.Start, StartInfo(module_name, config=self.module_configs[module_name])))  # other name?
        # inform other modules based on feature graph about termination. Decide based on config and termination cause if the runner schould restart the module

    def terminate(self):
        ...

    def terminate_all_modules(self):
        self.is_termination_process_running = True
        for module_name in self.module_configs:
            self.module_runner.terminate_module(ControlMsg(ControlType.Terminate, TerminateInfo("Shutdown", module_name)))
        self.terminate()
