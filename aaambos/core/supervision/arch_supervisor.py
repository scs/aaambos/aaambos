from __future__ import annotations

from collections import defaultdict
from datetime import datetime

import yaml
from attrs import asdict

from aaambos.core.configuration.pre_execution_config import PreExecutionConfig
from aaambos.core.execution.process_based import ProcessRunner
from aaambos.core.logging.executed_agents import ExecutedAgents
from aaambos.core.logging.setup import LoggerSetup
from aaambos.core.supervision.pre_execution import PreExecution

"""
ArchModuleSupervisor
- collects Mediator config
- collects run variant config
- collects module configs
- starts modules
- starts ModuleRunTime Manager
- handles module unexpected doings


Module Runner
Module Creation (Dep Checker)
ModuleRunTimeManager

"""
import os
import time
import signal
from typing import TYPE_CHECKING, Tuple

from loguru import logger

if TYPE_CHECKING:
    from aaambos.core.configuration.arch_config import ArchConfig
    from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.execution.thread_based import ThreadRunner
from aaambos.core.supervision.module_runner import ModuleRunner


RUN_CONFIG_RECORD_NAME = "run_config_record.yml"
ARCH_CONFIG_RECORD_NAME = "arch_config_record.yml"


class ArchSupervisor:
    """Delegator of the setup and supervision to ArchSetup, ModuleRunner and RunTimeManager."""

    def __init__(self, run_config: RunConfig):
        """Set the run config.

        Args:
            run_config: info about the run.
        """
        self.run_config: RunConfig = run_config
        self.arch_config: ArchConfig = ...
        self.setup_solver = ...

        # choose based on setup solver concurrency from architecture / architecture
        self.module_runner = ...
        self.run_time_manager = ...
        self.pre_executor: list[PreExecution] = ...
        ...

    def run(self, arch_config: ArchConfig):
        """Setup of other supervision classes and start the run of the architecture.

        Args:
            arch_config: the info about the specific architecture to run.

        Returns:
            Never (Keyboard Interrupt)
        """
        # loop = asyncio.get_event_loop()
        os.setpgrp()
        self.arch_config = arch_config

        self.setup_solver = self.run_config.supervisor.setup_class(self.run_config.supervisor.setup_config, self.run_config)

        self.setup_solver.create_directories()
        LoggerSetup.add_file_logger(logger, self.run_config.logging, "supervisor")

        logger.trace("Merge module configs")
        module_configs = self.setup_solver.merge_module_configs(arch_config)

        logger.trace("Setup features")
        feature_graph, _ = self.setup_solver.setup_features(module_configs)  # {module_config.module_class: module_configs for module_config in module_configs.values()})  # (module_configs)
        self.run_config.settings.feature_graph = feature_graph
        logger.trace("Setup communication")
        communication_graph, concurrency = self.setup_solver.setup_communication_and_parallelism(module_configs, arch_config)
        self.run_config.settings.communication_graph = communication_graph

        # record run
        ExecutedAgents.add_agent(
            date=datetime.now(),
            agent_id=self.run_config.general.get_instance_id(),
            directory=self.run_config.general.get_instance_dir()
        )
        self.record_configs()

        self.pre_execution()

        self.run_time_manager = self.run_config.supervisor.run_time_manager_class(self.run_config, module_configs)
        logger.info(f"Started run time manager: {self.run_time_manager.__class__.__name__!r}")
        self.module_runner: ModuleRunner = concurrency.to_process_runner(concurrency)(self.run_config)
        self.module_runner.set_run_time_manager(self.run_time_manager)
        logger.info(f"Started module runner: {self.module_runner.__class__.__name__!r}")

        logger.trace("Check modules with the module runner")
        self.module_runner.check_modules(module_configs)  # TODO change module_runner arg

        logger.trace("Start modules with the module runner")
        self.module_runner.start_modules(module_configs)  # TODO change module_runner arg

        # TODO set signal (int) for main process

        step_duration = min(self.run_time_manager.get_step_duration(), self.module_runner.get_step_duration())
        logger.info(f"Start supervisor loop with {step_duration=}")
        try:
            while True:
                self.module_runner.step()
                # loop.run_until_complete(self.run_time_manager.step())
                time.sleep(step_duration)
        except (KeyboardInterrupt, SystemExit):
            logger.warning("Received KeyboardInterrupt")
            logger.warning("Give modules 200 milliseconds to terminate")
            time.sleep(0.2)
            for _, com_setup in self.run_config.settings.communication_setups.items():
                com_setup.terminate()
            # if isinstance(self.module_runner, ThreadRunner):
            #     logger.warning("Terminate all modules. Wait 300 milliseconds before continuation")
            #     self.run_time_manager.terminate_all_modules()
            #     time.sleep(0.3)
            logger.warning("Terminate run time manager")
            self.run_time_manager.terminate()
            logger.warning("Terminate module runner")
            self.module_runner.terminate()
            if self.pre_executor:
                logger.warning("Terminate pre executions. Wait 100 milliseconds before continuation")
                for pre_executor in self.pre_executor:
                    logger.warning(f"Terminate pre executor {pre_executor.__class__.__name__}")
                    pre_executor.terminate()
                time.sleep(0.1)
        logger.warning("Exit program")
        try:
            # try to kill all still alive child processes and or threads.
            os.killpg(0, signal.SIGINT)
        except KeyboardInterrupt:
            pass
        finally:
            exit(0)

    def pre_execution(self):
        self.pre_executor = []
        if self.run_config.pre_start:
            logger.trace("Do Pre Execution")
            grouped_pre_exec = self.group_pre_executions()
            for executor_class, group in grouped_pre_exec:
                executor = executor_class(pre_execution_configs=group, supervisor=self)
                self.pre_executor.append(executor)
            time_to_wait = 0
            for executor in self.pre_executor:
                time_to_wait = max(executor.pre_execute(), time_to_wait)
            logger.info(f"Sleep for {time_to_wait} due to pre execution configuration")
            time.sleep(time_to_wait)

    def group_pre_executions(self) -> list[Tuple[type[PreExecution], list[PreExecutionConfig]]]:
        grouped = defaultdict(list)
        for name, pre_exec in self.run_config.pre_start.items():
            grouped[pre_exec.executor_class.__name__].append(pre_exec)

        return [(pre_exec_group[0].executor_class, pre_exec_group) for pre_exec_group in grouped.values()]

    def record_configs(self):
        logger.trace(f"Record run and arch config in {self.run_config.general.get_instance_dir()}")
        with open(self.run_config.general.get_instance_dir() / ARCH_CONFIG_RECORD_NAME, "w") as file:
            file.write(yaml.dump(asdict(self.arch_config)))

        with open(self.run_config.general.get_instance_dir() / RUN_CONFIG_RECORD_NAME, "w") as file:
            file.write(yaml.dump(asdict(self.run_config)))

