"""
The supervision part of *aaambos*

### `aaambos.core.supervision.arch_setup`
Setups the architecture, solves feature dependencies, communications servies etc.

### `aaambos.core.supervision.arch_supervisor`
The supervisor that handles the arch_setup, module_runner and run_time_manager.

### `aaambos.core.supervision.init_preparer`
Prepares the Supervisor, instantiates it and runs it.

### `aaambos.core.supervision.module_runner`
The base class of all module runners. And the base class for the "independent" module runner (e.g. Multiprocessing, and Threads).
Module runners starts the processes, threads, etc. for each module and checks if they are alive.

### `aaambos.core.supervision.run_time_manager`
The base clss of RunTimeManaer and a simple version of it (just restarting failed modules.
The RunTimeManager decides which modules are terminated and restarted, etc. The conenction to a GUI and that can receive and send messages into the architecture is located in the std library.

"""