from __future__ import annotations


"""
ModuleRunner (Runs and checks all Modules)

- abstract (multiprocess/single)
- starts and holds modules (/processes)
- communicates with run-time-manager (cannot load / start, decision to kill, restart etc.)
"""
import asyncio
import inspect
from abc import abstractmethod
from asyncio import Task, Future
from multiprocessing import Queue, Process
from typing import Type, Callable, TYPE_CHECKING

from attrs import define, field
from loguru import logger

if TYPE_CHECKING:
    from aaambos.core.configuration.run_config import RunConfig
    from aaambos.core.configuration.arch_config import ArchConfig
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import ModuleName
from aaambos.core.supervision.run_time_manager import ControlMsg, RunTimeManager, ControlType, TerminateInfo


class ModuleRunner:
    """Base class for running the modules."""
    # required ratio for computation time (update step)
    # step frequency, config ?

    def __init__(self, run_config: RunConfig):
        self.run_config = run_config
        self.run_time_manager = ...

    @abstractmethod
    def check_modules(self, modules):
        """Check if all modules can be started and run with this module runner.

        Args:
            modules: the modules to check.

        Returns:
            None
        
        Raises:
            Error if a module can not be started or run with the ModuleRunner.
        """
        ...

    @abstractmethod
    def start_modules(self, modules: dict[ModuleName, ModuleConfig]):
        """Start the given modules.

        Args:
            modules: the configs of the modules

        Returns:
            None
        """
        # guarantees run_time_manager is set
        ...

    @abstractmethod
    def get_step_duration(self) -> float:
        """Provide the minimal duration in seconds the step function should be called.

        Returns:
            duration in seconds 
        """
        ...

    @abstractmethod
    def step(self):
        """Check iteratively the state of the modules/processes.

        Returns:
            None
        """
        ...

    @abstractmethod
    def start_module(self, control_msg: ControlMsg):
        """Callback function to start a module from the run time manger with a ControlMsg.

        Args:
            control_msg: information which module to start.

        Returns:
            None
        """
        ...

    @abstractmethod
    def terminate_module(self, control_msg):
        """Callback function to terminate a module from the run time manager with a ControlMsg.

        Args:
            control_msg: informmation which module to terminate and why.

        Returns:
            None
        """
        ...

    @abstractmethod
    def restart_module(self, control_msg: ControlMsg):
        """Callback function to restart a module from the run time manager with a ControlMsg.

        Args:
            control_msg: information which module to restart and why.

        Returns:
            None
        """
        ...

    def set_run_time_manager(self, run_time_manager: RunTimeManager):
        """Couple the run time manager with the module runner.

        Args:
            run_time_manager: run time manager to set and to adapt.

        Returns:
            None
        """
        self.run_time_manager = run_time_manager
        self.run_time_manager.module_runner = self

    @abstractmethod
    def terminate(self):
        """Terminate the module runner and all modules.

        Returns:
            None
        """
        ...

class AsyncTask:
    """Wrapper that is similar to multiprocessing.Process and Threads but for the asynchronous Task.
    
    At the moment asynchronous is not implemented. Therefore the class is not used.
    """

    def __init__(self, target, name, args=(), kwargs=...):
        self.kwargs = {} if kwargs is ... else kwargs
        self.target = target
        self.name = name
        self.args = args
        self.task: Task | None = None
        self.exitcode = None

    async def start(self):
        self.task = asyncio.create_task(self.target(*self.args, **self.kwargs), name=self.name)
        self.task.add_done_callback(self.done_callback)

    async def done_callback(self, future: Future):
        self.exitcode = await future.result()

    async def is_alive(self):
        return await self.task is None or self.task.done()


QueueType = Queue   #| asyncio.Queue
ExecutionType = Process   #| Thread | AsyncTask


@define
class ModuleExecution:
    """Data for one module, either Process, Thread or AsyncTask data."""
    execution: ExecutionType
    queue: QueueType
    is_coroutine: field(type=bool, default=False)


class IndependentRunner(ModuleRunner):
    """A base class that can be used to run the modules in Processes or Threads.

    Maybe in the future also Async modules.
    """

    def __init__(self, run_config, execution_class: Type[ExecutionType], queue_class: Type[QueueType], terminate_with_module: bool):
        """Set the attributes of the module runner.

        Args:
            run_config: info about agent name, etc.
            execution_class: either Process or Thread.
            queue_class: the multiprocessing Queue, multithreading Queue or asyncio Queue.
            terminate_with_module: when the module terminates, should the sys.exit be called and should be a sig handler be registered for each module.
        """
        super().__init__(run_config)
        self.min_step_duration = 0.5
        self.module_processes: dict[ModuleName, ModuleExecution] = {}
        self.module_restart = {}
        self.execution_class = execution_class
        self.queue_class = queue_class
        self.terminate_with_module = terminate_with_module

    def check_modules(self, modules):
        pass

    def start_modules(self, modules):
        for module, config in modules.items():
            logger.info(f"Prepare module process {module!r}")
            self.module_processes[config.name] = self.prepare_process(config)

        # TODO inform module_runner
        for module_name, module_process in self.module_processes.items():
            logger.info(f"Start module process {module_name!r}")
            #if module_process.is_coroutine:
            #    await module_process.execution.start()
            #else:
            module_process.execution.start()
        # set max_mean_step_time_from config

    def get_step_duration(self) -> float:
        return self.min_step_duration

    def step(self):
        modules_to_restart = []
        modules_to_delete = []
        for module_name, module_process in self.module_processes.items():
            is_alive = module_process.execution.is_alive()
            if not is_alive:
                logger.info(f"Module process died {module_name!r} with exitcode {module_process.execution.exitcode}")
                modules_to_delete.append(module_name)
                if module_name in self.module_restart:
                    modules_to_restart.append(module_name)
        for module_name in modules_to_delete:
            module_process = self.module_processes[module_name]
            del self.module_processes[module_name]
            self.handle_termination(module_name, module_process.execution)
        for module_name in modules_to_restart:
            self.start_module(self.module_restart[module_name])  # TODO convert to Start Control Msg
            del self.module_restart[module_name]
        self.run_time_manager.step()

    def start_module(self, control_msg: ControlMsg):
        if control_msg.info.module_name not in self.module_processes:
            self.module_processes[control_msg.info.module_name] = self.prepare_process(control_msg.info.config)
            #if self.module_processes[control_msg.info.module_name].is_coroutine:
            #    await self.module_processes[control_msg.info.module_name].execution.start()
            #else:
            self.module_processes[control_msg.info.module_name].execution.start()
        # TODO else raise ro return error code
        ...

    def terminate_module(self, control_msg: ControlMsg):
        if control_msg.info.module_name in self.module_processes and self.module_processes[control_msg.info.module_name].execution.is_alive():
            #logger.info(f"Terminate module process {control_msg.info.module_name!r}")
            #self.module_processes[control_msg.info.module_name].execution.terminate()
            #if self.module_processes[control_msg.info.module_name].is_coroutine:
            #    await self.module_processes[control_msg.info.module_name].queue.put(control_msg)
            #else:
            logger.info(f"Send termination info to module {control_msg.info.module_name!r}")
            self.module_processes[control_msg.info.module_name].queue.put(control_msg)
        # TODO raise or return error code?

    def restart_module(self, control_msg: ControlMsg):
        self.terminate_module(ControlMsg(ControlType.Terminate, TerminateInfo(cause="Restart", module_name=control_msg.info.module_name)))  # TODO convert to Termination Control MSG
        self.module_restart[control_msg.info.module_name] = control_msg

    def handle_termination(self, module_name: ModuleName, module_process: Process | AsyncTask):
        """Handle the termination of a module. Here: delegate to the run time manager.

        Args:
            module_name: the module that terminated.
            module_process: the process / thread, etc. provides information about the reason for termination.

        Returns:
            None
        """
        self.run_time_manager.handle_module_termination(module_name, module_process.exitcode)

    def prepare_process(self, config: ModuleConfig) -> ModuleExecution:
        """Create the ModuleExecution with all data and setup for the execution, e.g. Process.

        Args:
            config: the config of the module that should be started.

        Returns:
            the ModuleExecution with the execution process/thread/etc. the queue and the info about if it is a coroutine.
        """
        mean_step_duration = 1 / config.mean_frequency_step
        q = self.queue_class()
        p = self.execution_class(target=config.module_info.get_start_function(config), name=config.name,
                                 kwargs={"module_path": config.module_path, "config": config, "control_queue": q,
                            "mean_step_time": mean_step_duration, "terminate_with_module": self.terminate_with_module, "com_setups": self.run_config.settings.communication_setups})
        return ModuleExecution(execution=p, queue=q, is_coroutine=inspect.iscoroutinefunction(p.start))

    def terminate(self):
        for module_name, module_process in self.module_processes.items():
            if module_process.execution.is_alive():
                if hasattr(module_process.execution, "terminate"):
                    logger.warning(f"Terminate still alive module process {module_name!r}",)
                    module_process.execution.terminate()
                else:
                    logger.error("Cannot kill thread!")


