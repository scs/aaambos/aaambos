from __future__ import annotations

from typing import TYPE_CHECKING, Any

from attrs import define, field

from aaambos.core.configuration.com_config import CommunicationConfig
from aaambos.core.configuration.extension_config import ExtensionConfig
from aaambos.core.configuration.feature_config import FeatureConfig
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.configuration.pre_execution_config import PreExecutionConfig
from aaambos.core.configuration.supervisor_config import SupervisorConfig
if TYPE_CHECKING:
    from aaambos.core.architecture.convention import ArchitectureConvention
from aaambos.core.execution.concurrency import ConcurrencyLevel
"""
- based on architecture architecture
- communication prefs
- extension prefs

- execution / concurrency prefs
- from arch preferred setup solver, run time manager 
- different modules with feature necessity, extensions, config etc


"""


@define
class ArchConfig:
    concurrency: ConcurrencyLevel
    communication: CommunicationConfig = field(converter=lambda x: CommunicationConfig(**x))

    modules: dict[str, ModuleConfig]
    pre_start: dict[str, PreExecutionConfig] = field(factory=dict, converter=lambda x: {name: PreExecutionConfig(_name=name, **config) for name, config in x})
    architecture: ArchitectureConvention | None = None
    extensions: dict[str, ExtensionConfig] | None = None
    supervisor: SupervisorConfig | None = None

    features: FeatureConfig = field(default={}, converter=lambda x: FeatureConfig(**x) if isinstance(x, dict) else x)

    description: str = ""
