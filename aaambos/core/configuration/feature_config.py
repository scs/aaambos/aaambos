from functools import partial
from typing import Any

from attrs import define, field

from aaambos.core.module.base import ModuleName, ModuleInfo
from aaambos.core.module.feature import Feature

# TODO based on regex, etc.

# TODO implement in arch setup


@define(kw_only=True)
class FeatureForModuleNamesConfig:
    feature: Feature
    module_names: list[str]


@define(kw_only=True)
class FeatureForModuleInfoConfig:
    feature: Feature
    module_info: list[ModuleInfo]


def convert_dicts(d: dict, c: Any) -> Any:
    if isinstance(d, dict):
        return c(**d)
    return d


def several_convert_dicts(l: list, c: Any) -> list:
    return list(map(partial(convert_dicts, c=c), l))


@define(kw_only=True)
class ScopeOfFeatureConfig:
    all: list[Feature] = field(factory=list)
    by_module_name: list[FeatureForModuleNamesConfig] = field(factory=list, converter=partial(several_convert_dicts, c=FeatureForModuleInfoConfig))
    by_module_info: list[FeatureForModuleInfoConfig] = field(factory=list, converter=partial(several_convert_dicts, c=FeatureForModuleInfoConfig))


@define(kw_only=True)
class FeatureConfig:
    assign_provide: ScopeOfFeatureConfig = field(default={}, converter=partial(convert_dicts, c=ScopeOfFeatureConfig))
    assign_require: ScopeOfFeatureConfig = field(default={}, converter=partial(convert_dicts, c=ScopeOfFeatureConfig))
    remove: ScopeOfFeatureConfig = field(default={}, converter=partial(convert_dicts, c=ScopeOfFeatureConfig))