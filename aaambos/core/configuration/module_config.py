from __future__ import annotations

from collections import defaultdict
from typing import TYPE_CHECKING, Type, Callable, Any

from attrs import define

from aaambos.core.communication.topic import Topic, LeafTopic
from aaambos.core.module.extension import ExtensionSetup

if TYPE_CHECKING:
    from aaambos.core.communication.service import CommunicationServiceInfo
    from aaambos.core.communication.promise import CommunicationPromise
    from aaambos.core.configuration.run_config import GeneralRunPart
    from aaambos.core.configuration.logging_config import LoggingConfig
    from aaambos.core.module.base import ModuleInfo, ModuleName
from aaambos.core.module.feature import FeatureNecessity, Feature, ExtensionFeature, CommunicationFeature, FeatureName

MODULE_CLASS = "module_class"


@define
class ModuleCommunicationInfo:
    services: list[CommunicationServiceInfo]
    in_wrapper: dict[LeafTopic, Type[Any]]
    out_wrapper: dict[LeafTopic, Type[Any]]
    leaf_to_topic: dict[LeafTopic, Topic]
    leaf_to_promise: dict[LeafTopic, CommunicationPromise]
    ...


@define
class ModuleFeatureInfo:
    turn_on_features: dict[str, tuple[Feature, FeatureNecessity]] = None
    r_features: dict[FeatureName, Feature | None] = None
    p_features: dict[FeatureName, Feature | None] = None
    ext_features: dict[FeatureName, ExtensionFeature] = None
    com_features: dict[FeatureName, CommunicationFeature] = None


@define
class ModuleConfig:
    module_path: str
    module_info: Type[ModuleInfo]
    expected_start_up_time: float | int  # to send initialized info to run time manager
    module_import_delay: float | int = 0  # not recommended, instead through communication receive info # i don't know what this was about for, maybe delete later
    module_start_delay: float | int = 0  # not recommended, instead through communication receive info
    mean_frequency_step: float = 2
    restart_after_failure: bool = True

    general_run_config: GeneralRunPart = None
    name: ModuleName = None
    ft_info: ModuleFeatureInfo | None = None
    com_info: ModuleCommunicationInfo | None = None
    module_extension_setups: dict[str, tuple[ExtensionSetup, ExtensionFeature]] | None = None
    logging: LoggingConfig | None = None
    extra_delay: float | int = 0  # set by setup (smallest negative start delay)
    # own_run_loop: bool | Callable = False

    def update_ft_info(self, turn_on_features: dict[FeatureName, tuple[Feature, FeatureNecessity]], p_features, r_features):
        """Update the feature info (ft_info) via the arch setup.

        Args:
            turn_on_features: the features that the module is allowed to turn on.
            p_features: Its provided features.
            r_features: Its required features.

        Returns:
            None
        """
        def turn_on_fts_filter_map(features, filter: Callable[[Feature], bool] = None) -> dict[FeatureName, Feature]:
            return {f_name: turn_on_features[f_name][0]
                    for f_name in features
                    if f_name in turn_on_features and (not filter or filter(turn_on_features[f_name][0]))}

        if not self.ft_info:
            self.ft_info = ModuleFeatureInfo()
            self.ft_info.r_features, self.ft_info.p_features, self.ft_info.ext_features, self.ft_info.com_features = defaultdict(), defaultdict(), defaultdict(), defaultdict(),
        if not self.ft_info.turn_on_features:
            self.ft_info.turn_on_features = turn_on_features
        else:
            self.ft_info.turn_on_features.update(turn_on_features)
        self.ft_info.r_features.update(turn_on_fts_filter_map(r_features))
        self.ft_info.p_features.update(turn_on_fts_filter_map(p_features))
        self.ft_info.ext_features.update(turn_on_fts_filter_map(r_features, lambda ft: isinstance(ft, ExtensionFeature)))
        self.ft_info.com_features.update(turn_on_fts_filter_map(p_features, lambda ft: isinstance(ft, CommunicationFeature)))
        self.ft_info.com_features.update(turn_on_fts_filter_map(r_features, lambda ft: isinstance(ft, CommunicationFeature)))

    def setup(self):
        # give the config the possibility to adapt their content, e.g. load data from extra config files, adapt, etc.
        ...
