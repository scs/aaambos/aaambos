from attrs import define

from aaambos.core.communication.service import CommunicationServiceInfo, ServiceName
from aaambos.core.communication.setup import CommunicationSetup


@define
class CommunicationConfig:
    communication_prefs: list[CommunicationServiceInfo]