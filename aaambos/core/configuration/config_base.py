from typing import Mapping

from attrs import define


CONFIG_FILE = "_config_file"


@define
class Config:

    def update(self, new_items: Mapping, check_attrs: bool = False):
        for key, value in new_items.items():
            if check_attrs and key not in self.__attrs_attrs__:
                continue
            setattr(self, key, value)
