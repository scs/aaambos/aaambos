from typing import Type

from attrs import define, field

from aaambos.core.configuration.module_config import ModuleCommunicationInfo, ModuleConfig


@define
class RunTimeManagerConfig:
    module_config_hull = field(factory=lambda: ModuleConfig("...", None, 0, name="RunTimeManager"))
