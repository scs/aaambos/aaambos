from typing import Callable, Any

from attrs import define, field

from aaambos.core.supervision.pre_execution import ShellPreExecution, PreExecution


@define
class PreExecutionConfig:
    executor_class:  PreExecution = ShellPreExecution
    execute: Any = None
    additional_sleep_time: float | int = 0
    miscellaneous: dict[str, Any] = field(factory=dict)
    name: str = "__auto_set__"
