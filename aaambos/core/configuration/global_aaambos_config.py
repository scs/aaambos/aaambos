from __future__ import annotations

import os
import traceback
from pathlib import Path

import yaml
from attrs import define, field, validators, asdict
from loguru import logger
from platformdirs import user_config_dir

from aaambos import APP_NAME
from aaambos.core.configuration.arch_config import ArchConfig
from aaambos.core.configuration.run_config import RunConfig

CONFIG_NAME = "global_config.yml"


def is_none_or_directory(instance, attribute, value):
    return value is None or isinstance (value, Path) or (isinstance(value, str) and Path(value))


def str_to_bool_converter(value) -> None | bool:
    if value is None:
        return None
    if isinstance(value, bool):
        return value
    assert isinstance(value, str), "value needs to be at this point a string!"
    val = value.lower()
    if val in ('y', 'yes', 't', 'true', 'on', '1'):
        return True
    elif val in ('n', 'no', 'f', 'false', 'off', '0'):
        return False
    else:
        raise ValueError("invalid truth value %r" % (val,))


@define(kw_only=True)
class GlobalAaaambosConfig:
    """Always overwrite configs"""
    local_agent_directories: str | None = field(default=None, validator=is_none_or_directory, converter=lambda x: Path(x) if x is not None else None)
    always_unique_instance: bool | None = field(default=None, validator=validators.instance_of((bool, type(None))), converter=str_to_bool_converter)

    # at best have a converter that converts from a string (CLI and yaml input).

    @classmethod
    def load(cls) -> GlobalAaaambosConfig:
        try:
            if (cls.get_config_file_path()).is_file():
                logger.info(f"Loading global aaambos config from {cls.get_config_file_path()}")
                from hyperpyyaml import load_hyperpyyaml
                with open(cls.get_config_file_path(), "r") as file:
                    file_dict = load_hyperpyyaml(file)
                return cls(**file_dict)
        except Exception as e:
            logger.warning(f"Loading global aaambos config from {cls.get_config_file_path()} failed. Use defaults.")
            logger.warning(traceback.format_exception(e))
        logger.info(f"No global aaambos config file at {cls.get_config_file_path()}. Use defaults.")
        return cls()

    @classmethod
    def create(cls):
        print("")
        if (cls.get_config_file_path()).is_file():
            logger.info(f"Global global aaambos config already exists at {cls.get_config_file_path()}")
            return
        if not cls.global_aaambos_dir().is_dir():
            logger.info(f"Creating global aaambos config directory {cls.global_aaambos_dir()}")
            os.makedirs(cls.global_aaambos_dir(), exist_ok=True)
        logger.info(f"Creating global aaambos config file with default values at {cls.get_config_file_path()}.")
        cls._safe_config(cls())

    @classmethod
    def global_aaambos_dir(cls) -> Path:
        return Path(user_config_dir(appname=APP_NAME, appauthor=False))

    @classmethod
    def get_config_file_path(cls) -> Path:
        return cls.global_aaambos_dir() / CONFIG_NAME

    @classmethod
    def store_value(cls, attribute: str, value):
        if attribute in cls.__annotations__:
            old_config = asdict(cls.load())
            old_config.update({attribute: value})
            # check if validators pass
            logger.info(f"Updating global aaambos config file at {cls.get_config_file_path()} with {attribute}: {value}.")
            if not (cls.get_config_file_path()).is_file():
                cls.create()
            cls._safe_config(cls(**old_config))
        else:
            logger.error(f"Option {attribute} is not part of the global aaambos config. Options: {', '.join(cls.__annotations__.keys())}")

    def update_run_config(self, run_config: RunConfig):
        if self.local_agent_directories is not None:
            logger.info(f"Updating 'local_agent_directories' field in run config based on global config: {self.local_agent_directories}")
            run_config.general.local_agent_directories = self.local_agent_directories
        if self.always_unique_instance is not None:
            logger.info(f"Updating 'always_unique_instance' field in run config based on global config: {self.always_unique_instance}")
            run_config.general.always_unique_instance = self.always_unique_instance

    def update_arch_config(self, arch_config: ArchConfig):
        ...

    @classmethod
    def _safe_config(cls, config: GlobalAaaambosConfig):
        updated_config = asdict(config, value_serializer=lambda i, a, v: str(v) if isinstance(v, Path) else v)
        with open(cls.get_config_file_path(), "w") as file:
            file.write(yaml.dump(updated_config))

    @classmethod
    def clear_config(cls, attribute: str = ...):
        if attribute is ...:
            os.makedirs(cls.global_aaambos_dir(), exist_ok=True)
            logger.info(f"Clearing global aaambos config -> Set all values back to None / Default")
            cls._safe_config(cls())
        if attribute in cls.__annotations__:
            logger.info(f"Clearing attribute {attribute!r} in global aaambos config -> Set the values back to None / Default")
            old_config = asdict(cls.load())
            del old_config[attribute]
            cls._safe_config(cls(**old_config))