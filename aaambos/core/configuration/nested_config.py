from __future__ import annotations

from abc import abstractmethod
from enum import Enum
from os.path import splitext
from pathlib import Path
from typing import MutableMapping, Type


class AbstractNestedConfigFormat(Enum):

    @classmethod
    @abstractmethod
    def from_file_extension(cls, file_extension: str) -> NestedConfigFormat:
        ...


class NestedConfigFormat(AbstractNestedConfigFormat):
    HyperYAML = 1
    YAML = 2
    JSON = 3
    TOML = 4
    INI = 5

    @classmethod
    def from_file_extension(cls, file_extension: str) -> NestedConfigFormat:
        """Map the file extension to the Config Format (this Enum).

        Args:
            file_extension: with the ".", e.g., ".yaml"

        Returns:
            Self
        """
        match file_extension:
            case ".yaml" | ".yml":
                return cls.HyperYAML
            case ".json":
                return cls.JSON
            case ".toml" | ".tml":
                return cls.TOML
            case ".ini" | ".config" | ".cfg":
                return cls.INI
            case _:
                raise ValueError(f"{file_extension=} is not supported by {cls.__name__}. Please provide appropriate "
                                 f"file extension for either {', '.join(map(lambda f: f.name, cls))}")

# for config files as arguments for classes in the HyperPyYaml config


class NestedConfigParser:
    """

    """

    def __init__(self, config_formats: Type[AbstractNestedConfigFormat] = NestedConfigFormat):
        """

        Args:
            config_formats:
        """
        self.config_formats = config_formats

    def detect_format(self, config_file: Path):
        """

        Args:
            config_file:

        Returns:

        """
        return self.config_formats.from_file_extension(splitext(config_file)[-1].lower())

    def parse_with_overrides(self, config_file: Path, overrides: MutableMapping, config_format: NestedConfigFormat = ...) -> MutableMapping:
        """

        Args:
            config_file:
            overrides:
            config_format:

        Returns:

        """
        if config_format is ...:
            config_format = self.detect_format(config_file)
        match config_format:
            case NestedConfigFormat.HyperYAML | NestedConfigFormat.YAML:
                from hyperpyyaml import load_hyperpyyaml
                with open(config_file, "r") as f:
                    return load_hyperpyyaml(f, overrides)
            case NestedConfigFormat.JSON | NestedConfigFormat.TOML | NestedConfigFormat.INI:
                from configparser import ConfigParser
                config = ConfigParser()
                with open(config_file, "r") as f:
                    config.read_file(f)
                config.read_dict(overrides)
                return config
            case _:
                # child class overrides this method.
                # either calls it with super and handles exception with own parser
                # or a complete reimplementation of loading and parsing for all types
                raise ValueError(f"{self.__class__.__name__} cannot handle {config_format.name}. Implement child class "
                                 f"that extends this class or use other format.")
