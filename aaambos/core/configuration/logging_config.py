from __future__ import annotations

from pathlib import Path
from typing import Callable

from attrs import define


@define
class LoggingConfig:
    provide_logger_func: Callable[[LoggingConfig], 'Logger'] | None = None  # is set to loguru_module_logger if no other present
    log_dir_name: str = "logs"
    log_to_files: bool = True
    log_level_command_line: str = "TRACE"
    log_level_logfile: str = "TRACE"

    log_dir: Path | None = None

