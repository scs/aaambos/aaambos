from __future__ import annotations

import os
from pathlib import Path
from typing import Any, TYPE_CHECKING, Type, Callable

from attrs import define, field, Factory

from aaambos.core.communication.graph import CommunicationGraph
from aaambos.core.communication.service import ServiceName
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.configuration.feature_config import FeatureConfig
from aaambos.core.configuration.logging_config import LoggingConfig
from aaambos.core.configuration.pre_execution_config import PreExecutionConfig

if TYPE_CHECKING:
    from aaambos.core.module.feature import FeatureGraph
    from aaambos.core.configuration.arch_config import ArchConfig
from aaambos.core.configuration.supervisor_config import SupervisorConfig
from aaambos.core.configuration.runner_config import RunnerConfig
from aaambos.core.architecture.convention import ArchitectureConvention

"""
- name of the agent,
- name of the run
- directory prefs to use
- architecture
- secret parts like pws / keys etc
- contains optional info about which ArchSupervisor, (SetupSolver, RunTimeManager overrides arch stuff)

- overrides arch args?

"""


@define
class GeneralRunPart:
    agent_name: str
    instance: str
    local_agent_directories: Path
    always_unique_instance: bool = True

    plus: dict[str, Any] = Factory(dict)

    def get_instance_id(self) -> str:
        """Connect the agent name and the instance name to form a instance id.

        Returns:
            The agent name and the instance name connected with a /.
        """
        return f"{self.agent_name}/{self.instance}"

    def get_instance_dir(self) -> Path:
        """Get the path of the instance. Defined by local agent directories, agent_name and instance.

        Returns:
            The path object (local agent dir, agent name and instance)
        """
        return Path(os.path.expanduser(self.local_agent_directories), self.agent_name, self.instance)


@define
class RunConfigSettings:
    # Settings set during runtime
    feature_graph: FeatureGraph | None = None
    communication_graph: CommunicationGraph | None = None
    topic_prefix: str | None = None
    communication_setups: dict[ServiceName, CommunicationSetup] = None


@define
class RunConfig:
    general: GeneralRunPart = field(converter=lambda d: d if isinstance(d, GeneralRunPart) else GeneralRunPart(**d))
    architecture: ArchitectureConvention | None = None
    module_options: dict[str, dict[str, Any]] | None = None
    pre_start: dict[str, PreExecutionConfig] = field(factory=dict, converter=lambda x: {name: PreExecutionConfig(name=name, **config) for name, config in x.items()})
    supervisor: SupervisorConfig = field(default=None, converter=lambda x: SupervisorConfig(**({} if x is None else x)))

    features: FeatureConfig = field(default={}, converter=lambda x: FeatureConfig(**x) if isinstance(x, dict) else x)

    description: str = ""

    logging: LoggingConfig = field(default=None, converter=lambda x: LoggingConfig(**({} if x is None else x)))

    run_config_class: Type[RunConfig] = None

    settings: RunConfigSettings = RunConfigSettings()

    def integrate_arch_config(self, arch_config: ArchConfig):
        """Merge the supervisor config from an arch config.

        The run config options override the arch config parameters.

        Args:
            arch_config: the config to use as a backup.

        Returns:
            None
        """
        if not self.supervisor:
            if arch_config.supervisor:
                self.supervisor = arch_config.supervisor
            else:
                self.supervisor = SupervisorConfig()
        if arch_config.supervisor:
            if not self.supervisor.run_time_manager_config and arch_config.supervisor.run_time_manager_config:
                self.supervisor.run_time_manager_config = arch_config.supervisor.run_time_manager_config
            if not self.supervisor.setup_config and arch_config.supervisor.setup_config:
                self.supervisor.setup_config = arch_config.supervisor.setup_config
            if not self.supervisor.module_runner_config and arch_config.supervisor.module_runner_config:
                self.supervisor.module_runner_config = arch_config.supervisor.module_runner_config

        self.supervisor.run_time_manager_config = self.supervisor.run_time_manager_class.create_config(self.supervisor.run_time_manager_config if self.supervisor.run_time_manager_config else {})
        self.supervisor.setup_config = self.supervisor.setup_class.create_config(self.supervisor.setup_config if self.supervisor.setup_config else {})
        self.supervisor.module_runner_config = RunnerConfig(**(self.supervisor.module_runner_config if self.supervisor.module_runner_config else {}))

        # arch config pre start does not overwrite run pre start values.
        for key in arch_config.pre_start:
            if key not in self.pre_start:
                self.pre_start[key] = arch_config.pre_start[key]