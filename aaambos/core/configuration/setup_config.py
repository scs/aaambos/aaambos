from __future__ import annotations

from typing import Type, Callable, TYPE_CHECKING

from attrs import define

from aaambos.core.communication.topic import Topic, HierarchicalTopic
if TYPE_CHECKING:
    from aaambos.core.configuration.run_config import RunConfig


def agent_name_instance_leaf_hierarchy_topic(config: RunConfig, leaf_topic: str):
    """A policy to create Topics.

    Args:
        config: the run config
        leaf_topic: the leaf topic to use to generate the topic.

    Returns:

    """
    return HierarchicalTopic(config.general.agent_name, config.general.instance, leaf_topic)


@define
class SetupConfig:
    topic_naming_policy: Callable[[RunConfig, str], Topic] = agent_name_instance_leaf_hierarchy_topic
