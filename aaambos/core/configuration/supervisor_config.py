from typing import Type

from attrs import define

from aaambos.core.configuration.run_time_manager_config import RunTimeManagerConfig
from aaambos.core.configuration.runner_config import RunnerConfig
from aaambos.core.configuration.setup_config import SetupConfig
from aaambos.core.supervision.arch_setup import ArchSetup, SimpleArchSetup
from aaambos.core.supervision.arch_supervisor import ArchSupervisor
from aaambos.core.supervision.run_time_manager import RunTimeManager, SimpleRunTimeManager


@define
class SupervisorConfig:
    supervisor_class: Type[ArchSupervisor] = ArchSupervisor
    setup_class: Type[ArchSetup] = SimpleArchSetup
    setup_config: SetupConfig = None
    module_runner_config: RunnerConfig = None
    run_time_manager_class: Type[RunTimeManager] = SimpleRunTimeManager
    run_time_manager_config: RunTimeManagerConfig = None
