from loguru import logger

from aaambos.core.commands.command import Command
from aaambos.core.configuration.global_aaambos_config import GlobalAaaambosConfig


class Set(Command):
    """Set attributes of the `GlobalAaaambosConfig`. Or 'reset' them via name or 'all'."""

    def __init__(self, option, value, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.option = option
        self.value = value

    def execute(self):
        if self.option in ["reset", "default"]:
            if self.value == "all":
                GlobalAaaambosConfig.clear_config()
            elif self.value in GlobalAaaambosConfig.__annotations__:
                GlobalAaaambosConfig.clear_config(self.value)
            else:
                logger.error(f"Invalid value for `aaambos set {self.option}`: {self.value!r}. Either 'all' or one of {list[GlobalAaaambosConfig.__annotations__.keys()]}.")
            return
        if self.value.lower() in ["none", "null"]:
            self.value = None
        GlobalAaaambosConfig.store_value(self.option, self.value)