from abc import ABC, abstractmethod


class Command(ABC):
    """Base class of all commands that defines how the command maps to an internal execution.

    A subclass of this abstract class is linked to a CLI command and starts the execution of the specific command.
    For example the RunCommand starts the InitPreparer.
    """

    def __init__(self, *args, **kwargs):
        """Inheritance specific init. The init kwargs should correlate to the CLI command arguments.

        Args:
            *args: inheritance specific arguments.
            **kwargs: inheritance specific kwargs. (The arguments of the CLI command)
        """
        ...

    @abstractmethod
    def execute(self):
        """Execute the command. 

        Returns:
            None
        """
        ...
