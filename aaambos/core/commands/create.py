"""
## Core behavior
With `aaambos create` you can create aaambos classes, code, and configs for your project / package.
In most cases, it will create a new file based on a template. You can adapt the template by passing the `-n` argument with the new name in CamelCase.
The command will replace all occurrences in CamelCase, snake_case, and SCREAMING_SNAKE_CASE accordingly.

### Pkg
```bash
aaambos create pkg
```
will create a new aaambos pkg. It uses the cookiecutter library and the [cookiecutter aaambos template](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/tree/main?ref_type=heads).
Via some questions / configurations, the created pkg is adapted to your needs and data.

Other intended arguments are:
- `-l`, `--location` to specify the location of the new pkg, e.g., `-l /path/to/location`
- `-s`, `--source` to specify another url of a cookiecutter template. ".git" urls. Default: "https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg.git"
- `-v`, `--version`, `--checkout` to specify the checkout branch / version of the cookiecutter template. Equivalent to the cookiecutter "checkout" argument. Default: "main"

### Module
```bash
aaambos create module -n MyOwnModule
```
will create a copy of the [`my_new_module.py`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/blob/main/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/modules/my_new_module.py?ref_type=heads) file from the cookiecutter template named `my_own_module.py` with the content replaced accordingly to the assigned new name.

Other intended arguments are:
- `-l`, `--location`
- `-v`, `--version`, `--checkout` to specify the checkout branch / version of the cookiecutter template. Equivalent to the cookiecutter "checkout" argument. Default: "main"

### Extension
```bash
aaambos create extension -n MyOwnExtension
```
will create a copy of the [`my_new_extension.py`](https://gitlab.ub.uni-bielefeld.de/scs/aaambos/cookiecutter-aaambos-pkg/-/blob/main/%7B%7Bcookiecutter.repo_name%7D%7D/%7B%7Bcookiecutter.__package_name%7D%7D/extensions/my_new_extension.py?ref_type=heads) file from the cookiecutter template named `my_own_extension.py` with the content replaced accordingly to the assigned new name.

Other intended arguments are:
- `-l`, `--location`
- `-v`, `--version`, `--checkout` to specify the checkout branch / version of the cookiecutter template. Equivalent to the cookiecutter "checkout" argument. Default: "main"


### Configs
```bash
aaambos create configs
```
will copy the run and arch configs from the cookiecutter template.

Optionally, `aaambos create run_config` will only copy the run config and `aaambos create arch_config` will copy the arch config.

Other intended arguments are:
- `-l`, `--location`

### Promise
```bash
aaambos create module -n MyOwmMsg
```
will print out the code for the struct, promise, and feature creation of the "MyOwnMsg". Default is "MyNewMsg". If you want to append the content to a file use the `-f <file>` option.

Other intended arguments are:
- `-f`, `--file` will append the code to the specified file. If the file does not exist it will be created.
- `-l`, `--location` only relevant if used with the --file argument

`promise` has the alias `com_promise` which can be used in the same way.

## Extending the `create` command
You can extend the `create` command via `aaambos.core.create` Python entry points.
You need to reference the function that should be called it should accept the `your_create_func` argument.
The name of the entry point is the argument for the `create` command:
```python
setup(
    ...,
    entry_points={
        'aaambos.core.create': [
            'what_to_create = {{pkg_name}}.plugins.create:your_create_func',
        ],
    },
)
```
## Code
"""

from importlib.metadata import entry_points

from loguru import logger

from aaambos.core.commands.command import Command
from aaambos.core.configuration.global_aaambos_config import GlobalAaaambosConfig

core_create_names = ["pkg", "module", "extension", "run_config", "arch_config", "configs", "com_promise", "promise", "aaambos_config", "global_config"]

create_eps = entry_points(group='aaambos.core.create')
create_names_from_eps = [e.name for e in create_eps]


class Create(Command):

    def __init__(self, what_to_create, source, location, name, version, file, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.what_to_create = what_to_create
        self.source = source
        self.location = location
        self.name = name
        self.file = file
        self.version = version

    def execute(self):
        """Create based on what_to_create a pkg, module, extension or configs.

        See https://cookiecutter.readthedocs.io/en/stable/README.html for the usage of cookiecutter
        """
        if self.location != ".":
            import os
            os.chdir(os.path.expanduser(self.location))
            logger.info(f"Uses the {os.getcwd()!r} as the location for the part to create.")
        if self.what_to_create in core_create_names:
            self.core_create()
        else:
            for ep in create_eps:
                if ep.name == self.what_to_create:
                    logger.info(f"Use {ep.name!r} entry point for {ep.group!r}")
                    ep_create_func = ep.load()
                    ep_create_func(self)
                    break

    def core_create(self):
        """Creation based on core names `core_create_names`."""
        if self.what_to_create in ["aaambos_config", "global_config"]:
            GlobalAaaambosConfig.create()
            return
        if self.what_to_create == "pkg":
            from cookiecutter.main import cookiecutter
            dir = cookiecutter(self.source, checkout=self.version)
            logger.info(f"Created aaambos pkg in directory {dir!r}")
            return
        if self.what_to_create in ["com_promise", "promise"]:
            camel_case = "MyNewMsg"
            snake_case = "my_new_msg"
            if self.name != "My":
                camel_case = self.name
                import re
                snake_case = re.sub(r'(?<!^)(?=[A-Z])', '_', self.name).lower()
            screaming_snake_case = snake_case.upper()

            com_promise_code = f'''
from datetime import datetime
from msgspec import Struct, field as m_field
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE            

{screaming_snake_case} = "{camel_case}"


class {camel_case}(Struct):
    """{camel_case} datastructure"""
    # TODO add attributes of the msg
    time: datetime = m_field(default_factory=lambda: datetime.now())


{camel_case}Promise, {camel_case}Unit = create_promise_and_unit(
    name={screaming_snake_case},
    payload_wrapper={camel_case},
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{{MSGSPEC_STRUCT_TYPE}}],
)

{camel_case}ComFeatureIn, {camel_case}ComFeatureOut = create_in_out_com_feature(
    name={screaming_snake_case}, promise={camel_case}Promise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# {camel_case}ComFeatureOut.name: ({camel_case}ComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# {camel_case}ComFeatureIn.name: ({camel_case}ComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# {camel_case}ComFeatureIn.name: ({camel_case}ComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# {camel_case}ComFeatureOut.name: ({camel_case}ComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the {camel_case}ComFeatureOut and {camel_case}ComFeatureIn based on their location.
'''
            if self.file != "__no__":
                import os
                if os.path.isfile(self.file):
                    with open(self.file, "a") as file:
                        file.write("\n")
                        file.write(com_promise_code)
                    logger.info(
                        f"Added the code for the CommunicationPromise and Feature {camel_case!r} to the {self.file!r} file.")
                else:
                    with open(self.file, "w") as file:
                        file.write(com_promise_code)
                    logger.info(
                        f"File {self.file!r} did not exist or was not a file. {self.file!r} is created with the code for a CommunicationPromise and Feature {camel_case!r}.")

            else:
                logger.info("Add the following code on top of your module, extension, or in a individual python file:")
                print(com_promise_code)
            return
        import requests
        if self.what_to_create in ["run_config", "configs"]:
            r = requests.get(
                f"https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{{{cookiecutter.repo_name}}}}%2f{{{{cookiecutter.__package_name}}}}%2fconfigs%2frun_config.yml/raw?ref={self.version}")
            with open("run_config.yml", "w") as file:
                file.write(r.text)
            logger.info(f"Created aaambos example run_config.yml")

        if self.what_to_create in ["arch_config", "configs"]:
            r = requests.get(
                f"https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{{{cookiecutter.repo_name}}}}%2f{{{{cookiecutter.__package_name}}}}%2fconfigs%2farch_config.yml/raw?ref={self.version}")
            with open("arch_config.yml", "w") as file:
                file.write(r.text)
            logger.info(f"Created aaambos example arch_config.yml")

        if "config" in self.what_to_create:
            return
        if self.what_to_create == "module":
            create_file_from_url(
                url=f"https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{{{cookiecutter.repo_name}}}}%2f{{{{cookiecutter.__package_name}}}}%2fmodules%2fmy_new_module.py/raw?ref={self.version}",
                snake_case="my_new_module",
                camel_case="MyNewModule",
                kind="module",
                create=self,
            )

        if self.what_to_create == "extension":
            create_file_from_url(
                url=f"https://gitlab.ub.uni-bielefeld.de/api/v4/projects/6430/repository/files/{{{{cookiecutter.repo_name}}}}%2f{{{{cookiecutter.__package_name}}}}%2fextensions%2fmy_new_extension.py/raw?ref={self.version}",
                snake_case="my_new_extension",
                camel_case="MyNewExtension",
                kind="extension",
                create=self,
            )


def create_file_content(base_text: str, snake_case: str, camel_case: str, name: str) -> tuple[str, str, str]:
    """Utility function that handles the replacement of the class name and similar (snake and screaming_snake_case) variants.

    Args:
        base_text: the text on which the replacement happens.
        snake_case: the snake case variant of the class inside the base text.
        camel_case: the camel case variant of the class inside the base text.
        name: the new name (in camel case) of the class. The snake and screaming snake case will be generated automatically.

    Returns:
        - converted/replaced text
        - the snake case of the class in the returned text
        - the camel case of the class in the returned text
    """
    if name != "My":
        import re
        original_snake_case = snake_case
        snake_case = re.sub(r'(?<!^)(?=[A-Z])', '_', name).lower()
        screaming_snake_case = snake_case.upper()
        return base_text.replace(camel_case, name).replace(original_snake_case, snake_case).replace(original_snake_case.upper(), screaming_snake_case), snake_case, name
    return base_text, snake_case, camel_case


def create_file_from_url(url: str, snake_case:str, camel_case:str, kind: str, create: Create):
    """Utility function that downloads the content, calls the replacement and writes into a file.

    Args:
        url: the url with the base text data.
        snake_case: the snake case of the default name (inside the base text from the url)
        camel_case: the camel_case of the default name (inside the base)
        kind: the what_to_create str, e.g., issue, observer, etc.
        create: the reference to the `create` object that contains the name for the class.
    """
    import requests
    r = requests.get(url)
    text, snake_case, camel_case = create_file_content(base_text=r.text, snake_case=snake_case, camel_case=camel_case, name=create.name)
    with open(f"{snake_case}.py", "w") as file:
        file.write(text)
    logger.info(f"Created {snake_case}.py containing the {camel_case} {kind}.")


def your_create_func(create: Create):
    """Dummy function that has the same "header" as new entry points should for the `create` command."""
    ...
