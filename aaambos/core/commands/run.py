from pathlib import Path

from aaambos.core.commands.command import Command
from aaambos.core.common.utils import update


class Run(Command):
    """The code part of the run command which starts the architecture.

    The CLI arguments are used to start and run the architecture system.
    """

    def __init__(self, run_config, arch_config, general_plus: None, instance: None, *args, **kwargs):
        """Gets the arguments of the `run`-CLI command.

        Args:
            run_config: the path str of the run config.
            arch_config: the path str of the arch config,
            other: run options. Overrides options in the run config via the CLI.
            *args: passed to base class.
            **kwargs: passed to base class.
        """
        super().__init__(*args, **kwargs)
        self.run_config = Path(run_config)
        self.arch_config = Path(arch_config)
        self.run_options = {}
        if general_plus is not None:
            general_plus = {key: value for key, value in map(lambda x: x.split(":", maxsplit=1), general_plus.split(";"))}
            update(self.run_options, {"general": {"plus": general_plus}})

        if instance is not None:
            update(self.run_options, {"general": {"instance": instance}})

    def execute(self):
        """Execute the run command with the InitPreparer which will start the supervisor.

        Returns:
            None
        """
        from aaambos.core.supervision.init_preparer import InitPreparer
        InitPreparer(self.run_config, self.arch_config, self.run_options)
