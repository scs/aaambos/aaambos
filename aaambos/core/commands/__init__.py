"""
Here the code is defined which is executed when a subcommand / _aaaambos_ command is parsed.


### `aaambos.core.commands.command`
The base class for commands.
Children are initialized with the parsed arguments of the subcommand and then the execute method is called to execute the command.

### `aaambos.core.commands.run`
The run command runs an architecture/agent.

### `aaambos.core.commands.create`
The create command creates pkgs, modules, configs, extensions based on templates.

### `aaambos.core.commands.check`
Future command that checks if an architecture is "valid" or can "run".


"""