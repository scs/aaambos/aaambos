from pprint import pprint

from loguru import logger

from aaambos.core.commands.command import Command
from aaambos.core.configuration.global_aaambos_config import GlobalAaaambosConfig
from aaambos.core.logging.executed_agents import ExecutedAgents

what_to_list = ["runs", "history", "agents", "last", "global_config", "aaambos_config"]


class List(Command):
    """List different things."""
    def __init__(self, what_to_list, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.what_to_list = what_to_list

    def execute(self):
        info_str = f"Execution Start Time{' '*6} - Agent Id{' '*25} - Agent Directory"
        if self.what_to_list in ["runs", "history", "agents"]:
            print(info_str)
            history = ExecutedAgents.get_all_executed_agents()
            if history:
                for date, name, path in history:
                    self.history_print(date, name, path)
            else:
                print("---No history recorded---")
        elif self.what_to_list == "last":
            print(info_str)
            self.history_print(*ExecutedAgents.get_last_executed_agent())
        elif self.what_to_list in ["global_config", "aaambos_config"]:
            if GlobalAaaambosConfig.get_config_file_path().is_file():
                logger.info(f"Content of the {self.what_to_list}.yml:")
                with open(GlobalAaaambosConfig.get_config_file_path(), "r") as file:
                    print(file.read())
            else:
                logger.info(f"No {self.what_to_list}.yml is not created yet.")

    @staticmethod
    def history_print(date, name, path):
        print(f"\x1B[3m{date}\x1B[0m - \x1B[1m{name}\x1B[0m - file://{path}")