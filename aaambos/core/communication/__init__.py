"""
The base classes for the communication between modules.

### `aaambos.core.communication.service`
The communication service base class. The instances are the attributes in the module class `com`.
Via [`CommunicationServiceAttribute`](`aaambos.core.commands.service.CommunicationServiceAttribute`)s different kinds of services are encapsulated.
A specific service implements these Attributes via multiple inheritances in python. For standard attributes see
`aaambos.core.std.communications.attributes` in the std library.

### `aaambos.core.communication.setup`
The setup of the services. Started in the arch setup and the creates the services in each module process later.

### `aaambos.core.communication.topic`
The topic messages and IUs are published and subscribed. Also called "category" or "channel"

### `aaambos.core.communication.promise`
The definition of the communication promise that a module uses to define its communication requirements and what it provides.

### `aaambos.core.communication.unit`
The communication unit that defines stores the information about the payload of exchanged messages and IUs (it is not the wrapper class, it has a reference of the wrapper class and can contain additional information).
It is an attribute in a defined Promise.

### `aaambos.core.communication.graph`
To handle the generated dependencies as a graph.


### `aaambos.core.communication.mediator`
In the future, this can be used to have two communication services in one module and the mediator passes the method calls to the correct service based on assigned the topics.


"""