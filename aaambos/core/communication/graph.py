from __future__ import annotations

import traceback
from datetime import datetime
from enum import Enum, auto
from os import PathLike
from pathlib import Path
from typing import TYPE_CHECKING

import networkx as nx

if TYPE_CHECKING:
    from aaambos.core.communication.promise import CommunicationPromise
    from aaambos.core.communication.service import CommunicationServiceInfo, ServiceName
    from aaambos.core.module.base import ModuleName
from aaambos.core.communication.topic import Topic


class GraphNodeTypes(Enum):
    MODULE = auto()
    TOPIC = auto()


class CommunicationGraph:
    """The representation of modules and their input and output topics via a graph.

    A multi directed networkx graph is used to store the graph.
    The nodes are the modules and topics. They are distinguished via the `type` kwarg via the GraphNodeTypes Enum.
    The directed edges contain the communication promises related to the module and the topic.

    The information stored for the modules is just the module name.
    The topic is stored via its name (not id) and the node contains the topic object (retrieved via `topic`).
    The edge contains the promise. If a module provides a topic/promise than the module is the origin and the topic is the target.
    Otherwise, the module is the target. A promise has a `key` to the communication service name. And contains the promise object via the `promise` kwargs.
    """

    def __init__(self):
        self.graph = nx.MultiDiGraph()
        self.com_service_by_name: dict[str, CommunicationServiceInfo] = {}
        # add set of module_names and a set of topic names for faster iteration ?

    def add_module(self, module_name: ModuleName):
        """Add a module node to the graph via its name if the name is not already in the graph.

        Args:
            module_name: the str of the module name. Should be unique.

        Returns:
            None
        """
        if module_name not in self.graph.nodes:
            self.graph.add_node(module_name, type=GraphNodeTypes.MODULE)

    def add_topic(self, topic: Topic):
        """Add a topic node to the graph if the topic name is not already in the graph.

        The node name is used for storing and should therefore be unique.

        Args:
            topic: the topic object to add.

        Returns:
            None
        """
        if topic.name not in self.graph.nodes:
            self.graph.add_node(topic.name, type=GraphNodeTypes.TOPIC, topic=topic)

    def add_promise(self, module_name: ModuleName, promise: CommunicationPromise, is_module_output: bool = True):
        """Add a promise to the graph which creates an edge between a module and a topic. 

        Would create the module and the topic if they are not in the graph.

        Args:
            module_name: the module that provides or requires the promise.
            promise: the promise that contains information about the topic and the communication service, etc.
            is_module_output: does the module provide the output.

        Returns:
            None
        """
        self.add_module(module_name)
        self.add_topic(promise.settings.topic)
        self.graph.add_edge(
            module_name if is_module_output else promise.settings.topic.name,
            promise.settings.topic.name if is_module_output else module_name,
            key=promise.settings.communication_service.name,
            promise=promise,
        )
        self.com_service_by_name[promise.settings.communication_service.name] = promise.settings.communication_service

    def get_in_out_com_infos_per_module(self, module_name) -> tuple[list[CommunicationServiceInfo], list[CommunicationServiceInfo], list[CommunicationServiceInfo]]:
        """Provide the communications service info for a module divided in `in` (required) `out` (provided) and `together` (in and out).

        Args:
            module_name: the module for that the info is requested

        Returns:
            the required, provided and both communication service infos from the promises that are directly linked to the module.
        """
        if module_name not in self.graph.nodes:
            return [], [], []
        in_ = self.graph.in_edges([module_name], keys=True)
        out_ = self.graph.out_edges([module_name], keys=True)
        together = [self.com_service_by_name[name] for name in set(k for _, _, k in out_).union(set(k for _, _, k in in_))]
        in_ = [self.com_service_by_name[name] for name in set(k for _, _, k in in_)]
        out_ = [self.com_service_by_name[name] for name in set(k for _, _, k in out_)]
        return in_, out_, together

    def get_all_out_topics_per_module(self, communication_service_name: ServiceName = ...) -> dict[ModuleName, list[Topic]]:
        """Provide the out (provided) topics that a communication service should provide grouped by module.

        Args:
            communication_service_name: the name of the communication service for which the method filters.

        Returns:
            the output topics of modules grouped by module name that are handled by the communication service.
        """
        out_topics = {}
        for module_name in self.graph.nodes:
            if self.graph.nodes[module_name]["type"] == GraphNodeTypes.MODULE:
                out_topics[module_name] = [self.graph.nodes[topic_name]["topic"] for topic_name in self.graph.successors(module_name)
                                           if communication_service_name is ... or self.graph.has_edge(module_name, topic_name, key=communication_service_name)]
        return out_topics

    def get_all_in_topics_per_module(self, communication_service_name: ServiceName = ...) -> dict[ModuleName, list[Topic]]:
        """Provide the in (required) topics that a communication service should provide grouped by module.


        Args:
            communication_service_name: the name of the communication service for which the method filters.

        Returns:
            the input topics of modules grouped by module name that are handled by the communication service.
        """
        in_topics = {}
        for module_name in self.graph.nodes:
            if self.graph.nodes[module_name]["type"] == GraphNodeTypes.MODULE:
                in_topics[module_name] = [self.graph.nodes[topic_name]["topic"] for topic_name in
                                           self.graph.predecessors(module_name)
                                           if communication_service_name is ... or self.graph.has_edge(topic_name,
                                                                                                       module_name,
                                                                                                       key=communication_service_name)]
        return in_topics

    def get_in_out_promises_per_module(self, module_name: ModuleName, com_service_name: ServiceName = ...) -> tuple[list[CommunicationPromise], list[CommunicationPromise]]:
        """Provide the promise objects for in- and output topics for a specific module (optional filtered for a communication service). 

        Args:
            module_name: the module
            com_service_name: (optional) a communication service for which the method should filter.

        Returns:
            the in and out promises of the module, divided via a tuple.
        """
        if module_name not in self.graph.nodes:
            return [], []
        in_ = [d["promise"] for _, _, k, d in self.graph.in_edges([module_name], keys=True, data=True) if com_service_name is ... or com_service_name == k]
        out_ = [d["promise"] for _, _, k, d in self.graph.out_edges([module_name], keys=True, data=True) if com_service_name is ... or com_service_name == k]
        return in_, out_

    def get_all_modules(self) -> list[ModuleName]:
        modules = []
        for n in self.graph.nodes:
            if self.graph.nodes[n]["type"] == GraphNodeTypes.MODULE:
                modules.append(n)

        return modules

    def is_module(self, node_name: str) -> bool:
        return node_name in self.graph.nodes and self.graph.nodes[node_name]["type"] == GraphNodeTypes.MODULE

    def is_topic(self, node_name: str) -> bool:
        return node_name in self.graph.nodes and self.graph.nodes[node_name]["type"] == GraphNodeTypes.TOPIC

    def debug_plot_graph(self, agent_name: str = "agent"):
        """Plot the communication graph via matplotlib. Not very beautiful.

        Args:
            agent_name: for the label of the matplotlib graph

        Returns:
            None
        """
        # DO NOT use it
        from matplotlib import pyplot as plt
        nx.draw_networkx(
            self.graph,
            with_labels=True,
            node_color=[(1, 0, 0) if self.graph.nodes[node_name]["type"] == GraphNodeTypes.TOPIC else (0, 1, 0) for node_name in self.graph.nodes],
            # node_size=[len(v) * the_base_size for v in self.graph.nodes()],
            )
        plt.title(f"Communication graph of {agent_name}")
        plt.draw()
        plt.pause(0.001)

    def plot(self, location: Path, agent_name: str = "agent") -> bool:
        try:
            # sudo apt-get install graphviz graphviz-dev
            # pip install pygraphviz
            agraph = nx.nx_agraph.to_agraph(self.graph)
            agraph.add_node("Module", color="green", shape="box")
            agraph.add_node("Topic - output", color="red")
            agraph.add_node("Topic - input", color="red")
            agraph.add_edge("Module", "Topic - output")
            agraph.add_edge("Topic - input", "Module")
            agraph.layout(prog="dot")
            agraph.graph_attr["label"] = f"Communication Graph of {agent_name} {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"
            for node_name in self.graph.nodes:
                node = agraph.get_node(node_name)
                if self.graph.nodes[node_name]["type"] == GraphNodeTypes.MODULE:
                    node.attr["shape"] = "box"
                    node.attr["color"] = "green"
                else:
                    node.attr["color"] = "red"
            agraph.draw(location / "com-graph.svg")
            return True
        except Exception as e:
            return False
            # traceback.print_tb(e)
