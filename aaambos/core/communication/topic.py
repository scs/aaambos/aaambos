
# also called category, channel

LeafTopic = str
TopicId = str


class Topic:
    """The topic for communication. Hear it is just the LeafTopic (str)."""
    name: LeafTopic

    def __init__(self, /, name: LeafTopic):
        self.name = name

    def id(self) -> str:
        """The unique id of the topic for all running systems.

        Returns:
            The unique id.
        """
        return self.name


class HierarchicalTopic(Topic):

    def __init__(self, *top_elements, name: LeafTopic = ..., delimiter: str = "/"):
        """Topic extension so that the topic can have a path like "hierarchy". The name (LeafTopic) should be unique (see com graph).

        Args:
            *top_elements: arguments represent the different hierarchies.
            name: the leaf topic / last hierarchy. Should be unique.
            delimiter: path like delimiter.
        """
        super().__init__(name=top_elements[-1] if name is ... else name)
        self.hierarchy = list(top_elements)
        self.id_ = delimiter.join(top_elements)

    def id(self) -> TopicId:
        return self.id_


# aliases
Category = Topic
Channel = Topic
LeafCategory = LeafTopic
LeafChannel = LeafTopic
HierarchicalCategory = HierarchicalTopic
HierarchicalChannel = HierarchicalTopic

