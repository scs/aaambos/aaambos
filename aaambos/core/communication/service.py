from __future__ import annotations

from threading import RLock
from typing import Type, TYPE_CHECKING

from attrs import define

from aaambos.core.common.utils import ShallowLock
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.communication.topic import Topic
if TYPE_CHECKING:
    from aaambos.core.configuration.module_config import ModuleCommunicationInfo


# TODO reduce to only bases and use them instead of attributes


# @define
# class CommunicationServiceAttribute:  # just a string?
#     name: str
#     categories: field(type=list[str], eq=False)
#     abstract_class_ref: None | Type[Any] = None
#     description: str = "no description"
#
#     def __hash__(self):
#         return self.name.__hash__()


ComServiceAttrCategory = str
"""A `CommunicationService` category is just a string."""
ComServiceAttrName = str
"""The name of a `CommunicationService`."""

EXTRA_SERVICE_CATEGORIES_KEY = "__extra_service_category__"

ServiceName = str


@define
class CommunicationServiceAttribute:
    """Base class for the attributes of a `CommunicationService`.

    Different `CommunicationService`s implement a number of different attributes. 
    An attribute contains a list of attribute categories (str) and a description.
    Different attributes are defined in `aaambos.std.communications.attributes`.
    """
    categories_: list[ComServiceAttrCategory]
    __description: str

    @classmethod
    def get_name(cls) -> ComServiceAttrName:
        """The name is defined by the class name of the attribute.

        Returns:
            The class name of the attribute.
        """
        return cls.__name__

    @classmethod
    def get_attribute_categories(cls) -> list[ComServiceAttrCategory]:
        """Getter of the categories_.

        Returns:
            List of `ComServiceAttrCategory`.
        """
        if issubclass(cls, CommunicationService):
            raise ValueError("'get_attribute_categories'-method is not intended to be called on a CommunicationService class")
        return cls.categories_

    @classmethod
    def get_attribute_description(cls) -> str:
        """Getter fir the description.

        Returns:
            The description string.
        """
        if issubclass(cls, CommunicationService):
            raise ValueError("'get_attribute_description'-method is not intended to be called on a CommunicationService class")
        return cls.__description


def to_attribute_dict(attributes: list[Type[CommunicationServiceAttribute]]) -> dict[ComServiceAttrName, Type[CommunicationServiceAttribute]]:
    """Convert a list of `CommunicationServiceAttribute`s classes to a dict with the key as the name of the attribute.

    Args:
        attributes: list of `CommunicationServiceAttribute`s classes.

    Returns:
        dict of `CommunicationServiceAttribute` with their names as keys.
    """
    return {attribute_class.get_name(): attribute_class for attribute_class in attributes}


@define
class CommunicationServiceInfo:
    """The information related to a `CommunicationService`.

    Is used to provide the relevant information of a communucation service in the architecture setup.
    """
    name: ServiceName
    import_path: str  # maybe delete?
    attributes: dict[ComServiceAttrName, Type[CommunicationServiceAttribute]]
    extra_categories: list[ComServiceAttrCategory]
    # base: CommunicationServiceBase
    # additional_attributes: set[CommunicationServiceAttribute]
    setup: Type[CommunicationSetup]

    def get_category_groups(self) -> dict[ComServiceAttrName, list[ComServiceAttrCategory]]:
        """Merge the CommunicationServiceAttrCategory from the extra categories (from the info) and from the attribute class itself. 

        Returns:
            list of `ComServiceAttrCategory`s grouped by ComServiceAttrName (provided by dict).
        """
        cat_groups = {EXTRA_SERVICE_CATEGORIES_KEY: self.extra_categories}
        cat_groups.update({attr_name: attribute_class.get_attribute_categories() for attr_name, attribute_class in self.attributes.items()})
        return cat_groups


class CommunicationService:  # part of each module
    """Handles communication of modules.

    Specific functionality is provided by different `CommunicationServiceAttribute`.
    For an example see the ipaccar CommunicationService. 
    """
    info: CommunicationServiceInfo
    input_topics: set[Topic]
    output_topics: set[Topic]
    lock: ShallowLock | RLock

    def __init__(self, config: ModuleCommunicationInfo):
        """Init with config and a lock.

        Args:
            config: The module specific communication info.
        """
        self.config = config
        self.lock = ShallowLock()

    async def initialize(self):
        """Setup async the communication.

        Returns:
            None
        """
        ...

# @define
# class CommunicationServiceRequirements:
#     preferred_services: Collection[CommunicationServiceInfo] # check via entry points ??
#     # if preferred_service is not available
#     required_service_attributes: set[CommunicationServiceAttribute]
#
#     def meet_requirements(self, service_info: CommunicationServiceInfo):
#         # TODO new requirements
#         return True
#         if service_info.name in [service.name for service in self.preferred_services]:
#             return True
#         return service_info.additional_attributes.union(service_info.base.base_attributes).issuperset(self.required_service_attributes)
