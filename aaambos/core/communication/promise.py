from __future__ import annotations

from enum import Enum, auto
from typing import Type, TYPE_CHECKING

from attrs import define, field

from aaambos.core.common.enums import Necessity
if TYPE_CHECKING:
    from aaambos.core.communication.service import CommunicationServiceInfo, ComServiceAttrName, \
    CommunicationServiceAttribute
    from aaambos.core.communication.unit import CommunicationUnit
from aaambos.core.communication.topic import LeafTopic, Topic


class PromiseConverter:
    """In the future convert promises to other promises."""
    ...


class PromiseNecessity(Necessity):
    REQUIRED = auto()
    OPTIONAL = auto()
    ...


class CommunicationPart(Enum):
    ...


@define
class PromiseSettings:
    """Part that is added later to the `CommunicationPromise`."""
    topic: Topic
    communication_service: CommunicationServiceInfo


@define
class CommunicationPromise:
    """Structure that contains information about the provided or required communication (message) on one topic."""
    communication_unit: CommunicationUnit
    frequency: float | None
    necessity: PromiseNecessity  # for usage outside of features ?
    suggested_leaf_topic: LeafTopic  # Why double in unit and promise? -> because of reusability of unit and promise - maybe change
    required_com_attr: dict[ComServiceAttrName, Type[CommunicationServiceAttribute]] = field(factory=dict)

    settings: PromiseSettings | None = None

    def set_settings(self, settings: PromiseSettings):
        """The settings of a communication promise are set during architecture setup."""
        self.settings = settings


class PromiseUsage:
    """In the future to check how often a module uses a promise."""
    ...