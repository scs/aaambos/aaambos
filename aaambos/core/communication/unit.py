from typing import Any, Type

from attrs import define
from semantic_version import Version

from aaambos.core.communication.service import ComServiceAttrCategory
from aaambos.core.communication.topic import LeafTopic


@define
class CommunicationUnit:
    """Contains the info about the payload wrapper.

    The unit is used to combine information about the passed data/information for a topic/promise.
    It is part of a communication promise.
    """
    name: str
    """The name of the `CommunicationUnit`."""
    version: Version
    """For compatibility reasons a vesion information about the unit."""
    payload_wrapper: Type[Any]
    """The class that is used to wrap the payload of the message."""
    default_leaf_topic: LeafTopic  # remove? compare in setup based on unit and set leaf topic based on promise
    """Suggest a leaf topic."""
    description: str
    """Longer human readable description of the unit."""
    required_service_categories: list[set[ComServiceAttrCategory]]
    """Required ComServiceAttrCategory so that the unit can be processed."""
    #service_requirements: CommunicationServiceRequirements


