from __future__ import annotations

from abc import abstractmethod
from typing import TYPE_CHECKING, Any


if TYPE_CHECKING:
    from aaambos.core.configuration.run_config import RunConfig
    from aaambos.core.communication.graph import CommunicationGraph
    from aaambos.core.communication.service import CommunicationService


class CommunicationSetup:
    """Setup of communication services of the same type.
    
    A service can require indivindual setups for each instance.
    """

    def __init__(self, communication_graph: CommunicationGraph, run_config: RunConfig):
        """Sets communication graph and run config as attributes and check the run config.

        Args:
            communication_graph: contains information about the communication of the architecture.
            run_config: the run config.
        """
        self.communication_graph = communication_graph
        self.run_config = run_config
        self.check_run_config(run_config)
        ...

    @abstractmethod
    def before_arch_start(self):
        """Is called in the arch setup

        Returns:
            None
        """
        ...

    @abstractmethod
    def before_module_start(self, module_config) -> CommunicationService:
        """Here the module config is provided. Create the `aaambos.core.communication.CommunicationService` instance for the module.

        Args:
            module_config: the configuration of the module for which the setup creates a com service instance.

        Returns:
            The `aaambos.core.communication.CommunicationService` instance.
        """
        ...

    def terminate(self):
        """Clean up."""
        ...

    @classmethod
    def run_config_general_plus_keys_and_defaults(cls) -> dict[str, Any]:
        """Provide the defaults for the general part of the run config, required/used by the com service.

        Returns:
            The default config via a dict.
        """
        # TODO also in extension setup
        return {}

    @classmethod
    def check_run_config(cls, run_config) -> bool:
        """The service can check if a run config with its values is valid.

        Args:
            run_config: the config to check.

        Returns:
            True if it is valid, False otherwise.
        """
        ...
