from __future__ import annotations

import asyncio
import importlib
import inspect
import sys
import time
from functools import partial
from multiprocessing import Queue
import signal
from typing import Type, TYPE_CHECKING, Callable

from aaambos.core.logging.setup import LoggerSetup

if TYPE_CHECKING:
    from aaambos.core.communication.service import ServiceName
    from aaambos.core.communication.setup import CommunicationSetup
    from aaambos.core.configuration.module_config import ModuleConfig
    from aaambos.core.module.base import Module
from aaambos.core.module.setup import ModuleSetup
from aaambos.core.supervision.run_time_manager import ControlMsg, ControlType, TerminateInfo


async def handle_sigint(module: Module, sig):
    """Sigint and Sigterm callback. Calls terminate method and sys exit.

    Args:
        module: The module that needs to be terminated.
        sig: info about the termination cause.

    Returns:
        None
    """
    exit_code = 0
    try:
        # print(module, sig)
        module.log.warning(f"Handle {sig!r}. Call terminate")
        exit_code = module.terminate(ControlMsg(ControlType.Terminate, TerminateInfo(cause=sig, module_name=module.name)))
        module.log.warning("Exit module process")
    finally:
        sys.exit(exit_code)


def start_module_func(loop_func: Callable, module_path: str, config: ModuleConfig, control_queue: Queue, mean_step_time: float, terminate_with_module: bool, com_setups: dict[ServiceName, CommunicationSetup], register_handle_sigint: bool = True):
    """Import and start the function.

    This function is used to pass to the process or thread (thread not correctly implemented).

    Args:
        module_path: the path to the module, so that the importlib can import the module.
        config: the module config.
        control_queue: the control queue to get information from the run time manager. Maybe deleted in future versions.
        mean_step_time: the time between the step function is called. The time spend in the step function is included in the step time.
        terminate_with_module: if sys exit should be called and a sigint handler assigned when the module terminates.
        com_setups: the communication setups

    Returns:
        Never
    """
    # does not produce expected behavior for threading. -> change to .bind() in module setup
    log = LoggerSetup.setup_logger(config.logging.provide_logger_func(config.logging), config.logging, config.name)

    try:
        loop_ = asyncio.get_event_loop()
    except RuntimeError:
        loop_ = asyncio.new_event_loop()
        asyncio.set_event_loop(loop_)

    if terminate_with_module and register_handle_sigint:
        for signame in ('SIGINT', 'SIGTERM'):
            loop_.add_signal_handler(getattr(signal, signame),
                                     lambda: asyncio.create_task(handle_sigint(arch_module, signame)))

    if config.module_start_delay and config.module_import_delay > 0:
        log.trace(f"Start 'module import'-delay of {config.module_import_delay}s")
        loop_.run_until_complete(asyncio.sleep(config.module_import_delay))

    log.trace(f"Import module {module_path}")
    py_module = importlib.import_module(module_path)

    if not hasattr(py_module, "provide_module"):
        raise Exception()  # TODO

    arch_module: Type[Module] = py_module.provide_module()

    log.trace(f"Setup module {arch_module}")
    arch_module: Module = ModuleSetup.setup_module(arch_module, config, com_setups, log)
    if config.extra_delay or config.module_start_delay:
        delay = abs(config.extra_delay) + config.module_start_delay
        assert delay >= 0, "delay must be positive; should be greater than 0 because extra delay is smallest module start delay."
        if delay != 0:
            log.trace(f"Start 'module start'-delay of {config.module_start_delay=}s and {abs(config.extra_delay)=}s. Resulting in {delay=}s.")
            loop_.run_until_complete(asyncio.sleep(delay))
        else:
            log.trace(f"No 'module start'-delay: {abs(config.module_start_delay)=}s, {config.extra_delay=}s.")

    log.trace(f"Start loop for module {config.name!r}")
    loop_.run_until_complete(loop_func(arch_module, config, control_queue, mean_step_time, terminate_with_module))


async def infinit_generator():
    """Utils method if the step function of the module is not a generator.

    Yields:
        True
    """
    while True:
        yield True


async def pre_loop_setup(module):
    """Calls communication initialize, module initialize, and extensions before and after module initialize."""
    if module.com:
        await module.com.initialize()
    else:
        module.log.info(f"module {module.name!r} has no com.")
    for extension in module.ext.values():
        if hasattr(extension, "before_module_initialize"):
            await extension.before_module_initialize(module)
    await module.initialize()
    for extension in module.ext.values():
        if hasattr(extension, "after_module_initialize"):
            await extension.after_module_initialize(module)


async def loop_step(module: Module, config: ModuleConfig, control_queue, mean_step_time, terminate_with_module: bool):
    """The loop function that calls the step function of the module and the async initialize methods.

    Args:
        module: the module object.
        config: the module config.
        control_queue: the control queue from the run time manager, maybe deleted in the future.
        mean_step_time: time between step cuntion calls.
        terminate_with_module: terminate the process when the module terminates

    Returns:
        Never
    """
    # TODO try catch block for errors
    await pre_loop_setup(module)
    is_generator_step = inspect.isgeneratorfunction(module.step)
    is_async_queue = isinstance(control_queue, asyncio.Queue)
    next_iter_time = time.time()
    if is_generator_step:
        generator = module.step
    else:
        generator = infinit_generator

    async for _ in generator():
        next_iter_time += mean_step_time
        if is_async_queue:
            control_msg: ControlMsg = None if control_queue.empty() else await control_queue.get()
        else:
            control_msg: ControlMsg = None if control_queue.empty() else control_queue.get(block=False)

        if control_msg:
            match control_msg.control:
                case ControlType.Terminate:
                    module.log.info(f"Received Terminate Control for {module.name}. Terminate. {terminate_with_module=}")
                    exit_code = module.terminate(control_msg)
                    if terminate_with_module:
                        sys.exit(exit_code)
                    else:
                        return exit_code
        else:
            if not is_generator_step:
                await module.step()
            await module.extension_step()
            await asyncio.sleep(max(0.0, next_iter_time - time.time()))


async def loop_run(module: Module, config: ModuleConfig, control_queue, mean_step_time, terminate_with_module: bool):
    """When the module should use a step method but a run method, this loop function will call it.

    The run method needs to check the control queue correctly and needs to call the extension step method. """
    await pre_loop_setup(module)
    assert hasattr(module, "run"), "module must have a run method when using the 'loop_run'-function ('start_run_module'). Otherwise return the 'start_step_module' function inside the 'get_start_function'-method of your ModuleInfo."
    await module.run(control_queue, terminate_with_module)

loop = loop_step
"""DEPRECATED. Old name of the loop_step function. Remains for older pkgs. Will be removed in a later version."""

start_step_module = partial(start_module_func, loop_func=loop_step)
"""Standard start function that calls the step function, etc."""
start_module = start_step_module
"""DEPRECATED. Old name of the start_step_module function. Remains for older pkgs. Will be removed in a later version."""

start_run_module = partial(start_module_func, loop_func=loop_run, register_handle_sigint=False)
"""Reference this if the module needs the control of the whole execution via 'run' method.

The 'run' method gets passed two arguments: the control_queue and a bool: the process should terminate when the module does.
And the handle_sigint registration needs to be done by the run method.
"""
