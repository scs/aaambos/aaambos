from __future__ import annotations

import inspect
from abc import abstractmethod
from typing import Type, TYPE_CHECKING, Callable

from loguru import logger

if TYPE_CHECKING:
    from aaambos.core.communication.service import CommunicationService
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.extension import Extension
from aaambos.core.module.feature import Feature, EnabledFeatureReason, FeatureNecessity, FeatureName
from aaambos.core.module.run import start_step_module
from aaambos.core.supervision.run_time_manager import ControlMsg


# add import setup function to each Module definition script that will be called when the module is initiated
# and use from typing import TYPECHECKING for other imports related to type hints

ModuleName = str


class ModuleInfo:
    """Info about the module used in the arch setup.

    Can be inherited by the module if the module should/can be imported/loaded during arch setup.
    """
    is_meta_module: bool = False

    @classmethod
    @abstractmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        """Define/Provide the features that the module provides (com output, etc.)

        Args:
            config: the module config for config related feature turn on / off.

        Returns:
            the features keyed by their name in a dict with the feature necessity (can it be turned on or off).
        """
        ...

    @classmethod
    @abstractmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        """Define/Provide the features that the module requires (com input, extensions, etc.)

        Args:
            config: the module config for config related feature turn on / off.

        Returns:
            the features keyed by their name in a dict with the feature necessity (can it be turned on or off).
        """
        ...

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        """Provide the config class. Overwrite when the module requires another ModuleConfig (additional parameters).

        Returns:
            the module config class.
        """
        return ModuleConfig

    # @classmethod
    # def get_module_communication_config_class(cls) -> Type[ModuleCommunicationConfig]:
    #     return ModuleCommunicationConfig

    @classmethod
    def get_start_function(cls, config: ModuleConfig) -> Callable:
        """Provide the function that handles the start of the module (e.g., in a new process).

        Args:
            config: the module config if it depends on the config.

        Returns:
            the function reference.
        """
        return start_step_module


class Module:
    """The base class for a module.

    A module is part of the architecture / system that handles a specific part,
    It communicates via a communications service with other modules. Features define the requirements and what the module provides for the other modules.
    It can have extensions (defined via features).

    Attributes:
        name:
        com: (CommunicationService)
        ext: all extensions of the module in a dict keyed by the extension names. Extensions can also be passed via kwarg to the init if the attribute in the ExtensionFeature is set.
        log:
        tpc:
        wpr:

    Examples:


    """
    name: ModuleName
    """(str) module name defined in the arch config"""
    com: CommunicationService
    """communication service that is used to send and register callbacks for msgs, etc."""
    ext: dict[str, Extension]
    """all extensions of the module in a dict keyed by the extension names. Extensions can also be passed via kwarg to the init if the attribute in the ExtensionFeature is set."""
    # concurrency_preference?
    # expected start up time (based on config?)

    def __init__(self, config: ModuleConfig, com: CommunicationService, log: logger,  ext: dict[str, Extension], *args, **kwargs):
        """Sets the attributes of the module.

        Args:
            config: the module config.
            com: communications service that is created via the setup.
            log: the loguru logger, configured for the module.
            ext: the dict of extension
            *args: further args.
            **kwargs: further kwargs.
        """
        self.config = config
        """the module config"""
        self.name = config.name
        self.com = com
        self.log = log
        """the loguru logger. Call it with self.log.info("123") (warning, error, trace, etc.)"""
        self.ext = ext
        # shortcuts:
        self.tpc = self.config.com_info.leaf_to_topic
        """maps the leaf topic (from the communication) to the topic object."""
        self.wpr = self.config.com_info.out_wrapper
        """the outwrapper indexed by the leaf topic."""
        self.prm = self.config.com_info.leaf_to_promise
        """leaf to promises. Maybe overwrites in and out if both are present"""
        self._extensions_with_step_function = []
        self._extensions_with_step_async_function = []
        for e_name, extension in self.ext.items():
            if hasattr(extension, "step"):
                if inspect.iscoroutinefunction(extension.step):
                    self._extensions_with_step_async_function.append(extension)
                else:
                    self._extensions_with_step_function.append(extension)

    async def initialize(self):
        """Implement this method that should happen during initialization (callback registration, etc.)

        Returns:
            None
        """
        ...

    async def step(self):
        """Is called every step of the module. Can be a Generator method but can also be a normal method.

        Implement it in your module.

        Returns/Yields:
            None
        """
        ...

    def terminate(self, control_msg: ControlMsg = None) -> int:
        """Is called during termination. Clean up etc. Is not an async method.

        Args:
            control_msg: can contain information about the reason for termination.

        Returns:
            the exit code of the module.
        """
        for _, extension in self.ext.items():
            if hasattr(extension, "terminate"):
                extension.terminate()
        return 0

    async def extension_step(self):
        """If extension do have a step function, here it is called. Also for the com service.
        
        Can be extended. Should call the base method.

        Returns:

        """
        if hasattr(self.com, "step"):
            await self.com.step()
        for extension in self._extensions_with_step_function:
            extension.step()
        for extension in self._extensions_with_step_async_function:
            await extension.step()

    def enabled_features(self, config: ModuleConfig) -> dict[Feature, EnabledFeatureReason]:
        """Get the enabled / turned on features of the module.

        Args:
            config: the module config.

        Returns:
            Not implemented
        """
        # TODO implement via config
        ...
