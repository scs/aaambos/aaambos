from __future__ import annotations

import asyncio
import sys
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from aaambos.core.communication.service import ServiceName
    from aaambos.core.communication.setup import CommunicationSetup
    from aaambos.core.configuration.com_config import CommunicationConfig
    from aaambos.core.configuration.module_config import ModuleConfig


class ModuleSetup:
    """Groups setup functions for modules."""

    @classmethod
    def setup_module(cls, module_class, config: ModuleConfig, com_setups: dict[ServiceName, CommunicationSetup], module_logger: 'Logger'):
        """Init of module class, setup of module communication, setup of extensions.

        Args:
            module_class: the class that is called.
            config: the module config with the extension info.
            com_setups: the communication setups.
            module_logger: the loguru logger that is passed to the module.

        Returns:
            the module object.
        """
        # communication service setup
        module_com = cls.setup_module_com(com_setups, config, module_logger)

        # extensions setup
        module_extensions = {}
        extension_args = {}
        if config.module_extension_setups:
            for extension_name, (extension_setup, extension_request) in config.module_extension_setups.items():
                extension = extension_setup.before_module_start(config)
                module_extensions[extension_name] = extension
                if extension_request.as_kwarg:
                    extension_args[extension_request.kwarg_alias or extension_request.name] = extension

        # change logger based on arch_config?
        module = module_class(config, log=module_logger, com=module_com, ext=module_extensions, **extension_args)
        return module

    @classmethod
    def setup_module_com(cls, com_setups: dict[ServiceName, CommunicationSetup], config: ModuleConfig, module_logger):
        """Setup the module communiaction services.

        If different services are used in one module a mediator should be created here (in the future).

        Args:
            com_setups: the communication setups that create the services.
            config: the module config with the information about the com services for the module.
            module_logger: the loguru logger of the module.

        Returns:
            the final communication service.
        """
        com_services = {}
        module_com = None
        if config.com_info:
            for service_info in config.com_info.services:
                module_logger.trace(f"Init communication service {service_info.name!r} for {config.name}")
                com_services[service_info.name] = com_setups[service_info.name].before_module_start(config)

            if not com_services:
                module_logger.warning(f"No communication service for {config.name}")
            if len(com_services) == 1:
                module_com = list(com_services.values())[0]
            else:
                # TODO setup mediator com service
                ...
        else:
            module_logger.info("No communication config available.")
            module_com = None
        return module_com
