from __future__ import annotations

from abc import abstractmethod
from typing import Type, TYPE_CHECKING, Any


if TYPE_CHECKING:
    from aaambos.core.module.feature import FeatureNecessity, CommunicationFeature
    from aaambos.core.configuration.run_config import RunConfig


class Extension:
    """Reusable "ability" extension for modules.

    Different methods can be implemented that are called at different points in the init process (before and after module start).

    The extension can implement a step function which is called by the module. (At the moment it is not a coroutine / async).
    """
    name: str
    module: Any  # Module
    ...

    @abstractmethod
    async def before_module_initialize(self, module):
        """Is called before the module 'initialze' call. The com is already initialized.

        Args:
            module: the assigned module of the extension.

        Returns:
            None
        """
        ...

    @abstractmethod
    async def after_module_initialize(self, module):
        """Is called after the module 'initialize' call.

        Args:
            module: the assigned module of the extension.

        Returns:
            None
        """
        ...
    # requires other extension

    @classmethod
    @abstractmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        """Provides the extension setup class.

        Maybe delete in the future.

        Returns:
            the extension setup class that can init the extension in the `before_module_start` method.
        """
        ...


class ExtensionSetup:
    """Setup of one or more extensions."""

    def __init__(self, run_config: RunConfig):
        """Set the run config.

        Args:
            run_config:
        """
        self.module_configs = []
        self.run_config = run_config

    @classmethod
    def check_run_config(cls, run_config: RunConfig) -> bool:
        """Check the run config if it contains the required attributes.

        Args:
            run_config: run config to check.

        Returns:
            True if it is a valid run config, False otherwise.
        """
        ...

    @classmethod
    def run_config_general_plus_keys_and_defaults(cls) -> dict[str, Any]:
        """Provide the defaults for the general part of the run config, required/used by the com service.

        Returns:
            The default config via a dict.
        """
        return {}

    def before_ach_start(self, module_configs):
        """Called in the arch setup before communication setup but after the turned on features are selected.

        Args:
            module_configs: the module configs of the assienged modules.

        Returns:
            None
        """
        # before communications setup (after turn on features)
        ...

    @abstractmethod
    def before_module_start(self, module_config) -> Extension:
        """Before the module start the extension setup provides the extension.

        Args:
            module_config: the module config.

        Returns:
            The initialized extension.
        """
        ...

    def terminate(self):
        # is this called?
        ...

    def add_assigned_module(self, module_config):
        """The arch setup informs about a newly assigned module.

        Args:
            module_config: the config of the new assigned module.

        Returns:
            None
        """
        self.module_configs.append(module_config)

    # TODO also normal features (just not communication features)
    @classmethod
    def provides_features(cls) -> dict[str, tuple[CommunicationFeature, FeatureNecessity]]:
        """Additional provided features by the extension.

        At the moment the additional features from extension are not fully implemented in the arch setup.
        At best copy the extension features to the feature methods from the module.

        Returns:
            provided features and necesssities as dict.
        """
        return {}

    @classmethod
    def requires_features(cls) -> dict[str, tuple[CommunicationFeature, FeatureNecessity]]:
        """Additional required features by the extension

        At the moment the additional features from extension are not fully implemented in the arch setup.
        At best copy the extension features to the feature methods from the module.


        Returns:
            required features and necessities as dict.
        """
        return {}


