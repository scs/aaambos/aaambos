"""
The relevant parts assigned directly to the module are located here.

### `aaambos.core.module.base`
The base class of all modules and the ModuleInfo.

### `aaambos.core.module.extension`
The base class for all module extensions and the extension setup.

### `aaambos.core.module.feature`
The base classes for the different kinds of features (Extension, Communication, etc.)

### `aaambos.core.module.run`
The functions that run a module. For special modules developer might implement their own loops etc.

### `aaambos.core.module.setup`
The module setup that instantiates a module with the correct kwargs, etc.

"""