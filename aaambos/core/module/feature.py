from __future__ import annotations

import functools
import traceback
from abc import abstractmethod
from datetime import datetime
from enum import Enum, auto
from os import PathLike
from pathlib import Path
from typing import Collection, Type, Iterable, TYPE_CHECKING

import networkx as nx
from attrs import define
from semantic_version import Version, SimpleSpec

from aaambos.core.common.enums import Reason, Necessity
if TYPE_CHECKING:
    from aaambos.core.communication.promise import CommunicationPromise
    from aaambos.core.module.base import ModuleName
from aaambos.core.module.extension import Extension, ExtensionSetup

FeatureName = str


class EnabledFeatureReason(Reason):
    ...


class FeatureNecessity(Necessity):
    """For future adaptations of the necessity of a feature."""


class SimpleFeatureNecessity(FeatureNecessity, Enum):
    """Is a feature required or optional.

    """
    Required = auto()
    """The feature needs to be turned on."""
    Optional = auto()
    """The feature can be turned on. E.g., if its required by another module."""

    def is_required(self) -> bool:
        """If it is the Required value it is required.

        Returns:
            True if it is the Required value.
        """
        return self.value == self.Required.value


@define
class Feature:
    """Comparable ability/feature of a module.

    Instead of request other modules. Modules request and provide features.
    Requested feature needs to be fullfilled by other modules in the architecture/system.

    The name of the feature is the main part to compare if features are equal. Versions may be the next step.
    """
    # TODO architecture for feature naming
    name: str
    version:  Version
    requirements: Collection[Feature]
    version_compatibility: SimpleSpec = SimpleSpec(">=0.0.1")# TODO are all previous versions compatible?
    behavior_to_same_provided_feature: None = None  # TODO turn all features on or is one necessary

    # TODO set name and version relevant for comparison and/or hash

    def is_convertable_to_feature(self, feature_request: Feature):
        """Compare feature to another feature and check if it is equal and so convertable.

        Args:
            feature_request: other feature.

        Returns:

        """
        # TODO rename, because no feature requests anymore -> not convertable but now equal
        return self.name == feature_request.name


def feature_decorator(func, features, necessity):
    """Decorator for requires_ or provides_features methods of ModuleInfo classes.
    
    Use it with the `features` and `necessity` arguments via `functools.partial` to declare a custom decorator.
    See `required` and `optional` for examples.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        features_dict = func(*args, **kwargs)
        add_features = {a.name: (a, necessity) for a in features}
        if features_dict:
            features_dict.update(add_features)
            return features_dict
        return add_features
    
    return wrapper

def required(*features):
    """Decorator for requires_ or provides_features methods of ModuleInfo classes to require features
    
    Syntactic sugar instead of defining the dict yourself in the method. Here you just need to pass the features.
    
    Example:
        ```python
        @classmethod
        @required(
            ModuleStatusInfoComFeatureOut,
            ModuleStatesComFeatureOut,
            ModuleManagerInstructionFeatureIn,
            RunConfigAccessFeature,
            ModuleStatusesComFeatureOut,
        )
        def requires_features(cls, config: ModuleStatusManagerConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
            features = {}
            if config.gui:
                features.update({PySimpleGUIWindowExtensionFeature.name: (PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required)})
            return features
        ```
    """
    return functools.partial(feature_decorator, features=features, necessity=SimpleFeatureNecessity.Required)

def optional(*features):
    """Decorator for requires_ or provides_features methods of ModuleInfo classes.
    
    Similar to `required` but with the `SimpleFeatureNecessity.Optional` value.
    You can add both required and optional features decorator on one method.
    Just put them under each other.
    
    Example:
        ```python
        @classmethod
        @required(...)
        @optional(...)
        def provides_features(cls, config: ModuleStatusManagerConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
            ...
        ```
    """
    return functools.partial(feature_decorator, features=features, necessity=SimpleFeatureNecessity.Optional)


@define
class FeatureGroup:
    """For easier reference: a group of features"""
    name: str
    version: Version | SimpleSpec
    features: Collection[Feature]


@define(kw_only=True)
class CommunicationFeature(Feature):
    """References input and output communication promises."""
    # TODO remove required and provided in attribute names because of confusion ?
    required_input: Collection[CommunicationPromise]
    provided_output: Collection[CommunicationPromise]


# Completely unnecessary??
@define(kw_only=True)
class ExtensionFeature(Feature):
    """References Extension features."""
    extension_setup_class: Type[ExtensionSetup]
    as_kwarg: bool
    kwarg_alias: str | None = None
    shared_extension_setup: bool = True


@define(kw_only=True)
class KeyWordArgumentFeature(Feature):
    """Provide something via kwarg to the module. Still necessary? Maybe in the future."""
    kwarg_values: Type | list[str]  # eg. Enum, bool,
    kwarg_alias: str | None


class GraphNodeTypes(Enum):
    MODULE = auto()
    FEATURE = auto()


class FeatureGraph:
    """Representation of the features and modules.
    
    Similar to the communication graph but for features.
    """

    def __init__(self):
        self.graph = nx.MultiDiGraph()

    def add_module(self, module_name: ModuleName):
        """Add a module node to the graph via its name if the name is not already in the graph.

        Args:
            module_name: the str of the module name. Should be unique.

        Returns:
            None
        """
        if module_name not in self.graph.nodes:
            self.graph.add_node(module_name, type=GraphNodeTypes.MODULE)

    def add_feature(self, feature: Feature):
        """Add a feature node to the graph if the feature name is not already in the graph.

        The feature name is used for storing and should therefore be unique.

        Args:
            feature: the feature object to add.

        Returns:
            None
        """
        if feature.name not in self.graph.nodes:
            self.graph.add_node(feature.name, type=GraphNodeTypes.FEATURE, feature=feature)

    def connect_module_and_feature(self, module_name: ModuleName, feature: Feature, is_request=True):
        """Add an edge between the module and the feature.

        Args:
            module_name: str, the name of the module.
            feature: the feaure object.
            is_request: requests connect the module with the feature, otherwise the feature is the origin and the module is the target.

        Returns:
            None
        """
        self.add_feature(feature)
        self.graph.add_edge(module_name if is_request else feature.name, feature.name if is_request else module_name, key="feature")

    def get_all_module_names_with_extensions(self, extension_name: str) -> Iterable[ModuleName]:
        """Get all module names that have a specific extension.

        Args:
            extension_name: the name of the extension for which the method searches modules.

        Yields:
            the module names that have the extensions.
        """
        for node_name in self.graph.nodes:
            if self.graph.nodes[node_name]["type"] == GraphNodeTypes.MODULE:
                for feature_name in self.graph.successors(node_name):
                    if isinstance(self.graph.nodes[feature_name]["feature"], ExtensionFeature):
                        if extension_name == self.graph.nodes[feature_name]["feature"].name:
                            yield node_name

    def get_provided_features_of_module(self, module_name: ModuleName) -> list[Feature]:
        if module_name in self.graph.nodes and self.graph.nodes[module_name]["type"] == GraphNodeTypes.MODULE:
            for feature_name in self.graph.predecessors(module_name):
                yield feature_name

    def get_required_features_of_module(self, module_name: ModuleName) -> list[Feature]:
        if module_name in self.graph.nodes and self.graph.nodes[module_name]["type"] == GraphNodeTypes.MODULE:
            for feature_name in self.graph.successors(module_name):
                yield feature_name

    def debug_plt_graph(self, agent_name: str = "agent"):
        """Plot the feature graph via matplotlib.

        Not very beautiful.

        Args:
            agent_name: for the label of the plot.

        Returns:
            None
        """
        # DO NOT use
        from matplotlib import pyplot as plt
        nx.draw_networkx(
            self.graph,
            with_labels=True,
            node_color=[(0,1,0) if self.graph.nodes[node_name]["type"] == GraphNodeTypes.MODULE else (1, 0, 0) for node_name in self.graph.nodes],
            # node_size=[len(v) * the_base_size for v in self.graph.nodes()],
        )
        plt.title(f"Feature graph of {agent_name}")
        plt.draw()
        plt.pause(0.001)

    def plot(self, location: Path, agent_name: str = "agent") -> bool:
        def graph_setup(a_graph, addon_name=""):
            # legend
            a_graph.add_node("Module", color="green", shape="box")
            a_graph.add_node("Feature - requires", color="red")
            a_graph.add_node("Feature - provides", color="red")
            a_graph.add_edge("Module", "Feature - requires")
            a_graph.add_edge("Feature - provides", "Module")
            a_graph.layout(prog="dot")
            a_graph.graph_attr["label"] = f"Feature Graph of {agent_name} {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}{addon_name}"
            for node_name in a_graph.nodes():
                node = agraph.get_node(node_name)
                if node_name in self.graph.nodes:
                    if self.graph.nodes[node_name]["type"] == GraphNodeTypes.MODULE:
                        node.attr["shape"] = "box"
                        node.attr["color"] = "green"
                    else:
                        node.attr["color"] = "red"

        try:
            # sudo apt-get install graphviz graphviz-dev
            # pip install pygraphviz
            agraph = nx.nx_agraph.to_agraph(self.graph)
            graph_setup(a_graph=agraph)
            agraph.draw(location / "feature-graph.svg")

            # without com features
            agraph = nx.nx_agraph.to_agraph(self.graph.subgraph([n for n in self.graph.nodes if not (n.endswith("FeatureIn") or n.endswith("FeatureOut") or n.endswith("InFeature") or n.endswith("OutFeature"))]))
            graph_setup(a_graph=agraph, addon_name=" - Without Com Features")
            agraph.draw(location / "feature-graph-wo-com.svg")
            return True
        except Exception as e:
            # if not installed
            # traceback.print_exception(e)
            return False

    def get_all_module_names(self):
        """Provide all module names that are stored in the graph.

        Yields:
            the module names.
        """
        for node_name in self.graph.nodes:
            if self.graph.nodes[node_name]["type"] == GraphNodeTypes.MODULE:
                yield node_name
