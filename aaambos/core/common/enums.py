
class Necessity:
    """Base necessity in aambos.

    Curently just reduces the necessities to required or not (optional).
    Is intended to be used as the base class with the Enum class.
    """

    def is_required(self) -> bool:
        """Is the enum value required (not optional).

        Returns:
            the boolean value if the enum object correlates to the hardly required variant.
        """
        return False


class Reason:
    ...