import collections.abc


class ShallowLock:
    """Does not lock anything. Just a replacement / default for synchronous (com) services that do not need a RLock (recursive lock) """
    def acquire(self, blocking=True, timeout=- 1):
        pass

    def release(self):
        pass

    def __enter__(self):
        self.acquire()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.release()


def update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = update(d.get(k, {}), v)
        else:
            d[k] = v
    return d