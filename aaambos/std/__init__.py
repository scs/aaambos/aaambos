"""The std library of aaambos provides some general implementations and examples.


### `aaambos.std.communications`
Utitily definitios for communication services, like attributes and categories of communications services (that modules/promises/units can request)

### `aaambos.std.extensions`
Standard extensions.

### `aaambos.std.guis`
The different provided approaches to create aa GUI for modules. Currently only using PySimpleGUI

### `aaambos.std.modules`
Standard modules like a module status manager, ping pong examples, and a shell command execution module.

### `aaambos.std.supervison`
The more complex Instruction Run Time Manger. And later other parts related to the supervision by *aaambos*

"""