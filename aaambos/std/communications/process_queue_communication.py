"""
## Do not use this communication service

This is just a test service that works with queues that are only checked during the step call for multiprocessing modules.
"""
import signal
import sys
from functools import partial
from multiprocessing import Process, Queue
from multiprocessing.pool import ThreadPool
from typing import Callable

from aaambos.core.communication.graph import CommunicationGraph
from aaambos.core.communication.service import CommunicationServiceInfo, CommunicationService, to_attribute_dict, ComServiceAttrCategory
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.communication.topic import Topic, TopicId
from aaambos.core.configuration.module_config import ModuleConfig, ModuleCommunicationInfo
from aaambos.core.execution.concurrency import ConcurrencyLevel
from aaambos.core.module.base import ModuleName
from aaambos.std.communications.attributes import MsgTopicCallbackReceivingAttribute, MsgTopicSendingAttribute

QUEUE_COM = "QueueCom"
QUEUE_COM_TYPE: ComServiceAttrCategory = "QueueComType"
QUEUE_IMPORT = "aaambos.std.communications.process_queue_communication.QueueComService"


class QueueComService(CommunicationService, MsgTopicSendingAttribute, MsgTopicCallbackReceivingAttribute):

    def __init__(self, config: ModuleCommunicationInfo, in_queues: dict[TopicId, Queue], out_queues: dict[TopicId, Queue]):
        super().__init__(config)
        self.info = QueueComInfo
        self.in_queues = in_queues
        self.out_queues = out_queues
        self.callbacks: dict[TopicId, Callable] = {}

    async def send(self, topic, msg):
        if topic.id() in self.out_queues:
            self.out_queues[topic.id()].put(msg)

    async def register_callback(self, topic: Topic, callback: Callable, with_topic_info=False, *args, **kwargs):
        if with_topic_info:
            self.callbacks[topic.id()] = partial(callback, topic=topic)
        else:
            self.callbacks[topic.id()] = callback

    async def unregister_callback(self, topic: Topic, callback):
        if topic.id() in self.callbacks:
            del self.callbacks[topic.id()]

    async def step(self):
        for topic, in_queue in self.in_queues.items():
            while not in_queue.empty():
                msg = in_queue.get(False)
                if topic in self.callbacks:
                    await self.callbacks[topic](msg)

    async def add_input_topic(self, topic: Topic, *args, **kwargs):
        if topic.id() not in self.in_queues:
            raise NotImplementedError(f"The QueueComService is not able to add topics after module start. Topic {topic.id()} is not part of the requested topics at setup.")

    async def add_output_topic(self, topic: Topic):
        if topic.id() not in self.out_queues:
            raise NotImplementedError(f"The QueueComService is not able to add topics after module start. Topic {topic.id()} is not part of the requested topics at setup.")


def pass_message_for_module(task):
    out_queue, in_queues, module_name = task
    while True:
        msg = out_queue.get()
        for other_module_name, queue in in_queues.items():
            if other_module_name != module_name:
                queue.put(msg)


def broker_process(module_out_queues, module_in_queues):
    def handle_sigint(sig, event):
        sys.exit(sig)
    signal.signal(signal.SIGINT, handle_sigint)
    thread_tasks = [(module_out_queues[module_name][topic_name],  module_in_queues[topic_name], module_name) for module_name in module_out_queues for topic_name in module_out_queues[module_name]]

    with ThreadPool() as pool:
        for _ in pool.map(pass_message_for_module, thread_tasks):
            pass


class QueueComSetup(CommunicationSetup):

    def __init__(self, communication_graph: CommunicationGraph):
        super().__init__(communication_graph)
        self.broker_process: Process | None = None
        self.module_out_queues: dict[ModuleName, dict[TopicId, Queue]] = {}
        self.module_in_queues: dict[TopicId, dict[ModuleName, Queue]] = {}

    def before_arch_start(self):

        for module_name, out_topics in self.communication_graph.get_all_out_topics_per_module(QUEUE_COM).items():
            self.module_out_queues[module_name] = {topic.id(): Queue() for topic in out_topics}

        for module_name, in_topics in self.communication_graph.get_all_in_topics_per_module(QUEUE_COM).items():
            for topic in in_topics:
                if topic.id() not in self.module_in_queues:
                    self.module_in_queues[topic.id()] = {}
                self.module_in_queues[topic.id()][module_name] = Queue()

        self.broker_process = Process(target=broker_process, name="QueueComBroker", kwargs={"module_out_queues": self.module_out_queues , "module_in_queues":self.module_in_queues})
        self.broker_process.start()
        ...

    def before_module_start(self, module_config: ModuleConfig) -> CommunicationService:
        if module_config.name in self.module_out_queues:
            out_queues = self.module_out_queues[module_config.name]
        else:
            out_queues = {}

        in_queues = {}
        for topic_name in self.module_in_queues:
            if module_config.name in self.module_in_queues[topic_name]:
                in_queues[topic_name] = self.module_in_queues[topic_name][module_config.name]
        return QueueComService(module_config.com_info, in_queues, out_queues)

    def terminate(self):
        if self.broker_process:
            self.broker_process.terminate()
        ...


QueueComInfo = CommunicationServiceInfo(name=QUEUE_COM,
                                        import_path=QUEUE_IMPORT,
                                        attributes=to_attribute_dict([
                                            MsgTopicSendingAttribute,
                                            MsgTopicCallbackReceivingAttribute
                                        ]),
                                        extra_categories=[QUEUE_COM_TYPE, ConcurrencyLevel.MultiProcessing.name],
                                        setup=QueueComSetup)





