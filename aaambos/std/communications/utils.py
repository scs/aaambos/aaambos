from enum import Enum, auto
from typing import Any, Type

from semantic_version import Version, SimpleSpec

from aaambos.core.communication.promise import CommunicationPromise, PromiseNecessity
from aaambos.core.communication.service import ComServiceAttrCategory, ComServiceAttrName, CommunicationServiceAttribute
from aaambos.core.communication.unit import CommunicationUnit
from aaambos.core.module.feature import CommunicationFeature, Feature
from aaambos.std.communications.attributes import IncrementalUnitTopicWrapperAttribute


# TODO add base class to Attributee


class IUEvent(Enum):
    NEW = auto()
    UPDATED = auto()
    COMMITTED = auto()


def is_iu_promise(promise: CommunicationPromise) -> bool:
    """

    Args:
        promise:

    Returns:

    """
    for attr_name, attr_class in promise.required_com_attr.items():
        # Or via Incremental type category?
        if issubclass(attr_class, IncrementalUnitTopicWrapperAttribute):
            return True
    return False
# From normal Ipaaca
# ADDED = 'ADDED',
# COMMITTED = 'COMMITTED',
# DELETED = 'DELETED',
# RETRACTED = 'RETRACTED',
# UPDATED = 'UPDATED',
# LINKSUPDATED = 'LINKSUPDATED',
# MESSAGE = 'MESSAGE'


def create_promise_and_unit(
        name: str,
        payload_wrapper: Any,
        unit_description: str,
        frequency: float,
        version_str: str = "0.1.0",
        required_service_categories: list[set[ComServiceAttrCategory]]=...,
        required_com_attr: dict[ComServiceAttrName, Type[CommunicationServiceAttribute]]=...,
        necessity: PromiseNecessity = PromiseNecessity.REQUIRED
) -> tuple[CommunicationPromise, CommunicationUnit]:
    """

    Args:
        name:
        payload_wrapper:
        unit_description:
        frequency:
        version_str:
        required_service_categories:
        required_com_attr:
        necessity:

    Returns:

    """
    unit = CommunicationUnit(
        name=f"{name}Unit",
        version=Version(version_str),
        payload_wrapper=payload_wrapper,
        default_leaf_topic=name,
        description=unit_description,
        required_service_categories=[] if required_service_categories is ... else required_service_categories   # TODO
        # TODO add "runtime connect"
        #service_requirements=CommunicationServiceRequirements([], set()) if service_requirements is ... else service_requirements
    )
    promise = CommunicationPromise(
        communication_unit=unit,
        frequency=frequency,
        necessity=necessity,
        suggested_leaf_topic=name,
        required_com_attr={} if required_com_attr is ... else required_com_attr
    )
    return promise, unit


def create_in_out_com_feature(
        name: str,
        promise: CommunicationPromise,
        version_str: str = "0.1.0",
        requirements: list[Feature] = ...,
        version_compatibility_str: str = ">=0.1.0",
) -> tuple[CommunicationFeature, CommunicationFeature]:
    """

    Args:
        name:
        promise:
        version_str:
        requirements:
        version_compatibility_str:

    Returns:

    """
    return (
        CommunicationFeature(
            name=f"{name}ComFeatureIn",
            version=Version(version_str),
            requirements=[] if requirements is ...  else requirements,
            version_compatibility=SimpleSpec(version_compatibility_str),
            required_input=[promise],
            provided_output=[],
        ),
        CommunicationFeature(
            name=f"{name}ComFeatureOut",
            version=Version(version_str),
            requirements=[] if requirements is ... else requirements,
            version_compatibility=SimpleSpec(version_compatibility_str),
            required_input=[],
            provided_output=[promise],
        )
    )