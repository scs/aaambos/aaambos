SERVICE_TYPE = "service_type"
SENDING_TYPE = "sending_type"
RECEIVING_TYPE = "receiving_type"
MSG_TYPE = "msg_type"
TOPIC_TYPE = "topic_type"
WRAPPER_TYPE = "wrapper_type"
INCREMENTAL_TYPE = "incremental_type"
RUN_TIME_TYPE = "run_time_type"

IPAACAR_IU_TYPE = "ipaacar_iu_type"
IPAACA_IU_TYPE = "ipaaca_iu_type"

MSGSPEC_STRUCT_TYPE = "msgspec_struct_type"

ONLY_LOCAL_TYPE = "only_local_type"  # no network connection

# wrapper optional / wrapper required?