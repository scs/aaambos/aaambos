from abc import abstractmethod
from typing import Callable, Type, Any, Awaitable, Tuple

from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.service import CommunicationServiceAttribute
from aaambos.core.communication.topic import Topic, LeafTopic
from aaambos.std.communications.categories import MSG_TYPE, TOPIC_TYPE, SENDING_TYPE, RECEIVING_TYPE, WRAPPER_TYPE, \
    INCREMENTAL_TYPE, RUN_TIME_TYPE


class MsgTopicSendingAttribute(CommunicationServiceAttribute):
    """

    """
    categories_ = [MSG_TYPE, TOPIC_TYPE, SENDING_TYPE]
    __description = ""

    @abstractmethod
    async def send(self, topic: Topic, msg):
        """
        Sends a message on a given topic.

        Args:
            topic: An instance of the Topic class representing the topic on which the message should be sent.
            msg: The message to be sent.

        """
        ...


class MsgTopicCallbackReceivingAttribute(CommunicationServiceAttribute):
    """

    """
    categories_ = [MSG_TYPE, TOPIC_TYPE, RECEIVING_TYPE]
    __description = ""
    _promises_with_no_topic: dict[LeafTopic, Tuple[CommunicationPromise, Callable]] = None

    @abstractmethod
    async def register_callback(self, topic: Topic, callback: Callable, *args, **kwargs):
        """
        Registers a callback for a specified topic.

        Args:
            topic: The topic to register the callback for.
            callback: The callable object to be invoked when the topic is triggered.
            *args: Any additional positional arguments to be passed to the callback.
            **kwargs: Any additional keyword arguments to be passed to the callback.

        """
        ...

    @abstractmethod
    async def unregister_callback(self, topic: Topic, callback: Callable):
        """
        Unregister Callback method is used to remove a registered callback function from the specified topic. It is an asynchronous abstract method.

        Args:
            topic: The topic from which the callback needs to be removed. The topic is of type Topic.
            callback: The callback function that needs to be unregistered. The callback function is of type Callable.

        """
        ...

    async def register_callback_for_promise(self, promise: CommunicationPromise, callback: Callable):
        """
        Registers a callback function for a given promise.

        Args:
            promise: The promise to register the callback for.
            callback: The callback function to be called when the promise is fulfilled.

        """
        if not self._promises_with_no_topic:
            self._promises_with_no_topic = {}
        if promise.settings and promise.settings.topic:
            if promise.suggested_leaf_topic in self._promises_with_no_topic:
                del self._promises_with_no_topic[promise.suggested_leaf_topic]
            await self.register_callback(promise.settings.topic, callback, promise.communication_unit.payload_wrapper)
        else:
            self._promises_with_no_topic[promise.suggested_leaf_topic] = (promise, callback)

    def get_promises_without_topic(self) -> list[Tuple[CommunicationPromise, Callable]]:
        """
        Returns a list of promises without a topic.

        Returns:
            A list of tuples, each containing a CommunicationPromise and a Callable.

        """
        if not self._promises_with_no_topic:
            self._promises_with_no_topic = {}
            return []
        return list(self._promises_with_no_topic.values())


class PayloadWrapperAttribute(CommunicationServiceAttribute):
    """

    """
    categories_ = [WRAPPER_TYPE]
    __description = ""

    @abstractmethod
    def check_wrapping(self, payload, wrapper: Type[Any]) -> bool:
        """
        Args:
            payload: The payload object that needs to be checked for wrapping.
            wrapper: The expected wrapper type of the payload object.

        Returns:
            bool: True if the payload object is wrapped by the specified wrapper type, False otherwise.
        """
        ...

    # def encode?
    # def
    # decode?

    # optional
    # def set wrapper function??


class MsgPayloadWrapperCallbackReceivingAttribute(MsgTopicCallbackReceivingAttribute):
    """

    """
    categories_ = [MSG_TYPE, TOPIC_TYPE, RECEIVING_TYPE, WRAPPER_TYPE]
    __description = ""

    @abstractmethod
    async def register_callback(self, topic: Topic, callback: Callable, wrapper: Type[Any] = ..., *args, **kwargs):
        """
        Register a callback function for a specific topic.

        Args:
            topic: The topic to register the callback for.
            callback: The callback function to be registered.
            wrapper: (optional) The wrapper class to be used for the callback function.
            *args: (optional) Additional positional arguments to be passed to the callback function.
            **kwargs: (optional) Additional keyword arguments to be passed to the callback function.
        """
        ...


class IncrementalUnitTopicWrapperAttribute(CommunicationServiceAttribute):
    categories_ = [INCREMENTAL_TYPE, TOPIC_TYPE, WRAPPER_TYPE]
    __description = ""

    @abstractmethod
    async def create_and_send_iu(self, topic: Topic, payload) -> 'IU':
        """
        Args:
            topic: The topic object representing the message topic.
            payload: The payload of the message.

        Returns:
            IU: The IU object representing the created and sent IU.

        """
        ...

    @abstractmethod
    async def register_callback_iu(self, topic: Topic, callback: Callable[[Topic, Any, 'IUEvent', 'IU', bool], None], wrapper: Type[Any] = ..., *args, **kwargs):
        """
        Args:
            topic: The topic to register the callback for.
            callback: The callback function to be executed when the topic is triggered. The callback function should take the following parameters:
                - `topic` (Topic): The topic that triggered the callback.
                - `data` (Any): The data passed along with the topic.
                - `events` ('IUEvent'): The IUEvent object used for managing IU events.
                - `iu` ('IU'): The IU instance associated with the callback.

            wrapper: The optional wrapper class to wrap the callback function. If provided, the callback function will be wrapped with an instance of the wrapper class before being executed
        *.

            *args: Variable length argument list to be passed to the callback function.

            **kwargs: Arbitrary keyword arguments to be passed to the callback function.

        """

        # callable
        ...

    async def register_callback_iu_for_promise(self, promise: CommunicationPromise, callback: Callable):
        """
        Registers a callback function for a specific IU promise.

        Args:
            promise: A CommunicationPromise object representing the promise.
            callback: A Callable object representing the callback function.

        """
        await self.register_callback_iu(promise.settings.topic, callback, promise.communication_unit.payload_wrapper)

    @abstractmethod
    async def set_payload_iu(self, iu, payload):
        """
        Args:
            iu: The IU (Incremental Unit) to which the payload will be set.
            payload: The payload that will be set for the given IU.

        """
        ...


class IncrementalUnitAccessAttribute(CommunicationServiceAttribute):
    """

    """
    categories_ = [INCREMENTAL_TYPE]  # access?
    __description = ""

    @abstractmethod
    async def get_all_ius(self) -> list['IU']:
        """

        This method retrieves a list of "IU" objects.

        Returns:
            A list of "IU" objects.

        """
        ...

    @abstractmethod
    async def get_iu_by_id(self, uid: str) -> 'IU':
        """
        Args:
            uid: The unique identifier for the IU.

        Returns:
            IU: The IU object with the specified unique identifier.
        """
        ...


class RunTimeReceivingNewTopicsAttribute(CommunicationServiceAttribute):
    """

    """
    categories_ = [RUN_TIME_TYPE, RECEIVING_TYPE, TOPIC_TYPE]
    __description = ""

    @abstractmethod
    async def add_input_topic(self, topic: Topic, callback: Callable[[Topic, Any], Awaitable[None]] = ..., wrapper: Any = ...):
        """
        Args:
            topic: The topic to add as an input topic.
            callback: The callback function to handle incoming messages from the input topic. It should accept two parameters: the topic and the received message. It should return an awaitable
        * None. (default: ...)
            wrapper: An optional wrapper object that can be used to customize the behavior of the input topic. (default: ...)

        """
        ...


class RunTimeSendingNewTopicsAttribute(CommunicationServiceAttribute):
    """

    """
    categories_ = [RUN_TIME_TYPE, SENDING_TYPE, TOPIC_TYPE]
    __description = ""

    @abstractmethod
    async def add_output_topic(self, topic: Topic):
        """
        Args:
            topic: The topic to be added to the output topics.

        """
        ...
