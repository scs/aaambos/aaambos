from typing import Type

from semantic_version import Version, SimpleSpec

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature
from aaambos.std.extensions.restricted_resouce.applicant import ResourceApplicantInterface, ResourceIdentifier
from aaambos.std.extensions.restricted_resouce.resource import RestrictedResource

RESTRICTED_RESOURCE_EXTENSION = "RestrictedResourceExtension"


class RestrictedResourceExtension(Extension):
    """A restricted resource store in which a module can manage restricted resources.

    Restricted resources can be requested and released. Acquirement's and losses are communicated via method calls.
    The applicants need to implement/follow the `aaambos.std.extensions.restricted.resource.applicant.ResourceApplicantInterface`.
    """
    name = RESTRICTED_RESOURCE_EXTENSION
    _resources = None

    async def before_module_initialize(self, module):
        self.module = module
        self._resources: dict[ResourceIdentifier, RestrictedResource] = {}

    async def after_module_initialize(self, module):
        pass

    def create_resource(self, resource_name: ResourceIdentifier | list[ResourceIdentifier]) -> bool | list[bool]:
        """Create a resource or a list of resources based on their names.

        Args:
            resource_name: the resource name or a list of resource names to create.

        Returns:
            indicates if the resource was created new (True) or already existed (False)
        """
        if isinstance(resource_name, list):
            return [self._create_resource(name) for name in resource_name]
        return self._create_resource(resource_name)

    def _create_resource(self, resource_name: ResourceIdentifier):
        if self.resource_exists(resource_name):
            return False
        self._resources[resource_name] = RestrictedResource(name=resource_name)
        return True

    def resource_exists(self, resource_name: ResourceIdentifier) -> bool:
        """Does the resource exists based on its name.

        Args:
            resource_name: the requested resource

        Returns:
            if the resource indicator is in the local resource store.
        """
        return resource_name in self._resources

    async def request_resource(self, resource_name: ResourceIdentifier, priority: int, applicant: ResourceApplicantInterface) -> bool:
        """Request the restricted resource. The applicant is notified when acquiring and lose via applicant methods.

        On a previous owner the 'handle_resource_loss'-method is called.
        It can request in this method the resource with a higher priority and therefore the original new owner can be set to the waiting list.

        Args:
            resource_name: the identifier for the resource.
            priority: the priority to request the resource, higher is better. 0 is smallest value.
            applicant: the applicant that is used for comparison and on which the methods
                'handle_resource_loss' and 'handle_acquired_resource' is caleld.

        Returns:
            if the 'handle_acquired_resource' was directly called on the applicant.
        """
        if resource_name in self._resources:
            resource = self._resources[resource_name]
            if resource.acquired_by is applicant:
                await applicant.handle_acquired_resource(resource.name)
                return True
            if resource.acquired_with_priority < priority:
                released_applicant = resource.release(priority)
                resource.set_owner(applicant, priority)
                if released_applicant is not None:
                    await released_applicant.handle_resource_loss(resource.name)
                if resource.acquired_by is applicant:
                    await applicant.handle_acquired_resource(resource.name)
                    return True

            resource.add_applicant(applicant, priority)
            return False
        raise ValueError(f"The resource {resource_name!r} does not exist in the resource store.")

    async def release_resource(self, resource_name: ResourceIdentifier, owner: ResourceApplicantInterface) -> bool:
        """Release a restricted resource. The "handle_resource_loss" is not called.

        Args:
            resource_name: the identifier of the resource to release
            owner: the applicant that releases the resource (for checking validity of the release)

        Returns:
            if the owner was the owner of the resource.
        """
        if resource_name in self._resources:
            resource = self._resources[resource_name]
            if resource.acquired_by is owner:
                resource.release()
                next_applicant = resource.next_applicant()
                if next_applicant is not None:
                    resource.set_owner(next_applicant[1], next_applicant[0])
                    await next_applicant[1].handle_acquired_resource(resource.name)
                return True
            else:
                resource.remove_applicant_from_waiting_list(owner)
        return False

    def is_resource_acquired(self, resource_name: ResourceIdentifier, priority_at_least: int = 0, owner: ResourceApplicantInterface = ...):
        """Check if a resource is acquired either by anyone or a specific owner.

        Args:
            resource_name: the identifier of the resource
            priority_at_least: if the priority of the current acquirement should reach a specific level,
                e.g., if an acquirement with a priority of less than 2 should not be seen as acquired.
            owner: optional, if the method should check if that object is the owner of the resource.

        Returns:
            if the resource is acquired under that circumstances (priority level, owner comparison if argument was set)
        """
        if resource_name in self._resources:
            resource = self._resources[resource_name]
            if resource.is_acquired() and resource.acquired_with_priority >= priority_at_least:
                if owner is ...:
                    return True
                return resource.acquired_by is owner
            return False
        raise ValueError(f"The resource {resource_name!r} does not exist in the resource store.")

    async def clean_applicant(self, applicant):
        """At the end of an applicant lifetime it should be released all acquired and requested resources.

        The 'handle_resource_loss' is not called by the release resource method.

        Args:
            applicant: the applicant to clean up.
        """
        for resource_name in self._resources:
            await self.release_resource(resource_name, applicant)
    
    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return RestrictedResourceExtensionSetup


class RestrictedResourceExtensionSetup(ExtensionSetup):
    """
    A class that sets up the RestrictedResourceExtension for a module.

    This class inherits from the ExtensionSetup base class.

    Example usage:
        setup = RestrictedResourceExtensionSetup()
        extension = setup.before_module_start(module_config)
"""
    def before_module_start(self, module_config) -> Extension:
        return RestrictedResourceExtension()


RestrictedResourceExtensionFeature = ExtensionFeature(
    name=RESTRICTED_RESOURCE_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=RestrictedResourceExtensionSetup,
    as_kwarg=True,
    kwarg_alias="resources",
)
"""The feature of the RestrictedResourceExtension that needs to be named in the required features of a module."""

