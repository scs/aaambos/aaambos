from typing import Hashable

from aaambos.std.extensions.restricted_resouce.applicant import ResourceApplicantInterface, ResourceIdentifier


class RestrictedResource:
    """A resource that can be acquired by one applicant.

    Handles the acquiring and releasing process.

    Args:
        The resource identifier for the resource.
    """
    def __init__(self, name: ResourceIdentifier):
        self.name = name
        self.acquired_by: ResourceApplicantInterface | None = None
        self.acquired_with_priority: int = -1
        self.requested_by: list[tuple[int, ResourceApplicantInterface]] = []

    def is_acquired(self):
        """
        Returns True if the object is acquired by someone, False otherwise.

        Returns:
            bool
        """
        return self.acquired_by is not None

    def set_owner(self, owner, priority):
        """
        Sets the owner and priority for the object.

        Args:
            owner: The owner to be set.
            priority: The priority to be set.

        """
        self.acquired_by = owner
        self.acquired_with_priority = priority

    def add_applicant(self, applicant, priority):
        """
        Args:
            applicant: The applicant object to be added.
            priority: The priority assigned to the applicant.

        """
        if applicant != self.acquired_by:
            self.remove_applicant_from_waiting_list(applicant)
            self.requested_by.append((priority, applicant))

    def release(self, new_priority=-1):
        """
        Release the ownership of the object.

        Args:
            new_priority (int, optional): The new priority value for the object. Defaults to -1.

        Returns:
            released_owner: The previous owner of the object.

        """
        released_owner = self.acquired_by
        self.acquired_by = None
        self.acquired_with_priority = new_priority
        return released_owner

    def next_applicant(self) -> tuple[int, ResourceApplicantInterface] | None:
        """
        Fetches the next applicant in the requested_by list.

        Returns:
            - tuple[int, ResourceApplicantInterface] | None: A tuple containing the priority level and the applicant object, or None if there are no more applicants in the list.
        """
        if len(self.requested_by) > 0:
            next_applicant = max(self.requested_by, key=lambda b: b[0])
            self.requested_by.remove(next_applicant)
            return next_applicant
        return None

    def remove_applicant_from_waiting_list(self, applicant):
        """
        Removes the specified applicant from the waiting list.

        Args:
            applicant: The applicant object to be removed from the waiting list.

        """
        requested_applicant_refs = [app for _, app in self.requested_by]
        if applicant in requested_applicant_refs:
            self.requested_by.pop(requested_applicant_refs.index(applicant))
