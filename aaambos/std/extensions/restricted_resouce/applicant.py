from abc import abstractmethod
from typing import Hashable

ResourceIdentifier = Hashable
"""A generic type that can be used to identify the resource, e.g., str"""


class ResourceApplicantInterface:

    async def handle_resource_loss(self, resource_name: ResourceIdentifier):
        """Handle the loss of an acquired resource.

        Args:
            resource_name: the identifier of the resource.
        """
        ...

    async def handle_acquired_resource(self, resource_name: ResourceIdentifier):
        """Handle an acquired resource that was requested.

        Args:
            resource_name: the identifier of the resource.
        """
        ...