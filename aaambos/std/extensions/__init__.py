"""
Different standard extensions.
Hooks, MessageCache, ModuleStatus, PipeSubprocess, RestrictedResources, and RunConfigAccess extend the capabilities of modules in a reusable way.
"""