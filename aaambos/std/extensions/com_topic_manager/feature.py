from datetime import datetime
from enum import Enum
from typing import Hashable, Any

from msgspec import Struct, field as m_field

from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.topic import Topic
from aaambos.core.module.base import ModuleName
from aaambos.std.communications.attributes import MsgTopicCallbackReceivingAttribute
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE

ASSIGN_TOPIC = "AssignTopic"


class AssignTopicAction(Enum):
    SubscribeTopic = "SubscribeTopic"
    UnsubscribeTopic = "UnsubscribeTopic"
    PublishTopic = "PublishTopic"


class AssignTopic(Struct):
    """AssignTopic datastructure"""
    reference: Hashable
    """The reference that the module registered a callback to."""
    topic: Topic
    """The topic to which the callback with the reference should be linked with."""
    wrapper_class: Any
    """E.g. msgspec Struct class to wrap the incoming messages around"""
    module_name: ModuleName | list[ModuleName]
    """The module name(s) that should assign the topic to the registered callbacks"""
    action: AssignTopicAction = AssignTopicAction.SubscribeTopic
    """Either subscribe or unsubscribe to the topic"""
    time: datetime = m_field(default_factory=lambda: datetime.now())


AssignTopicPromise, AssignTopicUnit = create_promise_and_unit(
    name=ASSIGN_TOPIC,
    payload_wrapper=AssignTopic,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

AssignTopicComFeatureIn, AssignTopicComFeatureOut = create_in_out_com_feature(
    name=ASSIGN_TOPIC, promise=AssignTopicPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# AssignTopicComFeatureOut.name: (AssignTopicComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# AssignTopicComFeatureIn.name: (AssignTopicComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# AssignTopicComFeatureIn.name: (AssignTopicComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# AssignTopicComFeatureOut.name: (AssignTopicComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the AssignTopicComFeatureOut and AssignTopicComFeatureIn based on their location.

from datetime import datetime
from msgspec import Struct, field as m_field
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE

UNASSIGNED_PROMISES = "UnassignedPromises"


class UnassignedPromises(Struct):
    """UnassignedPromises datastructure"""
    module_name: ModuleName
    promises: list[CommunicationPromise]
    time: datetime = m_field(default_factory=lambda: datetime.now())


UnassignedPromisesPromise, UnassignedPromisesUnit = create_promise_and_unit(
    name=UNASSIGNED_PROMISES,
    payload_wrapper=UnassignedPromises,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
    required_com_attr={MsgTopicCallbackReceivingAttribute.get_name(): MsgTopicCallbackReceivingAttribute},
)

UnassignedPromisesComFeatureIn, UnassignedPromisesComFeatureOut = create_in_out_com_feature(
    name=UNASSIGNED_PROMISES, promise=UnassignedPromisesPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# UnassignedPromisesComFeatureOut.name: (UnassignedPromisesComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# UnassignedPromisesComFeatureIn.name: (UnassignedPromisesComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# UnassignedPromisesComFeatureIn.name: (UnassignedPromisesComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# UnassignedPromisesComFeatureOut.name: (UnassignedPromisesComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the UnassignedPromisesComFeatureOut and UnassignedPromisesComFeatureIn based on their location