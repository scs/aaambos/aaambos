from typing import Type, Hashable, Callable

from semantic_version import Version, SimpleSpec

from aaambos.core.communication.topic import Topic
from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature
from aaambos.std.extensions.com_topic_manager.feature import ASSIGN_TOPIC, AssignTopic, AssignTopicAction, \
    UNASSIGNED_PROMISES, UnassignedPromises

COM_TOPIC_MANAGER_EXTENSION = "ComTopicManagerExtension"


class ComTopicManagerExtension(Extension):
    callbacks: dict[Hashable, Callable]
    """Subscribe and unsubscribe during run time to topics based on AssignTopic messages.

    Module needs to register a callback method for an abstract reference that is also mentioned in the AssignTopic message.
    The module that uses the extension needs to mention also the ComInFeature of the AssignTopic message in its provides_feature method.
    ```python
    AssignTopicComFeatureIn.name: (AssignTopicComFeatureIn, SimpleFeatureNecessity.Required)
    UnassignedPromisesComFeatureOut.name: (UnassignedPromisesComFeatureOut, SimpleFeatureNecessity.Required)
    ```
    A managing module, a gui, or a module that instantiates an outside process (that sends on a mqtt channel) can send the AssignTopic messages and then module with this extension can listen to different topics.
    """
    name = COM_TOPIC_MANAGER_EXTENSION

    async def before_module_initialize(self, module):
        self.callbacks = {}
        self.module = module

    async def after_module_initialize(self, module):
        await self.module.com.register_callback_for_promise(self.module.prm[ASSIGN_TOPIC], self.handle_assign_topic)
        promises_without_topic = self.module.com.get_promises_without_topic()
        if promises_without_topic and not self.module.tpc[UNASSIGNED_PROMISES]:
            self.module.log.error(f"Using ComTopicManagerExtension but the promise {UNASSIGNED_PROMISES!r} is not setup and has no topic assigned. Maybe you need to add `UnassignedPromisesComFeatureOut.name: (UnassignedPromisesComFeatureOut, SimpleFeatureNecessity.Required)` to the provides_features method of the {self.module.__class__.__name__!r} class.")
        else:
            for promise, callback in promises_without_topic:
                self.callbacks[promise.suggested_leaf_topic] = callback
            await self.module.com.send(self.module.tpc[UNASSIGNED_PROMISES], UnassignedPromises(module_name=self.module.name, promises=[p for p, _ in promises_without_topic]))

    async def handle_assign_topic(self, topic: Topic, msg: AssignTopic):
        """
        Handle the assignment of a topic.

        Args:
            topic: The topic to assign.
            msg: The assignment message.

        """
        if msg.action == AssignTopicAction.SubscribeTopic:
            if msg.reference in self.callbacks:
                self.module.log.info(f"Subscribing to {msg.topic} for topic {msg.reference!r} reference.")
                self.module.com.register_callback(topic=msg.topic, callback=self.callbacks[msg.reference], wrapper=msg.wrapper_class)
            else:
                self.module.log.error(f"For reference {msg.reference} is not a registered callback available. Registered callbacks in module {self.module.name} are: {self.callbacks}.")
        elif msg.action == AssignTopicAction.UnsubscribeTopic:
            if msg.reference in self.callbacks:
                self.module.log.info(f"Unsubscribe to topic {msg.topic}.")
                self.module.com.unregister_callback(topic=msg.topic, callback=self.callbacks[msg.reference])
        elif msg.action == AssignTopicAction.PublishTopic:
            self.module.tpc[msg.reference] = msg.topic

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return ComTopicManagerExtensionSetup

    def register_callback(self, reference, callback):
        # if the module wants to define callbacks by hand. Is automatically done if the promise has no topic assigned (The reference is the suggested_leaf_topic).
        self.callbacks[reference] = callback


class ComTopicManagerExtensionSetup(ExtensionSetup):
    
    def before_module_start(self, module_config) -> Extension:
        return ComTopicManagerExtension()


ComTopicManagerExtensionFeature = ExtensionFeature(
    name=COM_TOPIC_MANAGER_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=ComTopicManagerExtensionSetup,
    as_kwarg=True,
    kwarg_alias="com_topic_manager",
)



