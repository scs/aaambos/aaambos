from typing import Type

from semantic_version import Version

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature

RUN_CONFIG_ACCESS_EXTENSION = "FeatureGraphAccess"
"""The name of the Extension"""


class RunConfigAccessExtension(Extension):
    """The extension is just a copy of the RunConfig."""
    name = RUN_CONFIG_ACCESS_EXTENSION

    async def before_module_initialize(self, module):
        ...

    async def after_module_initialize(self, module):
        pass

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return RunConfigAccessExtensionSetup


class RunConfigAccessExtensionSetup(ExtensionSetup):
    """
    Class: RunConfigAccessExtensionSetup
    Inherits: ExtensionSetup

    The RunConfigAccessExtensionSetup class is responsible for handling the setup process before a module starts. It inherits from the ExtensionSetup class.

    """
    def before_module_start(self, module_config) -> Extension:
        return self.run_config


RunConfigAccessFeature = ExtensionFeature(
    name=RUN_CONFIG_ACCESS_EXTENSION,
    version=Version("1.0.0"),
    requirements=[],
    extension_setup_class=RunConfigAccessExtensionSetup,
    as_kwarg=True,
    kwarg_alias="run_config",
)
"""The feature for the extension."""
