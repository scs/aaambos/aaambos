import fcntl
import os
import re
import subprocess
import sys
import time
from abc import abstractmethod
from json import JSONDecodeError
from typing import Type, Callable, Awaitable, Any

import msgspec.json
from semantic_version import Version, SimpleSpec

from aaambos.core.module.base import Module
from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature


PIPE_SUBPROCESS_EXTENSION = "PipeSubprocessExtension"

MSG_PATTERN = r"<<<!!<<<(?P<msg>[^\n]*)>>>!!>>>"


class MsgDecode:

    @abstractmethod
    def extract_msg(self, line) -> Any | None:
        ...


class SimpleMsgDecode(MsgDecode):

    def __init__(self, pattern=MSG_PATTERN):
        self.pattern = pattern
        self.regex = re.compile(self.pattern)
        self.group_name = "msg"

    def extract_msg(self, line) -> Any | None:
        print(line)
        match = self.regex.match(line)
        if match:
            return match["msg"]
        print(match)
        return None


class PipeSubprocessExtension(Extension):
    """
    A subclass of Extension representing a pipe subprocess extension.

    Attributes:
        name (str): The name of the extension.
        sub (subprocess.Popen): The subprocess object.
        callback (Callable): A callback function to be called with each message received from the subprocess.
        msg_decoder (MsgDecode): An object for decoding messages from the subprocess.
        converter (Callable): A function for converting messages before passing them to the callback.
        module (Module): The module this extension is associated with.
    """
    name = PIPE_SUBPROCESS_EXTENSION
    sub: subprocess.Popen = None
    callback: Callable[[Any], Awaitable]
    msg_decoder: MsgDecode = SimpleMsgDecode()
    converter = Callable
    module: Module

    async def before_module_initialize(self, module):
        self.module = module
        pass

    async def after_module_initialize(self, module):
        pass

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return PipeSubprocessExtensionSetup

    async def start_subprocess(self, command: list[str], callback, converter=lambda x: x):
        """
        Args: command: The command to be executed as a subprocess. It should be a list of strings where each element
        represents a command or argument. callback: The callback function to be called when there is output from the
        subprocess. The function should accept a single parameter, which is the output from the subprocess. converter
        (optional): A function that can be used to convert or process the output from the subprocess. The default
        value is lambda x: x, which means no conversion will be performed.

        """
        if self.sub and self.sub.returncode is None:
            raise ValueError("Process is still running")

        self.sub = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, text=True, bufsize=1)
        self.callback = callback
        self.converter = converter
        # non-blocking stdout and stdin
        #fl = fcntl.fcntl(self.sub.stdout, fcntl.F_GETFL)
        #fcntl.fcntl(self.sub.stdout, fcntl.F_SETFL, fl | os.O_NONBLOCK)
        #fl_ = fcntl.fcntl(self.sub.stderr, fcntl.F_GETFL)
        #fcntl.fcntl(self.sub.stderr, fcntl.F_SETFL, fl_ | os.O_NONBLOCK)

    async def step(self):
        """
        Executes the step asynchronously.

        This method is used to execute a step asynchronously. It checks if there is a running subprocess (
        `self.sub`). If there is no subprocess, the method returns without doing anything. If the subprocess has
        exited, it logs a warning message along with the return code and the output of the subprocess. Then,
        it sets `self.sub` to `None` and returns.

        If the subprocess is still running, the method checks if the stdout of the subprocess is a tty. If it is not
        a tty, it reads a line from stdout and attempts to convert it using the `self.converter` method. If the
        conversion is successful, it calls the `self.callback` method with the converted message as the argument.

        The method does not handle stderr at the moment. There is commented code that suggests handling stderr by
        reading lines from the stderr and logging them as error messages, but it is currently disabled.

        Returns:
            - None

        Example usage:
            ```python
            await self.step()
            ```
        """
        if not self.sub:
            return
        if self.sub.returncode is not None:
            self.module.log.warning(f"sub died {self.sub.returncode=}", self.sub.returncode)
            self.module.log.warning(self.sub.stdout.readlines())
            for err in self.sub.stderr.readlines():
                self.module.log.warning(err[:-1])
            self.sub = None
            return
        if self.sub.poll() is None:
            # try:
            #     line = self.sub.stdout.readline()
            #     if line:
            #         self.module.log.trace(line)
            #         msg = self.msg_decoder.extract_msg(line)
            #         if msg:
            #             await self.callback(self.converter(msg))
            #         error = self.sub.stderr.readline()
            #         if error:
            #             for line in error:
            #                 self.module.log.error(line[:-1])
            #     else:
            #         return
            # except IOError:
            #     print("IO Error")
            #     return
            if not os.isatty(self.sub.stdout.fileno()):
                line = self.sub.stdout.readline()
                try:
                    converted = self.converter(line)
                    if converted:
                        await self.callback(converted)
                except JSONDecodeError:
                    self.module.log.trace(line[:-1])
                    pass
            #while not os.isatty(self.sub.stderr.fileno()):
            #    self.module.log.error(self.sub.stderr.readline()[:-1])
            return
            try:
                line = self.sub.stdout.readline()
                if line:
                    msg = self.msg_decoder.extract_msg(line)
                    if msg:
                        await self.callback(self.converter(msg))
                    else:
                        self.module.log.trace(line)
                error = self.sub.stderr.readline()
                if error:
                    print(f"{error!r}")
            except IOError:
                print("IO Error")
                return

    async def msg_str_subprocess(self, msg: str):
        """
        Writes the given message to the subprocess stdin.

        Args:
            msg: A string representing the message to be written.
        """
        # TODO check if input is available (last read is a specific string?)
        self.sub.stdin.write(msg)

    async def msg_subprocess(self, msg):
        """
        Args:
            msg: The message to be passed to the subprocess.

        """
        msg = msgspec.json.encode(msg).decode("utf-8")
        await self.msg_str_subprocess(msg + "\n")

    def terminate_sub_process(self):
        if self.sub:
            self.sub.terminate()


class PipeSubprocessExtensionSetup(ExtensionSetup):
    """
    This class represents the setup for the PipeSubprocessExtension.

    Example Usage:
        setup = PipeSubprocessExtensionSetup()
        extension = setup.before_module_start(module_config)
    """
    def before_module_start(self, module_config) -> Extension:
        return PipeSubprocessExtension()


PipeSubprocessExtensionFeature = ExtensionFeature(
    name=PIPE_SUBPROCESS_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=PipeSubprocessExtensionSetup,
    as_kwarg=True,
    kwarg_alias="subprocess",
)

