from collections import defaultdict
from datetime import datetime
from typing import Type, Awaitable, Any

from msgspec import Struct, field as m_field
from semantic_version import Version, SimpleSpec

from aaambos.core.communication.topic import Topic
from aaambos.core.module.base import ModuleName, Module
from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature

MODULE_STATUS_EXTENSION = "ModuleStatusExtension"
"""Name of the ModuleStatusExtension."""
MODULE_STATUS_INFO = "ModuleStatusInfo"
"""The suggested topic of the ModuleStatusInfo dataclass."""

TERMINATED_STATUS = "terminated"

class ModuleStatusInfo(Struct):
    """Module Status Information send from the ModuleStatusExtension (in modules) to the ModuleStatusModule."""
    module_name: ModuleName
    """The name of the module the status is about. (Which sent it)"""
    module_status: str
    """The arbitrary string that describes the status. (The definition of specific statuses is not guaranteed here)"""
    extra_info: dict
    """Further infos about the current status, or why it changed."""
    time: datetime = m_field(default_factory=lambda: datetime.now())


ModuleStatusInfoPromise, ModuleStatusInfoUnit = create_promise_and_unit(
    name=MODULE_STATUS_INFO,
    payload_wrapper=ModuleStatusInfo,
    unit_description="Informs about the status of a module (send by the module), like initialized, ready, terminated, etc.",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)
"""The promise and unit of the ModuleStatusInfo"""

ModuleStatusInfoComFeatureIn, ModuleStatusInfoComFeatureOut = create_in_out_com_feature(
    name=MODULE_STATUS_INFO, promise=ModuleStatusInfoPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)
"""The communication features for the ModuleStatusInfo"""

MODULE_STATUSES = "ModuleStatuses"


class ModuleStatuses(Struct):
    """ModuleStatuses datastructure"""
    updates: dict[ModuleName, int]
    """Which modules updated their state and how many times since the last msg."""
    histories: dict[ModuleName, list[ModuleStatusInfo]]
    """Contains the complete history of module statuses of all modules with the extension. Maybe in the future limited to a number of last statuses."""
    time: datetime = m_field(default_factory=lambda: datetime.now())


ModuleStatusesPromise, ModuleStatusesUnit = create_promise_and_unit(
    name=MODULE_STATUSES,
    payload_wrapper=ModuleStatuses,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

ModuleStatusesComFeatureIn, ModuleStatusesComFeatureOut = create_in_out_com_feature(
    name=MODULE_STATUSES, promise=ModuleStatusesPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)


class ModuleStatusExtension(Extension):
    """Encapsulation of the status handling of a module.

    A module can call the `set_module_status` method, which handles the change.
    """
    name = "ModuleStatus"
    status_history = None
    """The history of statuses"""
    other_histories: dict[ModuleName, list[ModuleStatusInfo]] = None
    awaitable_on_module_name: dict[ModuleName, list[tuple[str, Awaitable]]] = None
    awaitable_on_extra_info:  list[tuple[dict, str, Awaitable]] = None
    awaitable_on_status_only: dict[str, list[Awaitable]] = None

    async def before_module_initialize(self, module):
        self.module = module
        self.status_history = []
        self.other_histories = {}
        self.awaitable_on_module_name = defaultdict(list)
        self.awaitable_on_extra_info = []
        self.awaitable_on_status_only = defaultdict(list)
        pass

    async def after_module_initialize(self, module: Module):
        # check com topic ModuleStatusInfo available
        await self.module.com.register_callback_for_promise(self.module.prm[MODULE_STATUSES], self.handle_module_statuses)

    async def set_module_status(self, module_status: str, extra_info: dict):
        """
        Sets the status of a module and sends module status information to the communication interface.

        Args:
            module_status (str): The current status of the module.
            extra_info (dict): Additional information related to the module status.

        """
        self.status_history.append(module_status)
        await self.module.com.send(self.module.tpc[MODULE_STATUS_INFO], ModuleStatusInfo(module_name=self.module.name, module_status=module_status, extra_info=extra_info))

    @property
    def current_status(self):
        """
        Returns the current status of the object.

        If the object has a history of status changes, the last status in the history will be returned.
        If there is no history, None will be returned.

        :return: The current status or None if there is no status history.
        """
        return self.status_history[-1] if self.status_history else None

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return ModuleStatusExtensionSetup

    def execute_based_on_status(self, awaitable: Awaitable[Any], status: str, on_module_name: ModuleName = ..., on_extra_info: dict = ...):
        """Execute an awaitable when a status of a module is received. The status is matched either by the module name or extra info."""
        # TODO additional functionality based on_module_info, on_feature etc. -> Communicate ModuleInfo or so

        # TODO check if status already in history
        if on_module_name is not ...:
            self.awaitable_on_module_name[on_module_name].append((status, awaitable))
        elif on_extra_info is not ...:
            self.awaitable_on_extra_info.append((on_extra_info, status, awaitable))
        else:
            self.awaitable_on_status_only[status].append(awaitable)

    async def handle_module_statuses(self, topic: Topic, msg: ModuleStatuses):
        """
        Handle module statuses and execute awaitables from the `execute_based_on_status` method when the status and module matches.

        Args:
            self: The instance of the class.
            topic (Topic): The topic of the message.
            msg (ModuleStatuses): The message containing module statuses.

        """
        self.other_histories.update(msg.histories)
        for search_name in self.awaitable_on_module_name:
            for modulename, number_statuses in msg.updates.items():
                if modulename == search_name:  # TODO regex?
                    for search_status, awaitable in self.awaitable_on_module_name[search_name]:
                        for status_info in list(reversed(self.other_histories[modulename]))[:number_statuses]:
                            if status_info.module_status == search_status:
                                await awaitable
        for search_extra, search_status, awaitable in self.awaitable_on_extra_info:
            for modulename, number_statuses in msg.updates.items():
                for status_info in list(reversed(self.other_histories[modulename]))[:number_statuses]:
                    if search_extra.items() <= status_info.extra_info.items():
                        if search_status == status_info.module_status:
                            await awaitable
        for search_status, awaitable_list in self.awaitable_on_status_only.items():
            for modulename, number_statuses in msg.updates.items():
                for status_info in list(reversed(self.other_histories[modulename]))[:number_statuses]:
                    if status_info.module_status == search_status:
                        for awaitable in awaitable_list:
                            await awaitable
        # TODO without status and only extra_info?


class ModuleStatusExtensionSetup(ExtensionSetup):
    """
    A class representing the setup for the ModuleStatusExtension.

    This class extends the ExtensionSetup class.
"""
    def before_module_start(self, module_config) -> Extension:
        return ModuleStatusExtension()


ModuleStatusExtensionFeature = ExtensionFeature(
    name=MODULE_STATUS_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],  #[ModuleStatusInfoComFeatureOut],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=ModuleStatusExtensionSetup,
    as_kwarg=True,
    kwarg_alias="status",
)


