from typing import Callable, Any, List


EventCallback = Callable  #[..., None]
PreCallback = Callable  #[..., Tuple[..., ...]]
PostCallback = Callable  #[[Any, ...], Tuple[Any]]


class Hook:
    """

    """

    def __init__(self):
        """

        """
        self.event_callbacks: List[EventCallback] = []
        self.pre_callbacks: List[PreCallback] = []
        self.post_callbacks: List[PostCallback] = []

    def register_event_callback(self, callback: EventCallback):
        """

        Args:
            callback:

        Returns:

        """
        if callback not in self.event_callbacks:
            self.event_callbacks.append(callback)

    def unregister_event_callback(self, callback: EventCallback):
        """

        Args:
            callback:

        Returns:

        """
        if callback in self.event_callbacks:
            self.event_callbacks.remove(callback)

    def set_event_point(self, *args, **kwargs):
        """

        Args:
            *args:
            **kwargs:

        Returns:

        """
        for c in self.event_callbacks:
            c(*args, **kwargs)

    def register_call_callback(self,
                               pre_callback: PreCallback = ...,
                               post_callback: PostCallback = ...,
                               pre_pos: int = -1,
                               post_pos: int = -1):
        """

        Args:
            pre_callback:
            post_callback:
            pre_pos:
            post_pos:

        Returns:

        """
        if pre_callback is not ... and pre_callback not in self.pre_callbacks:
            self.pre_callbacks.insert(self._get_insert_idx(pre_pos, len(self.pre_callbacks)), pre_callback)
        if post_callback is not ... and post_callback not in self.post_callbacks:
            self.post_callbacks.insert(self._get_insert_idx(post_pos, len(self.post_callbacks)), post_callback)

    def unregister_call_callback(self, callback: Callable):
        """

        Args:
            callback:

        Returns:

        """
        if callback in self.pre_callbacks:
            self.pre_callbacks.remove(callback)
        if callback in self.post_callbacks:
            self.post_callbacks.remove(callback)

    def call_with_hook(self, func: Callable[..., Any], *args, **kwargs) -> Any:
        """

        Args:
            func:
            *args:
            **kwargs:

        Returns:

        """
        for pre_c in self.pre_callbacks:
            args, kwargs = pre_c(*args, **kwargs)
        result = func(*args, **kwargs)
        for post_c in self.post_callbacks:
            result = post_c(result, *args, **kwargs)
        return result

    @staticmethod
    def _get_insert_idx(idx: int, len_: int) -> int:
        """

        Args:
            idx:
            len_:

        Returns:

        """
        # fixes list insert method to insert with -1 at the last value and not as the second last
        if idx < 0:
            return len_ - (idx + 1)
        return idx