from collections import defaultdict
from functools import partial
from typing import Callable, Hashable,  MutableMapping

from semantic_version import Version, SimpleSpec

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature
from aaambos.std.extensions.hook.hook import Hook


DICT_HOOK_STORE_EXTENSION = "DictHookStoreExtension"


class StdDictHookNames:
    ...


class DictHookStoreClass(defaultdict, Extension):
    """

    """
    name = "DictHookStore"

    async def before_module_initialize(self, module):
        self.module = module

    async def after_module_initialize(self, module):
        pass

    @classmethod
    def get_extension_setup(cls):
        return DictHookExtensionSetup


DictHookStore = partial(DictHookStoreClass, Hook)
""""""


class DictHookExtensionSetup(ExtensionSetup):
    """

    """

    def before_module_start(self, module_config) -> Extension:
        return DictHookStore()


DictHookStoreExtensionFeature = ExtensionFeature(
    name=DICT_HOOK_STORE_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=DictHookExtensionSetup,
    as_kwarg=True,
    kwarg_alias="hook"
)


def set_alias(hook_store: MutableMapping[Hashable, Hook], origin: Hashable, alias: Hashable):
    """

    Args:
        hook_store:
        origin:
        alias:

    Returns:

    """
    if origin in hook_store:
        hook_store[alias] = hook_store[origin]
    # TODO raise error


def register_on_all_hooks(hook_store: MutableMapping[Hashable, Hook], event_callback: Callable = ..., *args, **kwargs):
    """

    Args:
        hook_store:
        event_callback:
        *args:
        **kwargs:

    Returns:

    """
    for name in hook_store:
        if event_callback:
            hook_store[name].register_event_callback(event_callback)
        hook_store[name].register_call_callback(*args, **kwargs)


if __name__ == "__main__":
    hooks = DictHookStore()

    def callback_dummy(event):
        print(event)

    def pre_dummy(*args, **kwargs):
        print(*args, **kwargs)
        return args, kwargs

    def post_dummy(result, here):
        print(result, here)
        return result

    hooks["t1"].register_event_callback(callback_dummy)

    hooks["t1"].set_event_point("event happened")

    hooks["f2"].register_call_callback(pre_callback=pre_dummy, post_callback=post_dummy)

    set_alias(hooks, "f2", "f3")

    hooks["f3"].call_with_hook(lambda f: f + "1", "here")
