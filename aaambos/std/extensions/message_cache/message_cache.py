from collections import defaultdict
from typing import Type, Any

from semantic_version import Version, SimpleSpec

from aaambos.core.communication.topic import Topic, TopicId
from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature
from aaambos.std.extensions.message_cache.message_interest import MessageInterest

MESSAGE_CACHE_EXTENSION = "MessageCacheExtension"

# TODO cache of variable size (not just 1)


class MessageCacheExtension(Extension):
    """Cache the last msg based on an interest (for a topic or a topic with a specific value in the msg). Does also handle callbacks -> "ping for new msgs".

    Requires that the module already specified interest in the topics in the communication service.
    """
    name = MESSAGE_CACHE_EXTENSION
    _msg_store: dict[str, Any]
    _topic_to_interest: dict[TopicId, dict[str, list[MessageInterest]]]
    _registered_topics: set[TopicId]

    async def before_module_initialize(self, module):
        self.module = module
        self._msg_store = {}
        self._topic_to_interest = defaultdict(lambda: defaultdict(list))
        self._registered_topics = set()

    async def after_module_initialize(self, module):
        pass

    async def register_interest(self, interest: MessageInterest | list[MessageInterest]):
        """Register an interest in a messsage for cache last msgs and/or register a callback.

        Args:
            interest: contains the info about the interest like callback and if a specific value should be set in the msg.
        """
        if isinstance(interest, MessageInterest):
            await self._register_interest(interest)
        for i in interest:
            await self._register_interest(i)

    async def _register_interest(self, interest: MessageInterest):
        topic_id = interest.promise.settings.topic.id()
        if interest not in self._topic_to_interest[topic_id][interest.name]:
            self._topic_to_interest[topic_id][interest.name].append(interest)
            if topic_id not in self._registered_topics:
                await self.module.com.register_callback_for_promise(interest.promise, self.receive_msg_callback)
                self._registered_topics.update([topic_id])

    async def unregister_interest(self, interest: MessageInterest | list[MessageInterest]):
        """Remove an interest

        Args:
            interest: the interest to remove.
        """
        if isinstance(interest, MessageInterest):
            await self._unregister_interest(interest)
        for i in interest:
            await self._unregister_interest(i)

    async def _unregister_interest(self, interest: MessageInterest):
        topic_id = interest.promise.settings.topic.id()
        if interest in self._topic_to_interest[topic_id][interest.name]:
            self._topic_to_interest[topic_id][interest.name].remove(interest)

    def get_last_msg(self, interest_name: str):
        """Get a cached msg.

        Args:
            interest: the interest on which the msg is based on.

        Returns:
            The cached msg or None if not received yet
        """
        if interest_name in self._msg_store:
            return self._msg_store[interest_name]

    async def receive_msg_callback(self, topic: Topic, msg: Any):
        for _, interests in self._topic_to_interest[topic.id()].items():
            for interest in interests:
                if interest.specific_value:
                    if getattr(msg, interest.specific_value[0]) == interest.specific_value[1]:
                        self._msg_store[interest.name] = msg
                        if interest.callback:
                            await interest.callback(topic, msg)
                else:
                    self._msg_store[interest.name] = msg
                    if interest.callback:
                        await interest.callback(topic, msg)


    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return MessageCacheExtensionSetup


class MessageCacheExtensionSetup(ExtensionSetup):
    def before_module_start(self, module_config) -> Extension:
        return MessageCacheExtension()


MessageCacheExtensionFeature = ExtensionFeature(
    name=MESSAGE_CACHE_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=MessageCacheExtensionSetup,
    as_kwarg=True,
    kwarg_alias="msg_cache",
)


