from typing import Callable, Any, Awaitable

from attrs import define, Factory

from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.topic import Topic
from aaambos.core.module.feature import Feature


@define
class MessageInterest:
    """In what messages/promises/topics is a (for example) a behavior interested and the message cache should listen to and cache the methods."""
    name: str
    promise: CommunicationPromise
    required_features: list[Feature] = Factory(list)
    provided_features: list[Feature] = Factory(list)
    callback: Callable[[Topic, Any], Awaitable] | None = None
    specific_value: None | tuple[str, Any] = None
