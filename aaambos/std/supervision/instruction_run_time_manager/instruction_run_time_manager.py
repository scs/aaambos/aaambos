from __future__ import annotations

import asyncio
import signal
import sys
import time
from datetime import datetime
from enum import Enum
from multiprocessing import Queue, Process
from typing import TYPE_CHECKING

from attrs import define, Factory
from loguru import logger
from msgspec import Struct, field as m_field
from semantic_version import Version, SimpleSpec

from aaambos.core.communication.graph import CommunicationGraph
from aaambos.core.communication.promise import CommunicationPromise, PromiseNecessity
from aaambos.core.communication.service import ServiceName, CommunicationService
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.communication.topic import Topic
from aaambos.core.communication.unit import CommunicationUnit
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.setup import ModuleSetup
from aaambos.std.communications.attributes import MsgTopicCallbackReceivingAttribute, MsgTopicSendingAttribute

if TYPE_CHECKING:
    from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.configuration.run_time_manager_config import RunTimeManagerConfig
from aaambos.core.module.base import ModuleName
from aaambos.core.module.feature import FeatureGraph, CommunicationFeature, Feature, FeatureNecessity, FeatureName, \
    SimpleFeatureNecessity
from aaambos.core.supervision.run_time_manager import RunTimeManager, ControlMsg, ControlType, \
    TerminateInfo, StartInfo
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature


# TODO convert to agnostic process, thread, async

class ModuleStates(Struct):
    """The states of the processes of the modules"""
    module_states: dict[str, ModuleState]
    updated: list[str]
    time: datetime = m_field(default_factory=lambda: datetime.now())


class ModuleManagerInstruction(Struct):
    """The instruction to start or terminate specific modules."""
    modules: list[str]
    instruction: Instruction
    time: datetime = m_field(default_factory=lambda: datetime.now())


MODULE_STATES = "ModuleStates"
"""The topic name of the module statues."""
MODULE_MANAGER_INSTRUCTION = "ModuleManagerInstruction"
"""The topic name of the manager instructions."""


@define
class InfoPerQueueToCom:
    module_name: ModuleName
    termination_cause: int


@define
class InfoPerQueueFromCom:
    module_name: ModuleName
    instruction: Instruction


class Instruction(Enum):
    """Possible instructions for the manager to do with a module."""
    START = "start"
    TERMINATE = "terminate"
    STOP_SENDING_INIT_STATUS = "stop_sending_init_status"


class ModuleState(Enum):
    """The possible states of the process of a module."""
    STARTED = "started"
    TERMINATED = "terminated"
    FAILED = "failed"


ModuleStatesPromise, ModuleStatesUnit = create_promise_and_unit(
    name=MODULE_STATES,
    payload_wrapper=ModuleStates,
    unit_description="Informs about the status of the modules, like initialized, ready, terminated, etc.",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)
"""The promise and unit of the module states."""

ModuleManagerInstructionPromise, ModuleManagerInstructionUnit = create_promise_and_unit(
    name=MODULE_MANAGER_INSTRUCTION,
    payload_wrapper=ModuleManagerInstruction,
    unit_description="Instructs the InstructionRunTimeManager to restart, terminate, start modules.",
    frequency=0,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)
"""THe promise and unit of the module instruction."""

ModuleStatesComFeatureIn, ModuleStatesComFeatureOut = create_in_out_com_feature(MODULE_STATES, promise=ModuleStatesPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0")
ModuleManagerInstructionFeatureIn, ModuleManagerInstructionFeatureOut = create_in_out_com_feature(MODULE_MANAGER_INSTRUCTION, promise=ModuleManagerInstructionPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0")


class InstructionManager:
    """The class that does the communication part of the instruction manager. It runs in a different process, because of the async part."""

    def __init__(self, com: CommunicationService | MsgTopicCallbackReceivingAttribute | MsgTopicSendingAttribute, q_receive: Queue, q_send: Queue, module_configs: dict[ModuleName, ModuleConfig], run_time_manager_config: RunTimeManagerConfig):
        self.com = com
        self.q_receive = q_receive
        self.q_send = q_send
        self.module_configs = module_configs
        self.module_states = {module_name: ModuleState.STARTED for module_name in self.module_configs}
        self.terminated_by_hand = set()
        self.init_received = False
        self.run_time_manager_config = run_time_manager_config

    async def initialize(self):
        await self.com.register_callback_for_promise(self.run_time_manager_config.module_config_hull.com_info.leaf_to_promise[MODULE_MANAGER_INSTRUCTION], self.handle_instruction)

    async def step(self):
        if not self.init_received:
            await self.com.send(self.run_time_manager_config.module_config_hull.com_info.leaf_to_topic[MODULE_STATES],
                                ModuleStates(self.module_states, []))
        if not self.q_receive.empty():
            msg: InfoPerQueueToCom = self.q_receive.get(block=False)
            if msg:

                logger.info(f"Instruction - Manager: Received termination info {msg}")
                # TODO check with incoming info etc.
                match msg.termination_cause:
                    case 0:
                        self.module_states[msg.module_name] = ModuleState.TERMINATED
                    case _:
                        self.module_states[msg.module_name] = ModuleState.FAILED
                        if msg.module_name not in self.terminated_by_hand and msg.module_name in self.module_configs and self.module_configs[msg.module_name].restart_after_failure:
                            self.q_send.put(InfoPerQueueFromCom(module_name=msg.module_name, instruction=Instruction.START))
                            self.module_states[msg.module_name] = ModuleState.STARTED
                await self.com.send(self.run_time_manager_config.module_config_hull.com_info.leaf_to_topic[MODULE_STATES], ModuleStates(self.module_states, [msg.module_name]))

    async def handle_instruction(self, topic: Topic, instruction: ModuleManagerInstruction):
        """
        Handles an instruction received for a given topic.

        Args:
            self: The instance of the class.
            topic: The topic of the instruction.
            instruction: The module manager instruction.

        Returns:
            None.
        """
        if instruction.instruction == Instruction.STOP_SENDING_INIT_STATUS:
            self.init_received = True
            return
        modules_started = []
        for module_name in instruction.modules:
            if module_name in self.module_states:
                match instruction.instruction:
                    case Instruction.START:
                        if self.module_states[module_name] != ModuleState.STARTED:
                            self.q_send.put(InfoPerQueueFromCom(module_name=module_name, instruction=instruction.instruction))
                            self.module_states[module_name] = ModuleState.STARTED
                            modules_started.append(module_name)
                            if module_name in self.terminated_by_hand:
                                self.terminated_by_hand.remove(module_name)
                    case Instruction.TERMINATE:
                        if self.module_states[module_name] == ModuleState.STARTED:
                            self.q_send.put(InfoPerQueueFromCom(module_name=module_name, instruction=instruction.instruction))
                            self.terminated_by_hand.update([module_name])
        if instruction.instruction == Instruction.START:
            await self.com.send(self.run_time_manager_config.module_config_hull.com_info.leaf_to_topic[MODULE_STATES], ModuleStates(self.module_states, modules_started))


def handle_sigint(sig, event):
    sys.exit()


def start_instruct_manager(run_time_manager_config: RunTimeManagerConfig, com_setups: dict[ServiceName, CommunicationSetup], q_to_im: Queue, q_from_im: Queue, module_configs: dict[ModuleName, ModuleConfig]):
    """
    Args:
        run_time_manager_config: An object of type RunTimeManagerConfig, containing configuration for the runtime manager.
        com_setups: A dictionary mapping ServiceName to CommunicationSetup, containing setup information for communication services.
        q_to_im: A Queue object for sending messages to the instruction manager.
        q_from_im: A Queue object for receiving messages from the instruction manager.
        module_configs: A dictionary mapping ModuleName to ModuleConfig, containing configuration information for modules.
    """
    signal.signal(signal.SIGINT, handle_sigint)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_manager(run_time_manager_config, com_setups, q_to_im, q_from_im, module_configs))


async def run_manager(run_time_manager_config: RunTimeManagerConfig, com_setups: dict[ServiceName, CommunicationSetup], q_receive: Queue, q_send: Queue, module_configs: dict[ModuleName, ModuleConfig]):
    """
    Args:
        run_time_manager_config: A RunTimeManagerConfig object that contains configuration settings for the run manager.
        com_setups: A dictionary mapping ServiceName objects to CommunicationSetup objects. These setups define the communication configurations for each service.
        q_receive: A Queue object used for receiving messages.
        q_send: A Queue object used for sending messages.
        module_configs: A dictionary mapping ModuleName objects to ModuleConfig objects. These configurations specify the settings for each module.

    """
    com = ModuleSetup.setup_module_com(com_setups, run_time_manager_config.module_config_hull, logger)
    if com:
        await com.initialize()
    manager = InstructionManager(com, q_receive, q_send, module_configs, run_time_manager_config)
    await manager.initialize()
    while True:
        await manager.step()
        await asyncio.sleep(0.5)


# ModuleInstruction
# resend info ()
# terminate ?

@define(kw_only=True)
class InstructionRunTimeManagerConfig(RunTimeManagerConfig):
    ...


class InstructionRunTimeManager(RunTimeManager):
    """The RunTimeManager in the supervisor. It starts a process that does the communication part."""

    def __init__(self, run_config: RunConfig, module_configs: dict[ModuleName, ModuleConfig]):
        super().__init__(run_config, module_configs)
        self.restart_modules = []
        self.is_termination_process_running = False
        self.q_to_im, self.q_from_im = Queue(), Queue()
        self.com_manager_process = Process(name="StatusComManager", target=start_instruct_manager,
                                           kwargs={
                                               "run_time_manager_config": self.run_config.supervisor.run_time_manager_config,
                                               "com_setups": self.run_config.settings.communication_setups,
                                               "q_to_im": self.q_to_im, "q_from_im": self.q_from_im,
                                               "module_configs": self.module_configs,
                                           })
        self.com_manager_process.start()

    def get_step_duration(self) -> float:
        return 0.5

    def step(self):
        super().step()
        if self.is_termination_process_running:
            return
        if not self.q_from_im.empty():
            msg: InfoPerQueueFromCom = self.q_from_im.get(block=False)
            if msg:
                match msg.instruction:
                    case Instruction.START:
                        if msg.module_name in self.module_configs:
                            # adapt config so that the module knows that it is a restarted one / has a terminated sibling?
                            logger.info(f"Restart module {msg.module_name!r}")
                            self.module_runner.start_module(ControlMsg(ControlType.Start, StartInfo(msg.module_name, config=self.module_configs[msg.module_name])))  # other name?
                    case Instruction.TERMINATE:
                        self.module_runner.terminate_module(ControlMsg(ControlType.Terminate, TerminateInfo("Instruction", msg.module_name)))

        if not self.com_manager_process.is_alive():
            raise RuntimeError(f"StatusCommunicationManager died: {self.com_manager_process.exitcode}")

    def handle_module_termination(self, module_name: ModuleName, termination_cause):
        logger.info(f"Handle termination of module {module_name!r}")
        if self.is_termination_process_running:
            return
        self.q_to_im.put(InfoPerQueueToCom(termination_cause=termination_cause, module_name=module_name))

    def terminate(self):
        super().terminate()
        self.com_manager_process.terminate()

    def terminate_all_modules(self):
        self.is_termination_process_running = True
        for module_name in self.module_configs:
            self.module_runner.terminate_module(
                ControlMsg(ControlType.Terminate, TerminateInfo("Shutdown", module_name)))
        self.terminate()

    @classmethod
    def create_config(cls, config_dict: dict) -> RunTimeManagerConfig:
        return InstructionRunTimeManagerConfig(**config_dict)

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        return {ModuleStatesComFeatureOut.name: (ModuleStatesComFeatureOut, SimpleFeatureNecessity.Required), ModuleManagerInstructionFeatureIn.name: (ModuleManagerInstructionFeatureIn, SimpleFeatureNecessity.Required)}

    @classmethod
    def requires_features_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        return {ModuleStatesComFeatureIn.name: (ModuleStatesComFeatureIn, SimpleFeatureNecessity.Optional), ModuleManagerInstructionFeatureOut.name: (ModuleManagerInstructionFeatureOut, SimpleFeatureNecessity.Optional)}