from __future__ import annotations

import asyncio
from collections import defaultdict
from functools import partial
from typing import Type, Callable

import networkx as nx

from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.supervision.run_time_manager import ControlMsg
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from aaambos.std.extensions.run_config_access.run_config_access import RunConfigAccessFeature
import dearpygui.dearpygui as dpg

from aaambos.std.guis.dear_py_gui.dear_py_gui_extension import DearPyGUIExtensionExtensionFeature, \
    DearPyGUIExtensionExtension


# callback runs when user attempts to connect attributes
def link_callback(sender, app_data):
    # app_data -> (link_id1, link_id2)
    dpg.add_node_link(app_data[0], app_data[1], parent=sender)


# callback runs when user attempts to disconnect attributes
def delink_callback(sender, app_data):
    # app_data -> link_id
    dpg.delete_item(app_data)


def hide_callback(sender, app_data, user_data):
    node, hide = user_data
    ...
    print("clicked hide in", node)

class ComVisModule(Module, ModuleInfo):
    config: ComVisModuleConfig

    def __init__(self, config: ComVisModuleConfig, com, log, ext, run_config: RunConfig, dearpygui_window: DearPyGUIExtensionExtension, *args, **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.run_config = run_config
        self.dearpygui_window = dearpygui_window

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            RunConfigAccessFeature.name: (RunConfigAccessFeature, SimpleFeatureNecessity.Required),
            DearPyGUIExtensionExtensionFeature.name: (DearPyGUIExtensionExtensionFeature, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return ComVisModuleConfig

    # @classmethod
    # def get_start_function(cls, config: ModuleConfig) -> Callable:
    #     return start_run_module

    # def create_dpg_window_layout(self):
    #     o_graph = self.run_config.settings.communication_graph
    #     graph = nx.MultiDiGraph()
    #     for n in o_graph.graph.nodes:
    #         graph.add_node(n)
    #     for s, t in o_graph.graph.edges():
    #         graph.add_edge(s, t)
    #     layout = nx.nx_agraph.graphviz_layout(graph, prog="dot")
    #     max_layout_width = max(l[0] for l in layout.values()) + 150
    #     max_layout_height = max(l[1] for l in layout.values()) + 100
    #
    #     modules_sub = o_graph.get_all_in_topics_per_module()
    #     modules_pub = o_graph.get_all_out_topics_per_module()
    #     topics_pub = defaultdict(list)
    #     topics_sub = defaultdict(list)
    #     for module, topics in modules_sub.items():
    #         for topic in topics:
    #             topics_sub[topic.name].append(module)
    #     for module, topics in modules_pub.items():
    #         for topic in topics:
    #             topics_pub[topic.name].append(module)
    #     atts = {}
    #     with dpg.window(label="Com Grap", width=self.config.width, height=self.config.height):
    #         with dpg.node_editor(callback=link_callback, delink_callback=delink_callback):
    #             for node in graph.nodes:
    #                 pos = [(layout[node][0]/max_layout_width) * self.config.width, (layout[node][1]/max_layout_height) * self.config.height]
    #                 with dpg.node(label=node, pos=pos):
    #                     if o_graph.is_module(node):
    #                         if node in modules_sub:
    #                             for topic in modules_sub[node]:
    #                                 with dpg.node_attribute(label=f"at-{node}-{topic.name}-sub") as a:
    #                                     atts[f"at-{node}-{topic.name}-sub"] = a
    #                                     dpg.add_input_text(label=f"{topic.name}", width=5)
    #                         if node in modules_pub:
    #                             for topic in modules_pub[node]:
    #                                 with dpg.node_attribute(label=f"at-{node}-{topic.name}-pub", attribute_type=dpg.mvNode_Attr_Output) as a:
    #                                     atts[f"at-{node}-{topic.name}-pub"] = a
    #                                     dpg.add_input_text(label=f"{topic.name}", width=5)
    #
    #                     elif o_graph.is_topic(node):
    #                         if node in topics_pub:
    #                             for module in topics_pub[node]:
    #                                 with dpg.node_attribute(label=f"at-{node}-{module}-pub") as a:
    #                                     atts[f"at-{node}-{module}-pub"] = a
    #                                     dpg.add_input_text(label=f"{module}", width=5)
    #                         if node in topics_sub:
    #                             for module in topics_sub[node]:
    #                                 with dpg.node_attribute(label=f"at-{node}-{module}-sub",
    #                                                         attribute_type=dpg.mvNode_Attr_Output) as a:
    #                                     atts[f"at-{node}-{module}-sub"] = a
    #                                     dpg.add_input_text(label=f"{module}", width=5)
    #             for topic in topics_pub:
    #                 for module in topics_pub[topic]:
    #                     dpg.add_node_link(atts[f"at-{module}-{topic}-pub"], atts[f"at-{topic}-{module}-pub"])
    #             for topic in topics_sub:
    #                 for module in topics_sub[topic]:
    #                     dpg.add_node_link(atts[f"at-{topic}-{module}-sub"], atts[f"at-{module}-{topic}-sub"])

    def create_layout_without_topics(self):
        o_graph = self.run_config.settings.communication_graph
        graph = nx.MultiDiGraph()
        for n in o_graph.graph.nodes:
            graph.add_node(n)
        for s, t in o_graph.graph.edges():
            graph.add_edge(s, t)
        layout = nx.nx_agraph.graphviz_layout(graph, prog="dot")
        max_layout_width = max(l[0] for l in layout.values()) + 150
        max_layout_height = max(l[1] for l in layout.values()) + 100

        modules_sub = o_graph.get_all_in_topics_per_module()
        modules_pub = o_graph.get_all_out_topics_per_module()
        atts = {}

        with dpg.window(label="Com Grap", width=self.config.width, height=self.config.height):
            with dpg.node_editor(callback=link_callback, delink_callback=delink_callback):
                for node in graph.nodes:
                    pos = [(layout[node][0] / max_layout_width) * self.config.width,
                           (layout[node][1] / max_layout_height) * self.config.height]
                    if o_graph.is_module(node):
                        with dpg.node(label=node, pos=pos):
                            if node in modules_sub:
                                for topic in modules_sub[node]:
                                    with dpg.node_attribute(label=f"at-{node}-{topic.name}-sub") as a:
                                        atts[f"at-{node}-{topic.name}-sub"] = a
                                        dpg.add_text(f"{topic.name}")
                            if node in modules_pub:
                                for topic in modules_pub[node]:
                                    with dpg.node_attribute(label=f"at-{node}-{topic.name}-pub",
                                                            attribute_type=dpg.mvNode_Attr_Output) as a:
                                        atts[f"at-{node}-{topic.name}-pub"] = a
                                        dpg.add_text(f"{topic.name}")
                            # with dpg.node_attribute(attribute_type=dpg.mvNode_Attr_Static):
                            #     with dpg.group(horizontal=True):
                            #         dpg.add_input_text(width=100)
                            #         dpg.add_button(label="New Topic")
                            # with dpg.node_attribute(label=f"hide-bt-{node}", attribute_type=dpg.mvNode_Attr_Static) :
                            #     dpg.add_button(label="Hide", callback=hide_callback, user_data=(node, True))
                for module in modules_sub:
                    for module_2 in modules_pub:
                        for topic in modules_sub[module]:
                            if topic in modules_pub[module_2]:
                                dpg.add_node_link(atts[f"at-{module}-{topic.name}-sub"], atts[f"at-{module_2}-{topic.name}-pub"])

                # for topic in topics_pub:
                #     for module in topics_pub[topic]:
                #         for module_2 in modules_sub:
                #             print(module, module_2, topic)
                #             if topic in [t.name for t in modules_sub[module_2]]:
                #                 dpg.add_node_link(atts[f"at-{module}-{topic}-pub"], atts[f"at-{module_2}-{topic}-sub"])

    async def initialize(self):
        self.dearpygui_window.set_viewport_options(title=f"Communication of {self.config.general_run_config.agent_name}", width=self.config.width, height=self.config.height)
        self.dearpygui_window.set_layout_creation_func(self.create_layout_without_topics)
        self.dearpygui_window.initialize_window()

    async def step(self):
        pass

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class ComVisModuleConfig(ModuleConfig):
    module_path: str = "aaambos.std.modules.com_vis"
    module_info: Type[ModuleInfo] = ComVisModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    module_start_delay: float | int = -0.5
    mean_frequency_step: float = 24
    width: int = 1200
    height: int = 800


def provide_module():
    return ComVisModule
