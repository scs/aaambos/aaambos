from __future__ import annotations

import os
import select
import subprocess
from collections import defaultdict
from datetime import datetime
from typing import Type, TYPE_CHECKING

import PySimpleGUI as sg
from attrs import define

from aaambos.core.communication.service import CommunicationService
from aaambos.core.communication.topic import Topic
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.configuration.run_config import RunConfig
from aaambos.core.module.base import ModuleInfo, Module, ModuleName
from aaambos.core.module.extension import Extension
from aaambos.core.module.feature import FeatureName, Feature, FeatureNecessity, SimpleFeatureNecessity, required, \
    optional
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgTopicSendingAttribute, MsgPayloadWrapperCallbackReceivingAttribute
from aaambos.std.extensions.module_status.module_status_extension import ModuleStatusInfoComFeatureIn, \
    ModuleStatusInfoComFeatureOut, ModuleStatusInfoPromise, ModuleStatusInfo, MODULE_STATUS_EXTENSION, \
    MODULE_STATUS_INFO, ModuleStatusesComFeatureOut, MODULE_STATUSES, ModuleStatuses, ModuleStatusesComFeatureIn, TERMINATED_STATUS
from aaambos.std.extensions.run_config_access.run_config_access import RunConfigAccessFeature
from aaambos.std.guis.pysimplegui.pysimplegui_window_ext import PySimpleGUIWindowExtensionFeature
from aaambos.std.supervision.instruction_run_time_manager.instruction_run_time_manager import ModuleStatesComFeatureIn, \
    ModuleManagerInstructionFeatureOut, ModuleStatesComFeatureOut, ModuleManagerInstructionFeatureIn, \
    ModuleStatesPromise, ModuleStates, ModuleState, ModuleManagerInstructionPromise, ModuleManagerInstruction, \
    Instruction, MODULE_MANAGER_INSTRUCTION, MODULE_STATES

if TYPE_CHECKING:
    from aaambos.std.guis.pysimplegui.window_extension import PySimpleGUIWindowExtension


# TODO add instruction to instruction run time manager

class ModuleStatusManager(Module, ModuleInfo):
    """
    Class for managing module statuses and states.

    Attributes:
        is_meta_module (bool): Flag indicating if the module is a meta module.
        com (MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService): Communication service and attributes.
        module_statuses (dict[ModuleName, list[ModuleStatusInfo]]): Dictionary storing module statuses.
        module_states (dict[ModuleName, list[ModuleState]]): Dictionary storing module states.
        config (ModuleStatusManagerConfig): Configuration for the module.
    """
    is_meta_module = True
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService
    module_statuses: dict[ModuleName, list[ModuleStatusInfo]]
    module_states: dict[ModuleName, list[ModuleState]]
    config: ModuleStatusManagerConfig

    def __init__(self, config: ModuleConfig, com: CommunicationService, log, ext: dict[str, Extension], run_config: RunConfig, *args,
                 **kwargs):
        """Custom init, that sets also the run_config and gui_window extension"""
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.run_config = run_config
        self.gui_window = kwargs["gui_window"] if "gui_window" in kwargs else None
        self.steps = 0
        self.fst_states_received = False
        self.cur_log = None
        self.log_modified = None
        self.time_last_received = None
        self.has_com_graph = False
        self.has_feature_graph = False
        self.subprocesses = []

    @classmethod
    @required(
        ModuleStatusInfoComFeatureIn,
        ModuleStatesComFeatureIn,
        ModuleManagerInstructionFeatureOut,
        ModuleStatusesComFeatureOut
    )
    def provides_features(cls, config: ModuleConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        """Provides: Input usage of ModuleStatusInfo and ModuleStates. Outputs ModuleManagerInstruction """
        pass

    @classmethod
    @required(
        ModuleStatusInfoComFeatureOut,
        ModuleStatesComFeatureOut,
        ModuleManagerInstructionFeatureIn,
        RunConfigAccessFeature,
        ModuleStatusesComFeatureOut,
    )
    def requires_features(cls, config: ModuleStatusManagerConfig = ...) -> dict[FeatureName, tuple[Feature, FeatureNecessity]]:
        """Requires: ModuleStatusInfo, ModuleStates Outputs from other modules, ModuleManagerInstruction input. And the extensions: RunConfigAccess and the PySimpleGUIWindowExtension """
        features = {}
        if config.gui:
            features.update({PySimpleGUIWindowExtensionFeature.name: (PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required)})
        return features

    async def initialize(self):
        """Set class attributes, set up the GUI, and register callback methods for communication promises. """
        self.module_statuses = defaultdict(list)
        self.module_states = defaultdict(list)

        if self.config.gui:
            self.gui_window.set_event_handler(self.handle_window_event)
            self.gui_window.window_closed_handler = self.window_closed_handler
        self.has_com_graph = (self.run_config.general.get_instance_dir() / "com-graph.svg").is_file()
        self.has_feature_graph = (self.run_config.general.get_instance_dir() / "feature-graph.svg").is_file() and (self.run_config.general.get_instance_dir() / "feature-graph-wo-com.svg").is_file()

        if self.config.gui:
            self.gui_window.setup_window(window_title="Module Status Manager", layout=self.create_layout(), theme=self.config.theme)
        await self.com.register_callback_for_promise(self.prm[MODULE_STATUS_INFO], self.handle_module_status_info)
        await self.com.register_callback_for_promise(self.prm[MODULE_STATES], self.handle_module_states)

    async def handle_module_status_info(self, topic: Topic, msg: ModuleStatusInfo):
        """
        Handle module status info.

        This method is responsible for handling module status information. It updates the module status information history and GUI window.

        Args:
            self: The object itself.
            topic: The topic of module status information.
            msg: The module status information.

        Returns:
            None.
        """
        if self.module_statuses[msg.module_name]:
            last_status = self.module_statuses[msg.module_name][-1].module_status
        else:
            last_status = "-"
        if last_status != msg.module_status:
            self.module_statuses[msg.module_name].append(msg)
            if self.config.gui:
                self.gui_window.window[f"status_{msg.module_name}"](msg.module_status)
            await self.com.send(self.tpc[MODULE_STATUSES], ModuleStatuses(updates={msg.module_name: 1}, histories=self.module_statuses))

    async def handle_module_states(self, topic: Topic, msg: ModuleStates):
        """
        Handles the module states received from a topic and updates the GUI window accordingly.

        Args:
            topic (Topic): The topic from which the module states are received.
            msg (ModuleStates): The module states message received.

        """
        if self.time_last_received:
            modules_to_update = msg.updated
        else:
            modules_to_update = list(msg.module_states.keys())
        if not self.time_last_received or self.time_last_received < msg.time:
            for module_name in modules_to_update:
                self.module_states[module_name].append(msg.module_states[module_name])
                if module_name == self.name:
                    continue
                if self.config.gui:
                    self.gui_window.window[f"time_{module_name}"](str(msg.time))
                    match msg.module_states[module_name]:
                        case ModuleState.STARTED:
                            self.gui_window.window[f"state_{module_name}"].update("STARTED", background_color="Green")
                            self.gui_window.window[f"action_{module_name}"].update("Terminate", disabled=False)
                        case ModuleState.FAILED:
                            self.gui_window.window[f"state_{module_name}"].update("FAILED", background_color="Red")
                            self.gui_window.window[f"action_{module_name}"].update("Start", disabled=False)
                        case ModuleState.TERMINATED:
                            self.gui_window.window[f"state_{module_name}"].update("TERMINATED", background_color="Black")
                            self.gui_window.window[f"action_{module_name}"].update("Start", disabled=False)
                            self.module_statuses[module_name] = [ModuleStatusInfo(module_name=module_name, module_status=TERMINATED_STATUS, extra_info={"terminator": "GUI"})]
                            if self.config.gui:
                                self.gui_window.window[f"status_{module_name}"](TERMINATED_STATUS)
                            await self.com.send(self.tpc[MODULE_STATUSES], ModuleStatuses(updates={module_name: 1},
                                                                                          histories=self.module_statuses))

            is_first_time = not self.time_last_received
            self.time_last_received = msg.time
            if is_first_time:
                await self.com.send(self.tpc[MODULE_MANAGER_INSTRUCTION],
                                    ModuleManagerInstruction([], Instruction.STOP_SENDING_INIT_STATUS))

    def terminate(self, control_msg: ControlMsg = None) -> int:
        super().terminate(control_msg)
        # # not necessary because aaambos already kills everything
        # for subprocess in self.subprocesses:
        #    if subprocess.is_alive():
        #        subprocess.terminate()
        return 0

    async def terminate_all(self):
        """

            Terminates all started modules.

            This method terminates all modules that have been started, except for the RunTimeManager and the current module itself. It sends a termination instruction to each module and updates
        * the corresponding GUI window.

            Args:
                self: The current instance of the class.

            Returns:
                None

        """
        self.log.warning("Terminate all started modules")
        for module_name in self.module_states:
            if module_name in ["RunTimeManager", self.name]:
                continue
            if self.module_states[module_name]:
                match self.module_states[module_name][-1]:
                    case ModuleState.STARTED:
                        await self.com.send(self.tpc[MODULE_MANAGER_INSTRUCTION], ModuleManagerInstruction([module_name], Instruction.TERMINATE))
                        if self.config.gui:
                            self.gui_window.window[f"action_{module_name}"].update(disabled=True)
                    case _:
                        pass

    async def start_all(self):
        """
        Start all terminated or failed modules.

        This method is used to start all terminated or failed modules. It iterates through the module states and sends a start instruction to each module that is not currently running.

        Returns:
            None

        Example usage:
            await start_all(self)
        """
        self.log.warning("Start all terminated or failed modules")
        for module_name in self.module_states:
            if module_name in ["RunTimeManager", self.name]:
                continue
            if self.module_states[module_name]:
                match self.module_states[module_name][-1]:
                    case ModuleState.STARTED:
                        pass
                    case _:
                        await self.com.send(self.tpc[MODULE_MANAGER_INSTRUCTION], ModuleManagerInstruction([module_name], Instruction.START))
                        if self.config.gui:
                            self.gui_window.window[f"action_{module_name}"].update(disabled=True)

    async def handle_window_event(self, event, values):
        """
        Handles a window event.

        Args:
            event (str): The name of the event triggered.
            values: The values associated with the event triggered.
        """
        if event.startswith("action_"):
            module_name = event[len("action_"):]
            if module_name == self.name:
                return
            if self.module_states[module_name]:
                match self.module_states[module_name][-1]:
                    case ModuleState.STARTED:
                        self.log.info(f"Terminate module {module_name!r}")
                        await self.com.send(self.tpc[MODULE_MANAGER_INSTRUCTION], ModuleManagerInstruction([module_name], Instruction.TERMINATE))
                        self.gui_window.window[event].update(disabled=True)
                    case _:
                        self.log.info(f"Start module {module_name!r}")
                        await self.com.send(self.tpc[MODULE_MANAGER_INSTRUCTION], ModuleManagerInstruction([module_name], Instruction.START))
                        self.gui_window.window[event].update(disabled=True)
        if event.startswith("logs_"):
            module_name = event[len("logs_"):]
            if self.cur_log:
                self.gui_window.window[f"logs_{self.cur_log}"].update("Show Logs")
            if module_name != self.cur_log:
                if not os.path.isfile(self.run_config.logging.log_dir / f"{module_name}.log"):
                    self.gui_window.window["frame_log"].update(module_name)
                    self.gui_window.window["ml_log"].update("<<Log File does not exist>>")
                    self.cur_log = None
                    self.log_modified = None
                else:
                    self.gui_window.window[event].update("Stop Logs")
                    self.cur_log = module_name
                    self.log_modified = None
                    self.gui_window.window["frame_log"].update(module_name)
            else:
                self.cur_log = None
                self.log_modified = None
        if event == "terminate_all":
            await self.terminate_all()
        if event == "start_all":
            await self.start_all()
        if event == "show_com_graph":
            self.subprocesses.append(subprocess.Popen(["eog", self.run_config.general.get_instance_dir() / "com-graph.svg"]))
        if event == "show_feature_graph":
            if "without-com-features" in values and values["without-com-features"]:
                self.subprocesses.append(subprocess.Popen(["eog", self.run_config.general.get_instance_dir() / "feature-graph-wo-com.svg"]))
            else:
                self.subprocesses.append(subprocess.Popen(["eog", self.run_config.general.get_instance_dir() / "feature-graph.svg"]))

    async def window_closed_handler(self):
        self.log.warning("Window closed")

    def create_layout(self) -> list:
        """

        Create a layout for the application.

        Returns:
            list: The layout for the application.

        """
        name_col = [[sg.Text("Module Name")]]
        status_col = [[sg.Text("Status")]]
        state_col = [[sg.Text("State")]]
        button_col = [[sg.Text("Action")]]
        time_col = [[sg.Text("Time")]]
        log_col = [[sg.Text("Logs")]]
        modules_with_status_extension = set(self.run_config.settings.feature_graph.get_all_module_names_with_extensions(MODULE_STATUS_EXTENSION))
        for module_name in self.run_config.settings.feature_graph.get_all_module_names():  #self.run_config.settings.feature_graph.get_all_module_names_with_extensions(MODULE_STATUS_EXTENSION):
            name_col.append([sg.Text(module_name, size=(20,2))])
            status_col.append([sg.Text("-" if module_name in modules_with_status_extension else "??", key=f"status_{module_name}", size=(20,2))])
            state_col.append([sg.Text("-", key=f"state_{module_name}", size=(20,2))])
            button_col.append([sg.Button("-", key=f"action_{module_name}", size=(20,1))])
            time_col.append([sg.Text("-", key=f"time_{module_name}", size=(20,2))])
            log_col.append([sg.Button("Show Logs", key=f"logs_{module_name}", size=(20,1))])

        plot_buttons = []
        if self.has_feature_graph:
            plot_buttons.append(sg.Checkbox("Without Com Features", default=True, key="without-com-features"))
            plot_buttons.append(sg.Button("Show Feature Graph", key="show_feature_graph"))
        if self.has_com_graph:
            plot_buttons.append(sg.Button("Show Communication Graph", key="show_com_graph"))
        return [
            [sg.Button("Terminate All", key="terminate_all"), sg.Button("Start All", key="start_all"), sg.Text(self.run_config.general.get_instance_id(), background_color="#404040")],
            [sg.Column(layout=name_col), sg.Column(layout=status_col), sg.Column(layout=state_col), sg.Column(layout=button_col), sg.Column(layout=time_col), sg.Column(layout=log_col)],
            [sg.Frame(title="Logs", layout=[[sg.Multiline("----", size=(140,self.config.log_number_of_lines + 1), key="ml_log")]], key="frame_log")],
            plot_buttons,
        ]

    async def step(self):
        if self.config.gui:
            await self.gui_window.step()
            if self.steps == 0:
                self.gui_window.window[f"action_{self.name}"].update("Disabled", disabled=True)
            if self.cur_log:
                file_name = self.run_config.logging.log_dir / f"{self.cur_log}.log"
                log_modified = os.path.getmtime(file_name)
                if self.log_modified != log_modified:
                    self.log_modified = log_modified
                    text = subprocess.run(['tail', str(file_name), "-n", str(self.config.log_number_of_lines)], capture_output=True)
                    self.gui_window.window["ml_log"].update(text.stdout.decode())
        self.steps += 1

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        """Use the ModuleStatusManagerConfig as a config."""
        return ModuleStatusManagerConfig


@define(kw_only=True)
class ModuleStatusManagerConfig(ModuleConfig):
    module_path: str = "aaambos.std.modules.module_status_manager"
    module_info = ModuleStatusManager
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    module_start_delay: float | int = -0.5
    log_number_of_lines = 24
    mean_frequency_step: float = 10
    theme: str = "Dark2"
    # terminate_arch_with_closing: bool = True  # TODO
    gui: bool = True


def provide_module():
    return ModuleStatusManager


