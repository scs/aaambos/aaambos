import asyncio
import signal
import sys
import time
from multiprocessing import Process
from typing import Type

from attrs import define
from msgspec import Struct
from semantic_version import Version, SimpleSpec

from aaambos.core.communication.service import CommunicationService
from aaambos.core.communication.topic import Topic
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, CommunicationFeature, \
    SimpleFeatureNecessity
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgTopicCallbackReceivingAttribute, MsgTopicSendingAttribute
from aaambos.std.communications.utils import create_promise_and_unit
from aaambos.std.extensions.module_status.module_status_extension import ModuleStatusExtensionFeature, \
    ModuleStatusInfoComFeatureOut, ModuleStatusExtension, ModuleStatusesComFeatureIn

WAIT_UNTIL_SEND = 0.2


@define(kw_only=True)
class PingPongModuleConfig(ModuleConfig):
    fst_send: bool
    expected_start_up_time: float | int = 0
    module_path: str = "aaambos.std.modules.ping_pong_example"


class CountingMsg(Struct):
    """The Counting Wrapper, the count and the last module that increased the count"""
    value: int
    last_module: str


Counting = "Counting"
"""The name and leaf topic of the message, etc."""

CountingPromise, CountingUnit = create_promise_and_unit(
    name=Counting,
    payload_wrapper=CountingMsg,
    unit_description="Msg that contains increased value.",
    frequency=1/WAIT_UNTIL_SEND,
)
"""The promise of the counting."""


CountingFeature = CommunicationFeature(
    name="CountingFeature",
    version=Version("0.0.1"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.0.1"),
    required_input=[CountingPromise],
    provided_output=[CountingPromise],
)
"""The counting com feature. Each module provides and requires the (in and out) promise"""


class PingPongModule(Module, ModuleInfo):
    """An example module that send messages (counting) between to instances"""
    config: PingPongModuleConfig
    com: CommunicationService | MsgTopicCallbackReceivingAttribute | MsgTopicSendingAttribute

    def __init__(self, config: PingPongModuleConfig, status: ModuleStatusExtension, *args, **kwargs):
        super().__init__(config, *args, **kwargs)
        self.counter = 0
        self.steps = 0
        self.started_process = False
        self.process = None
        self.status = status

    async def initialize(self):
        """

        Async method to initialize the software module.

        This method performs the necessary initialization steps for the module. It enables the counting feature if it is configured in the software's settings. If the counting feature is enabled
        *, it registers a callback for a specific promise.

        If the 'fst_send' configuration setting is False, it executes some specific actions by calling the `execute_based_on_status` method of the 'status' object. These actions include calling
        * the `do_stuff` method with different arguments and specific status and module information.

        After these actions, the method waits for a brief moment using `asyncio.sleep(0.1)` to ensure asynchronous behavior. Then, it sets the module status to "initialized" with additional
        * module-specific information.

        Returns:
            None

        """
        if self.config.ft_info.com_features[CountingFeature.name]:
            self.log.info("Counting Feature enabled")
            await self.com.register_callback_for_promise(self.prm[Counting], self.handle_increased)
        if not self.config.fst_send:
            self.status.execute_based_on_status(self.do_stuff(my_argument="123456"), status="started counting", on_module_name="ping")
            self.status.execute_based_on_status(self.do_stuff(my_argument="ABCDEFG"), status="started counting")
            self.status.execute_based_on_status(self.do_stuff(my_argument="abcdefg"), status="started counting", on_extra_info={"count": "start"})
        await asyncio.sleep(0.1)  # does not work otherwise (to send module status)
        await self.status.set_module_status("initialized", {"color": "blue"})

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[str, tuple[Feature, FeatureNecessity]]:
        """
        Provides features based on the given configuration.

        Args:
            config (ModuleConfig): The configuration for which the features are being provided.

        Returns:
            dict[str, tuple[Feature, FeatureNecessity]]: A dictionary containing the provided features and their necessities.

        Example:
            >>> config = ModuleConfig()
            >>> provides_features(config)
            {
                'CountingFeature': (CountingFeature, SimpleFeatureNecessity.Required),
                'ModuleStatusInfoComFeatureOut': (ModuleStatusInfoComFeatureOut, SimpleFeatureNecessity.Required),
                'ModuleStatusesComFeatureIn': (ModuleStatusesComFeatureIn, SimpleFeatureNecessity.Required)
            }

        """
        return {
            CountingFeature.name: (CountingFeature, SimpleFeatureNecessity.Required),
            ModuleStatusInfoComFeatureOut.name: (ModuleStatusInfoComFeatureOut, SimpleFeatureNecessity.Required),
            ModuleStatusesComFeatureIn.name: (ModuleStatusesComFeatureIn, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[str, tuple[Feature, FeatureNecessity]]:
        """
        Args:
            config: A module configuration object that specifies the required features.

        Returns:
            A dictionary containing the names of the required features as keys, and tuples consisting of the corresponding feature class and feature necessity as values.

        Example:
            >>> config = ModuleConfig()
            >>> required_features = requires_features(config)
            >>> required_features
            {
                'ModuleStatusExtensionFeature': (ModuleStatusExtensionFeature, SimpleFeatureNecessity.Required),
            }
        """
        return {
            ModuleStatusExtensionFeature.name: (ModuleStatusExtensionFeature, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return PingPongModuleConfig

    async def handle_increased(self, topic: Topic, msg: CountingMsg):
        """
        Handles the increased message by updating the counter and sending a new message.

        Args:
            topic (Topic): The topic of the message.
            msg (CountingMsg): The message containing the counter value.

        """
        if msg.last_module != self.name:
            if self.counter == 0:
                await self.status.set_module_status("received and continue counting", {"color": "green"})
            self.counter = msg.value
            self.counter += 1
            # self.log.info(f"{self.name}: {self.counter}")
            # await asyncio.sleep(WAIT_UNTIL_SEND)
            await self.com.send(self.tpc[Counting], CountingMsg(self.counter, self.name))
            # == await self.com.send(self.tpc[Counting], self.wpr[Counting](self.counter, self.name))

    async def step(self):
        """
        This method is used to perform a step in the process. It checks the configuration parameters, performs certain actions based on those parameters, and updates the status accordingly.

        Returns:
            None
        """
        if self.config.fst_send and not self.counter and self.steps > 5:
            self.counter += 1
            self.log.info(f"Start {self.name}, {self.counter}")
            await self.com.send(self.tpc[Counting], CountingMsg(self.counter, self.name))
            await self.status.set_module_status("started counting", {"color": "green", "count": "start"})
        self.steps += 1
        # if self.config.fst_send:
        #     self.log.info(self.steps)
        # if self.config.fst_send and self.steps > 4:
        #    sys.exit(0)
        if self.steps > 10 and not self.started_process and self.config.fst_send:
            await self.status.set_module_status("started subprocess", {"color": "darkgreen"})
            self.process = Process(target=do_stuff, kwargs={"parent_name": self.name})
            self.process.start()
            self.started_process = True

    def terminate(self, control_msg: ControlMsg = None) -> int:
        super().terminate(control_msg)
        if self.process:
            self.process.terminate()
        return 0

    async def do_stuff(self, my_argument):
        self.log.info(f"Do here {my_argument}")


def provide_module():
    return PingPongModule


def do_stuff(parent_name):
    """
    Args:
        parent_name: The name of the parent process. Will be used in the log message to identify the module.

    """
    from loguru import logger
    logger_format = (
        "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | "
        "<level>{level: <8}</level> | "
        "{extra[module]} | "
        "<cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - "
        "<level>{message}</level>"
    )
    logger.configure(extra={"module": f"{parent_name}/{'subprocess'}"})
    logger.remove()
    logger.add(sys.stderr, format=logger_format, level="TRACE")

    def handle_sigint(sig, event):
        logger.trace("sigint subprocess")
        sys.exit(0)

    signal.signal(signal.SIGINT | signal.SIGTERM, handle_sigint)
    while True:
        logger.trace("subprocess")
        time.sleep(1)