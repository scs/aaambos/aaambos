import os
import subprocess
import sys
from typing import Type

from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature


@define(kw_only=True)
class ShellCommandConfig(ModuleConfig):
    shell_command: list[str] | str
    restart_after_failure: bool = False
    module_path: str = "aaambos.std.modules.shell_command"
    expected_start_up_time: float | int = 0


class ShellCommandModule(Module, ModuleInfo):
    """
    Class ShellCommandModule

    A class that represents a shell command module that executes a shell command and provides necessary methods for initialization, execution, and termination.

    Attributes:
        config (ShellCommandConfig): The configuration object for the shell command module.
        p_open (subprocess.Popen): The Popen object representing the running shell command process.

    """
    config: ShellCommandConfig
    p_open: subprocess.Popen

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature, FeatureNecessity]:
        pass

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature, FeatureNecessity]:
        pass

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return ShellCommandConfig

    async def initialize(self):
        """
        Initializes the subprocess Popen object.

        This method initializes the Popen object by executing the shell command specified in the configuration. The
        shell command is obtained from the `config.shell_command` attribute. The command is expanded to include the
        user's home directory, if necessary.

        Returns:
            None

        Example:
            >>> await initialize()
            # Subprocess Popen object is initialized
        """
        self.p_open = subprocess.Popen([os.path.expanduser(x) for x in self.config.shell_command])

    async def step(self):
        """
        Executes the next step of the process.

        This method checks if the shell command has finished executing. If the command has finished,
        it logs the return code and terminates the process.

        Returns:
            None

        """
        if self.p_open.poll() is not None:
            self.log.info(f"Shell command finished with {self.p_open.returncode}")
            return_code = self.terminate()
            sys.exit(return_code)

    def terminate(self, control_msg = None) -> int:
        if self.p_open.returncode is None:
            self.log.info("Terminate shell command (Popen)")
            self.p_open.terminate()
        super().terminate()
        return self.p_open.returncode


def provide_module():
    return ShellCommandModule