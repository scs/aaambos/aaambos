"""
Some standard modules come with *aaambos*.

### `aaambos.std.modules.module_status_manager`
Gathers the module statuses and has a GUI to visualize them.
Further, it can start and terminate modules (instructed via the GUI) with the `aaambos.std.supervision.instruction_run_time_manager` (not the standard one).
It can also show the logs of a module.

### `aaambos.std.modules.ping_pong_example`
A simple example of a module that sends messages (ping pong) between two instances of itself.
It also creates a subprocess to check if subprocess are terminated correctly (without the extension).

### `aaambos.std.modules.ping_pong_example_iu`
The same as the example above but it uses a IU instead of messages.

### `aaambos.std.modules.shell_command`
The module that just starts a shell command and let it run.

"""