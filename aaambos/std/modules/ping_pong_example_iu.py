from typing import Type

from attrs import define
from semantic_version import Version, SimpleSpec

from aaambos.core.communication.service import CommunicationService, to_attribute_dict
from aaambos.core.communication.topic import Topic
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, CommunicationFeature, \
    SimpleFeatureNecessity
from aaambos.std.communications.attributes import IncrementalUnitTopicWrapperAttribute
from aaambos.std.communications.utils import create_promise_and_unit, IUEvent
from aaambos.std.modules.ping_pong_example import CountingMsg

WAIT_UNTIL_SEND = 0.2


@define(kw_only=True)
class PingPongIUModuleConfig(ModuleConfig):
    fst_send: bool
    expected_start_up_time: float | int = 0
    module_path: str = "aaambos.std.modules.ping_pong_example_iu"


Counting = "Counting"
"""The topic used for the counting."""

CountingPromise, CountingUnit = create_promise_and_unit(
    name=Counting,
    payload_wrapper=CountingMsg,
    unit_description="Msg that contains increased value.",
    frequency=1/WAIT_UNTIL_SEND,
    required_com_attr=to_attribute_dict([IncrementalUnitTopicWrapperAttribute])
)
"""The promise and unit for the counting"""

CountingFeature = CommunicationFeature(
    name="CountingFeature",
    version=Version("0.0.1"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.0.1"),
    required_input=[CountingPromise],
    provided_output=[CountingPromise],
)
"""The Com feature for the counting. Both require and provide the count."""


class PingPongIUModule(Module, ModuleInfo):
    """Example Module that does the Ping Pong with two instances but uses a IU instead of messages."""
    config: PingPongIUModuleConfig
    com: CommunicationService | IncrementalUnitTopicWrapperAttribute

    def __init__(self, config: PingPongIUModuleConfig, *args, **kwargs):
        """

        Args:
            config:
            *args:
            **kwargs:
        """
        super().__init__(config, *args, **kwargs)
        self.counter = 0
        self.steps = 0
        self.iu_own = None

    async def initialize(self):
        if self.config.ft_info.com_features[CountingFeature.name]:
            self.log.info("Counting Feature enabled")
            await self.com.register_callback_iu_for_promise(self.prm[Counting], self.handle_increased)

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[str, tuple[Feature, FeatureNecessity]]:
        return {CountingFeature.name: (CountingFeature, SimpleFeatureNecessity.Required)}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[str, tuple[Feature, FeatureNecessity]]:
        pass

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return PingPongIUModuleConfig

    async def handle_increased(self, topic: Topic, msg: CountingMsg, event: IUEvent, iu: 'IU', is_own: bool):
        """

        Args:
            topic:
            msg:
            event:
            iu:
            is_own:

        Returns:

        """
        if self.iu_own and is_own:
            return
        self.counter = msg.value
        self.counter += 1
        self.log.info(f"{self.name}: {self.counter}")
        if self.iu_own:
            await self.com.set_payload_iu(self.iu_own, CountingMsg(self.counter, self.name))
        else:
            self.iu_own = await self.com.create_and_send_iu(self.tpc[Counting], CountingMsg(self.counter, self.name))
    
    async def step(self):
        if self.config.fst_send and not self.counter and self.steps > 5:
            self.counter += 1
            self.log.info(f"Start {self.name}, {self.counter}")
            self.iu_own = await self.com.create_and_send_iu(self.tpc[Counting], CountingMsg(self.counter, self.name))
        self.steps += 1


def provide_module():
    return PingPongIUModule
