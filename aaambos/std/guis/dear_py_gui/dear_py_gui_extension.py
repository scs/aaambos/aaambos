from typing import Type, Tuple

import dearpygui.dearpygui as dpg
from attrs import define, asdict

from semantic_version import Version, SimpleSpec

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature

DEAR_PY_GUI_EXTENSION_EXTENSION = "DearPyGUIExtensionExtension"


class DearPyGUIExtensionExtension(Extension):
    """
    DearPyGUIExtensionExtension

    Extension class for integrating Dear PyGui with the application.

    Attributes:
        name (str): The name of the extension.
        layout_creation_func (Callable): The function responsible for creating the GUI layout.
        step_update_func (Callable): The function responsible for updating the GUI on each step.
        window_close_handler (Callable): The function to be called when the GUI window is closed.
        window_config (dict): The configuration for the GUI window.
        window_closed_call (bool): Flag to indicate if the window close handler should be called when the window is closed.
    """
    name = DEAR_PY_GUI_EXTENSION_EXTENSION
    layout_creation_func = None
    step_update_func = None
    window_close_handler = None
    window_config: dict | None = None
    window_closed_call: bool = True

    async def before_module_initialize(self, module):
        self.module = module

    async def after_module_initialize(self, module):
        ...

    def initialize_window(self):
        dpg.create_context()
        if self.layout_creation_func:
            self.layout_creation_func()

        if self.window_config is None:
            self.set_viewport_options()
        dpg.create_viewport(**self.window_config)
        dpg.setup_dearpygui()
        dpg.show_viewport()

    def set_layout_creation_func(self, layout_creation_func):
        self.layout_creation_func = layout_creation_func

    def set_step_update_func(self, step_update_func):
        self.step_update_func = step_update_func

    def set_window_close_handler(self, window_close_handler):
        self.window_close_handler = window_close_handler

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return DearPyGuiExtensionExtensionSetup

    def set_viewport_options(self, *, title: str = ..., small_icon: str = '', large_icon: str = '', width: int = 1280,
                             height: int = 800, x_pos: int = 100, y_pos: int = 100, min_width: int = 250,
                             max_width: int = 10000, min_height: int = 250, max_height: int = 10000,
                             resizable: bool = True, vsync: bool = True, always_on_top: bool = False,
                             decorated: bool = True, clear_color: list[float] | Tuple[float, ...] = (0, 0, 0, 255),
                             disable_close: bool = False, **kwargs):
        """
        Args:
            title: A string representing the title of the window. Default: self.module.name + " GUI"
            small_icon: A string representing the path to the small icon of the window. Default: ''
            large_icon: A string representing the path to the large icon of the window. Default: ''
            width: An integer representing the width of the window. Default: 1280
            height: An integer representing the height of the window. Default: 800
            x_pos: An integer representing the x-position of the window. Default: 100
            y_pos: An integer representing the y-position of the window. Default: 100
            min_width: An integer representing the minimum width of the window. Default: 250
            max_width: An integer representing the maximum width of the window. Default: 10000
            min_height: An integer representing the minimum height of the window. Default: 250
            max_height: An integer representing the maximum height of the window. Default: 10000
            resizable: A boolean indicating whether the window is resizable. Default: True
            vsync: A boolean indicating whether vsync is enabled. Default: True
            always_on_top: A boolean indicating whether the window is always on top. Default: False
            decorated: A boolean indicating whether the window is decorated. Default: True
            clear_color: A list or tuple of floats representing the clear color of the window in RGBA format. Default: (0, 0, 0, 255)
            disable_close: A boolean indicating whether the close button is disabled. Default: False
            **kwargs: Additional keyword arguments for future compatibility.

        """
        self.window_config = {
            "title": f"{self.module.name} GUI" if title is ... else title,
            "small_icon": small_icon,
            "large_icon": large_icon,
            "width": width,
            "height": height,
            "x_pos": x_pos,
            "y_pos": y_pos,
            "min_width": min_width,
            "max_width": max_width,
            "min_height": min_height,
            "max_height": max_height,
            "resizable": resizable,
            "vsync": vsync,
            "always_on_top": always_on_top,
            "decorated": decorated,
            "clear_color": clear_color,
            "disable_close": disable_close,
        }

    async def step(self):
        if dpg.is_dearpygui_running():
            if self.step_update_func:
                self.step_update_func()
            dpg.render_dearpygui_frame()
        elif self.window_closed_call:
            self.window_closed_call = False
            if self.window_close_handler:
                self.window_close_handler()

    def terminate(self):
        if dpg.is_dearpygui_running():
            dpg.destroy_context()


class DearPyGuiExtensionExtensionSetup(ExtensionSetup):
    """
    Class representing the setup for the DearPyGuiExtension extension.

    This class extends the `ExtensionSetup` class and provides the necessary
    configuration for setting up the DearPyGuiExtension extension.
    """
    def before_module_start(self, module_config) -> Extension:
        return DearPyGUIExtensionExtension()


DearPyGUIExtensionExtensionFeature = ExtensionFeature(
    name=DEAR_PY_GUI_EXTENSION_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=DearPyGuiExtensionExtensionSetup,
    as_kwarg=True,
    kwarg_alias="dearpygui_window",
)
