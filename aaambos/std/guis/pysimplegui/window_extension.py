from typing import Callable, Any, Awaitable, Type
import PySimpleGUI as sg

from aaambos.core.module.extension import Extension, ExtensionSetup


class PySimpleGUIWindowExtension(Extension):
    """An Extension that creates a GUI for the module.

    The module should call the `setup_window`method for setting up the window with a defined layout.

    Further, it should set the event handler callback.

    Example Usage: `aaambos.std.modules.module_status_manager` module.
    """
    window: sg.Window | None = None
    event_handler: Callable[[str, Any], Awaitable]
    window_event_listen_time: int = 10
    layout: list
    is_window_up = False
    window_title: str
    window_closed_handler: Callable[[], Awaitable] = None

    async def before_module_initialize(self, module):
        self.window_title = f"{module.name} Window"

    async def after_module_initialize(self, module):
        pass

    def set_event_handler(self, event_handler: Callable[[str, Any], Awaitable]):
        """
        Args:
            event_handler (Callable[[str, Any], Awaitable]): The event handler function to be set.
                It should be a callable object that takes two parameters: a string representing the event name
                and any extra data associated with the event. The function should return an awaitable object.
        """
        self.event_handler = event_handler

    def set_window_event_listen_time(self, ms):
        """
        Set the window event listen time.

        Args:
            ms: The time in milliseconds to set as the window event listen time.
        """
        self.window_event_listen_time = ms

    def set_layout(self, layout):
        """
        Args:
            layout: The layout to be set for the object.

        """
        self.layout = layout

    def setup_window(self, window_title=..., theme="Dark2", **kwargs):
        """
        Args:
            window_title: A string representing the title of the window. If not provided, the default value will be used.
            theme: A string representing the theme of the window. The default value is "Dark2".
            **kwargs: Additional keyword arguments to be passed to the sg.Window constructor.

        """
        if "layout" in kwargs:
            self.layout = kwargs["layout"]
        kwargs["layout"] = self.layout
        if not self.layout:
            raise ValueError(f"{self.__class__.name} requires to call set_layout with a not empty layout before setup_window is callable or provide it with the argument layout to the setup_window method.")
        if window_title is not ...:
            self.window_title = window_title
        sg.theme(theme)
        self.window = sg.Window(self.window_title, **kwargs)
        self.is_window_up = True

    async def step(self):
        if self.is_window_up:
            event, values = self.window.read(timeout=self.window_event_listen_time)
            if event == sg.WIN_CLOSED:  # if user closes window or clicks cancel
                self.terminate()
                if self.window_closed_handler:
                    await self.window_closed_handler()
                return
            if self.event_handler:
                await self.event_handler(event, values)

    def terminate(self):
        if self.window:
            self.window.close()
        self.is_window_up = False

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        from aaambos.std.guis.pysimplegui.pysimplegui_window_ext import PySimpleGUIWindowExtensionSetup
        return PySimpleGUIWindowExtensionSetup
