from __future__ import annotations

import importlib
import sys
from typing import Callable, TYPE_CHECKING, Any, Type

import PySimpleGUI as sg
from attrs import define, Factory

from aaambos.core.communication.service import CommunicationService
from aaambos.core.communication.topic import LeafTopic, Topic, HierarchicalTopic
from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from aaambos.std.communications.attributes import MsgTopicSendingAttribute, MsgTopicCallbackReceivingAttribute, \
    RunTimeReceivingNewTopicsAttribute
from aaambos.std.extensions.run_config_access.run_config_access import RunConfigAccessFeature
from aaambos.std.guis.pysimplegui.com_promises import PySimpleGUIInfoOutFeature as InfoOut, PySimpleGUIInfoInFeature as InfoIn, \
    PySimpleGUIInfo, PySimpleGUIInfoLeafTopic as Info
from aaambos.std.guis.pysimplegui.pysimplegui_ext import PY_SIMPLE_GUI_EXTENSION
if TYPE_CHECKING:
    from aaambos.core.configuration.run_config import RunConfig


class PySimpleGUIWindowModule(Module, ModuleInfo):
    """The module that encapsulates all GUI information gathered with the PySimpleGUIExtension and own definitions (like which payloads to visalize).
    """
    window: sg.Window
    modules_to_show: list[str]
    config: PySimpleGUIWindowConfig
    com: CommunicationService | MsgTopicSendingAttribute | MsgTopicCallbackReceivingAttribute | RunTimeReceivingNewTopicsAttribute
    is_meta_module = True

    def __init__(self, config: ModuleConfig, com, log, ext, run_config, *args, **kwargs):
        """
        Args:
            config: The ModuleConfig object for the software module.
            com: The communication object used for module communication.
            log: The logging object used for logging messages.
            ext: The external dependencies object used for accessing external resources.
            run_config: The RunConfig object representing the runtime configuration.
            *args: Additional positional arguments.
            **kwargs: Additional keyword arguments.
        """
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.run_config: RunConfig = run_config
        self.gui_info: dict[str, PySimpleGUIInfo] = {}
        self.msgs_to_show_text_fields: dict[str, sg.Text] = {}
        self.is_window_setup_done = False
        self.topics_to_keys = {}

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[str, tuple[Feature, FeatureNecessity]]:
        # maybe as a "start up" communication model -> auto request after restart
        return {InfoIn.name: (InfoIn, SimpleFeatureNecessity.Required)}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[str, tuple[Feature, FeatureNecessity]]:
        return {
            RunConfigAccessFeature.name: (RunConfigAccessFeature, SimpleFeatureNecessity.Required),
            InfoOut.name: (InfoOut, SimpleFeatureNecessity.Required)
        }  # add features from config?

    async def initialize(self):
        # get info about all assigned extensions from feature graph
        self.modules_to_show = list(self.run_config.settings.feature_graph.get_all_module_names_with_extensions(PY_SIMPLE_GUI_EXTENSION))
        await self.com.register_callback(self.tpc[Info], self.handle_gui_info)
        for leaf_topic, wrapper, keys in self.config.topics_to_show:
            # Does only work if the delimiter is the same (/not changed with a custom topic_naming_policy)
            self.topics_to_keys[leaf_topic] = keys
            if isinstance(wrapper, str):
                wrapper = self.import_entity(wrapper)
            await self.com.add_input_topic(HierarchicalTopic(
                self.run_config.settings.topic_prefix, leaf_topic),
                self.handle_topic_to_show,
                wrapper
            )

    async def handle_topic_to_show(self, topic: Topic, msg):
        self.msgs_to_show_text_fields[topic.name].update("\n".join([f"{k}:{getattr(msg, k)}" for k in self.topics_to_keys[topic.name]]))  # better layout?

    async def handle_gui_info(self, info: PySimpleGUIInfo):
        """
        Asynchronously handles the GUI information by storing it in the `gui_info` dictionary.

        Args:
            info (PySimpleGUIInfo): An instance of the `PySimpleGUIInfo` class containing the GUI information.

        """
        self.gui_info[info.module_name] = info

    async def step(self):
        # get info for vis from all assigned extensions
        # if all received -> get layout and start vis
        # listen to events
        if not self.is_window_setup_done and len(self.gui_info) >= len(self.modules_to_show):
            self.setup_window()
            # TODO add new topics from extensions to listen
        if self.is_window_setup_done:
            event, values = self.window.read(timeout=100)
            if event == sg.WIN_CLOSED:  # if user closes window or clicks cancel
                self.terminate()
                sys.exit(0)
            self.config.event_handler_func(self, event, values)

    def terminate(self, control_msg=None) -> int:
        if self.window:
            self.window.close()
        return 0

    def setup_window(self):
        """
        Sets up the application window based on the configuration.

        Returns:
            None

        """
        # TODO set sg backend based on config (web, tkinter etc)
        sg.theme(self.config.theme)

        layout = self.config.layout_func(module=self)

        self.window = sg.Window(self.config.window_title, [layout])
        self.is_window_setup_done = True

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return PySimpleGUIWindowConfig

    @staticmethod
    def import_entity(wrapper):
        module, entity = wrapper.rsplit(".", 1)
        module = importlib.import_module(module)
        return module.__dict__[entity]


def create_layout(module: PySimpleGUIWindowModule) -> list:
    """
    Args:
        module: A PySimpleGUI window module.

    Returns:
        list: The layout for the window.

    Description:
        This method creates a layout for a PySimpleGUI window based on the input module. It constructs a
        list of elements that defines the visual arrangement of the window.

        The method begins by initializing the layout with a single text element, "Window". Then, it checks if the
        module's config has any topics to show. If topics are found, it iterates over them and creates a list of text
        elements corresponding to each topic. Each text element is assigned to the module's msgs_to_show_text_fields
        dictionary with the corresponding leaf topic as the key.

        Finally, if topics are present, a frame is added to the layout with the title "Topics To Show" and the list
        of text elements as its content.

        The resulting layout is returned as a list.
    """
    layout = [sg.Text("Window")]
    if module.config.topics_to_show:
        topics_to_show = []
        for leaf_topic, _, keys in module.config.topics_to_show:
            text_element = sg.Text(size=(30, max(len(keys), 1)))  # change size according to wrapper?
            module.msgs_to_show_text_fields[leaf_topic] = text_element
            topics_to_show.append(text_element)
        layout.append(sg.Frame("Topics To Show", [topics_to_show]))
    return layout


def event_handler(module: PySimpleGUIWindowModule, event, values):
    ...  # TODO


@define(kw_only=True)
class PySimpleGUIWindowConfig(ModuleConfig):
    window_title: str
    theme: str = "Dark2"  # See https://user-images.githubusercontent.com/46163555/173203488-7fef44b5-000f-48b9-b353-625c214e41d4.png
    layout_func: Callable = create_layout
    event_handler_func: Callable = event_handler
    topics_to_show: list[list[LeafTopic, Any, list[str]]] = Factory(list)
    # window size?

    module_path: str = "aaambos.std.guis.pysimplegui.pysimplegui_window"
    module_info: Type[ModuleInfo] = PySimpleGUIWindowModule
    restart_after_failure: bool = False  # because of no info in ...
    expected_start_up_time: float | int = 0


def provide_module():
    return PySimpleGUIWindowModule
