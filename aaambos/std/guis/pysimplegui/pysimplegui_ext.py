from typing import Type

from semantic_version import Version, SimpleSpec

from aaambos.core.communication.topic import Topic
from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import CommunicationFeature, FeatureNecessity, SimpleFeatureNecessity, ExtensionFeature
from aaambos.std.guis.pysimplegui.com_promises import PySimpleGUIInfoOutFeature, PySimpleGUIInfoInFeature, \
    PySimpleGUIInfoPromise, PySimpleGUIInfo
from aaambos.utils.exchangeable_py import ExchangeFunc

PY_SIMPLE_GUI_EXTENSION = "PySimpleGUIExtension"
"""The name of the GUI Extension"""


class PySimpleGUIExtension(Extension):
    """An Extension for GUIs that does now have an own window but communicates to a window module it GUI definitions. "One GUI for several modules approach". """
    name = PY_SIMPLE_GUI_EXTENSION

    async def before_module_initialize(self, module):
        self.module = module

    async def after_module_initialize(self, module):
        pass

    async def setup_gui(self, layout_func: ExchangeFunc, topics_listening: dict[str, Topic], topics_sending: dict[str, Topic]):
        """
        Args:
            layout_func (ExchangeFunc): The layout function that determines the GUI layout.
            topics_listening (dict[str, Topic]): A dictionary containing the topics that the module will listen to.
            topics_sending (dict[str, Topic]): A dictionary containing the topics that the module will send information to.

        Returns:
            None

        """
        # send info to gui monitor module
        gui_info = PySimpleGUIInfo(
            module_name=self.name,
            layout_func=layout_func,
            topics_listening=topics_listening,
            topics_sending=topics_sending,
        )
        self.module.com.send(PySimpleGUIInfoPromise.settings.topic, gui_info)

    @classmethod
    def get_extension_setup(cls) -> Type[ExtensionSetup]:
        return PySimpleGUIExtensionSetup


class PySimpleGUIExtensionSetup(ExtensionSetup):
    def before_module_start(self, module_config) -> Extension:
        return PySimpleGUIExtension()

    @classmethod
    def provides_features(cls) -> dict[str, tuple[CommunicationFeature, FeatureNecessity]]:
        return {PySimpleGUIInfoOutFeature.name: (PySimpleGUIInfoOutFeature, SimpleFeatureNecessity.Required)}

    @classmethod
    def requires_features(cls) -> dict[str, tuple[CommunicationFeature, FeatureNecessity]]:
        return {
            PySimpleGUIInfoInFeature.name: (PySimpleGUIInfoInFeature, SimpleFeatureNecessity.Required),
        }


PySimpleGUIExtensionFeature = ExtensionFeature(
    name=PY_SIMPLE_GUI_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=PySimpleGUIExtensionSetup,
    as_kwarg=True,
    kwarg_alias="gui_access",
)