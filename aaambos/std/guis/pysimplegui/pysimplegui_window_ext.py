from __future__ import annotations

from typing import Callable, Any, Awaitable, Type

from semantic_version import SimpleSpec, Version

from aaambos.core.module.extension import Extension, ExtensionSetup
from aaambos.core.module.feature import ExtensionFeature

PY_SIMPLE_GUI_WINDOW_EXTENSION = "PySimpleGUIWindowExtension"


class PySimpleGUIWindowExtensionSetup(ExtensionSetup):
    def before_module_start(self, module_config) -> Extension:
        from aaambos.std.guis.pysimplegui.window_extension import PySimpleGUIWindowExtension
        return PySimpleGUIWindowExtension()


PySimpleGUIWindowExtensionFeature = ExtensionFeature(
    name=PY_SIMPLE_GUI_WINDOW_EXTENSION,
    version=Version("0.1.0"),
    requirements=[],
    version_compatibility=SimpleSpec(">=0.1.0"),
    extension_setup_class=PySimpleGUIWindowExtensionSetup,
    as_kwarg=True,
    kwarg_alias="gui_window",
)
"""The feature to add for an individual GUI for a module."""