from msgspec import Struct
from semantic_version import Version

from aaambos.core.communication.promise import CommunicationPromise, PromiseNecessity
from aaambos.core.communication.topic import Topic
from aaambos.core.communication.unit import CommunicationUnit
from aaambos.core.module.feature import CommunicationFeature
from aaambos.utils.exchangeable_py import ExchangeFunc

PySimpleGUIInfoLeafTopic = "PySimpleGUIInfo"


class PySimpleGUIInfo(Struct):
    """Info for GUI Extensions that just provide info for one unified GUI"""
    module_name: str
    layout_func: ExchangeFunc
    topics_listening: dict[str, Topic]
    topics_sending: dict[str, Topic]


PySimpleGUIInfoUnit = CommunicationUnit(
    name="PySimpleGUIInfoUnit",
    version=Version("0.1.0"),
    payload_wrapper=PySimpleGUIInfo,
    default_leaf_topic=PySimpleGUIInfoLeafTopic,
    description="Information about the layout of the PySimpleGUI for a module, the processing for events and msgs.",
    required_service_categories=[]  # TODO
    #service_requirements=CommunicationServiceRequirements(preferred_services=[], required_service_attributes=set()),
)
"""The Unit for the Info."""


PySimpleGUIInfoPromise = CommunicationPromise(
    communication_unit=PySimpleGUIInfoUnit,
    frequency=None,
    necessity=PromiseNecessity.REQUIRED,
    suggested_leaf_topic=PySimpleGUIInfoLeafTopic
)
"""The Promise for the Info"""


PySimpleGUIInfoOutFeature = CommunicationFeature(
    name="PySimpleGUIInfoOutFeature",
    version=Version("0.1.0"),
    requirements=[],
    provided_output=[PySimpleGUIInfoPromise],
    required_input=[],
)
"""The com out Feature of the Info"""

PySimpleGUIInfoInFeature = CommunicationFeature(
    name="PySimpleGUIInfoInFeature",
    version=Version("0.1.0"),
    requirements=[],
    provided_output=[],
    required_input=[PySimpleGUIInfoPromise],
)
"""The Com in feature of the Info"""
