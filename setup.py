#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = [
    "attrs>=23.1.0",
    "matplotlib>=3.7.2",
    "jsonargparse[all]>=4.21.1",
    "pluginlib>=0.9.0",
    "hyperpyyaml>=1.2.1",
    "loguru>=0.7.1",
    "networkx>=3.1",
    "cookiecutter>=2.3.0",
    # "pysimplegui==4.60.0",
    "PySimpleGUI@git+https://github.com/RobFlo98/PySimpleGUI-LGPLv3",
    "msgspec>=0.18.2",
    "semantic-version>=2.10.0",
    "ruff>=0.0.290",
    "platformdirs>=4.1.0",
    "dearpygui>=1.10.0",
]

test_requirements = ['pytest>=3', "pytest-cov>=6.0.0", "pytest-timeout>=2.3.1"]

setup(
    author="Florian Schroeder",
    author_email='florian.schroeder@uni-bielefeld.de',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="Architectures for Autonomous Agents: A Modular Basis with an Operating Supervisor",
    entry_points={
        'console_scripts': [
            'aaambos = aaambos.__main__:main',
        ],
    },
    install_requires=requirements,
    license="MIT License",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords=['aaambos'],
    name='aaambos',
    packages=find_packages(include=['aaambos', 'aaambos.*']),
    test_suite='tests',
    extras_require={"test": test_requirements},
    url='https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/',
    version='0.1.32',
    zip_safe=False,
)
